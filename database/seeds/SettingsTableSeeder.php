<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            [
                'key'           => 'default_shipping_price',
                'name'          => 'Default Shipping Price',
                'description'   => 'The default shipping price.',
                'value'         => 0,
                'field'         => '{"name":"value","label":"Value","type":"number"}',
                'active'        => 1,
            ],
        ]);
    }
}
