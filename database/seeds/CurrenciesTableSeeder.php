<?php

use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('currencies')->delete();

    	$currencies = [
    		[
                'name'  => 'Philippine Peso',
                'code'  => 'PHP',
                'symbol' => '&#8369;',
    		],
            [
                'name'  => 'United States Dollar',
                'code'  => 'USD',
                'symbol' => '&#36;',
            ],
            [
                'name'  => 'Euro',
                'code'  => 'EUR',
                'symbol' => '&#8364;',
            ],
            [
                'name'  => 'Indonesian rupiah',
                'code'  => 'IDR',
                'symbol' => '&#82;&#112;',
            ],
    	];

    	DB::table('currencies')->insert($currencies);
    }
}
