<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->delete();

    	$users = [
            // Admin
            [
        		'first_name'    => 'Thaniel Jay',
                'last_name'     => 'Duldulao',
                'email'         => 'tj@boomtech.com',
                'password'      => '$2y$10$EZKbf/7VDO7R7JSC7a.ZF.fxEACDqLZhuCqlCCZRcVdStbIRTUUIm', // Encrypted password is: adminpass
                'salutation'    => 'Mr.',
        		'birthday'		=> \Carbon\Carbon::now()->toDateString(),
                'default_shipping_address_id' => 1,
                'default_billing_address_id' => 1,
        		'gender'		=> 1,
        		'active'		=> 1,
                'created_at'    => \Carbon\Carbon::now()->toDateTimeString()
            ],
    	];

    	DB::table('users')->insert($users);
    }
}
