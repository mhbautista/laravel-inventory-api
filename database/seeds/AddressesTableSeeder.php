<?php

use Illuminate\Database\Seeder;

class AddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('addresses')->delete();

    	$addresses = [
    		[
                'user_id'      => 1,
                'country_id'   => 1,
                'name'         => 'Thaniel Jay Duldulao',
                'address1'     => '#70 Dr. Jose Fabella St. Brgy. Plainview, Mandalayung City, Philippinnes.',
                'address2'     => '',
                'unit'        => '#70',
                'street'       => 'Dr. Jose Fabella',
                'barangay'     => 'Plainview',
                'region'       => 'ncr',
                'county'       => 'Metro Manila', //province
                'city'         => 'Mandalayung',
                'postal_code'  => '1550',
                'phone'        => '',
                'mobile_phone' => '+639065045197',
                'comment'      => '',
                'created_at'   => \Carbon\Carbon::now()->toDateTimeString()

    		],
    	];

    	DB::table('addresses')->insert($addresses);
    }
}
