<?php

use Illuminate\Database\Seeder;

class TaxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('taxes')->delete();

    	$taxes = [
    		[
				'name'  => 'Philippines Vat',
				'value' => '12.00',
    		],
    	];

    	DB::table('taxes')->insert($taxes);
    }
}
