<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('categories')->delete();

    	$categories = [
            // Categories
            [
                'id'        => 1,
                'parent_id' => null,
                'name'      => 'Categories',
                'slug'      => 'categories',
                'lft'       => 2,
                'rgt'       => 9,
                'depth'     => 1,
            ],
            // Groups
            [
                'id'        => 2,
                'parent_id' => null,
                'name'      => 'Groups',
                'slug'      => 'groups',
                'lft'       => 10,
                'rgt'       => 21,
                'depth'     => 1,
            ],
            // Categories Children
            [
                'id'        => 3,
                'parent_id' => 1,
                'name'      => 'Eyes',
                'slug'      => 'eyes',
                'lft'       => 3,
                'rgt'       => 4,
                'depth'     => 2,
            ],
            [
                'id'        => 4,
				'parent_id' => 1,
				'name'      => 'Lips',
				'slug'      => 'lips',
				'lft'       => 5,
				'rgt'       => 6,
				'depth'     => 2,
    		],
            [
                'id'        => 5,
                'parent_id' => 1,
                'name'      => 'Skin',
                'slug'      => 'skin',
                'lft'       => 7,
                'rgt'       => 8,
                'depth'     => 2,
            ],
            // Groups Children
            [
                'id'        => 6,
                'parent_id' => 2,
                'name'      => 'Artist\'s Pick',
                'slug'      => 'artist-s-pick',
                'lft'       => 11,
                'rgt'       => 12,
                'depth'     => 2,
            ],
            [
                'id'        => 7,
                'parent_id' => 2,
                'name'      => 'Best Featured',
                'slug'      => 'best-featured',
                'lft'       => 13,
                'rgt'       => 14,
                'depth'     => 2,
            ],
            [
                'id'        => 8,
                'parent_id' => 2,
                'name'      => 'Best Sellers',
                'slug'      => 'best-sellers',
                'lft'       => 15,
                'rgt'       => 16,
                'depth'     => 2,
            ],
            [
                'id'        => 9,
                'parent_id' => 2,
                'name'      => 'Featured',
                'slug'      => 'featured',
                'lft'       => 17,
                'rgt'       => 18,
                'depth'     => 2,
            ],
            [
                'id'        => 10,
                'parent_id' => 2,
                'name'      => 'New',
                'slug'      => 'new',
                'lft'       => 19,
                'rgt'       => 20,
                'depth'     => 2,
            ],
            [
                'id'        => 11,
                'parent_id' => 2,
                'name'      => 'Collections',
                'slug'      => 'collections',
                'lft'       => 21,
                'rgt'       => 22,
                'depth'     => 2,
            ],
    	];

    	DB::table('categories')->insert($categories);
    }
}
