<?php

use Illuminate\Database\Seeder;

class OrderStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('order_statuses')->delete();

    	$orderStatuses = [
    		[
                'name'  => 'Pending',
                'notification'  => 0,
    		],
            [
                'name'  => 'Paid',
                'notification'  => 1,
            ],
            [
                'name'  => 'Processing',
                'notification'  => 2,
            ],
            [
                'name'  => 'Delivered',
                'notification'  => 3,
            ],
            [
                'name'  => 'Done',
                'notification'  => 4,
            ],
            [
                'name'  => 'Cancelled',
                'notification'  => 5,
            ],
    	];

    	DB::table('order_statuses')->insert($orderStatuses);
    }
}
