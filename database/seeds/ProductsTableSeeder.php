<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('products')->delete();

    	$products = [
    		[
                'id'                => 1,
                'group_id'          => 1,
                'attribute_set_id' => 1,
                'name'              => 'Lipstick',
                'description'       => '<p><big>Teviant x Love Marie Collection</big></p>

<p>
<big>Here&rsquo;s to new beginnings. Embracing innocence and naivit&eacute;, Amore is a soft breath of fresh air. She has a fresh take on life and is ready to experience it.</big></p>

<p>
<big>Soft and earth-toned shades for a soft, everyday look. With various shades of nude that&rsquo;s suitable for all skin tones.</big></p>

<p>
<big>Use matte colors on the whole eyelid, followed by shimmery shades in the middle for definition. Concentrate dark tones on the outside, intensifying according to time of day.</big></p>

<p>
<big>Shades included: Stellato, Rose, Grazie, Gelato, Sole, Vita, Bella, Toscana, Allora, Dolce, Cappucino, Milano</big></p>',
                'tax_id'            => 1,
                'price'             => '2450',
                'sku'               => '0710535605501',
                'stock'             => 100,
                'active'            => 1
    		],
    	];
        $product_measurements = [
            [
                'id'    => 1,
                'product_id' => 1,
                'weight'    => 1,
                'length'    => 1,
                'width'     => 1,
                'height'    => 1
            ],
        ];

    	DB::table('products')->insert($products);
        DB::table('product_measurements')->insert($product_measurements);
    }
}
