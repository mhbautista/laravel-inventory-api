<?php

use Illuminate\Database\Seeder;

class CarriersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('carriers')->delete();

    	$carriers = [
    		[
                'name'  => 'LBC',
                'price'  => '150',
                'delivery_text' => 'LBC Express, Inc. is a courier company based in the Philippines.',
				'logo'  => null,
    		],
    	];

    	DB::table('carriers')->insert($carriers);
    }
}
