<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder {

	public function run()
	{
		DB::table('countries')->delete();

		$countries = [
			['code' => 'US', 'name' => 'United States', 'currency_id' => 2],
			['code' => 'AD', 'name' => 'Andorra', 'currency_id' => 3], //
			['code' => 'AT', 'name' => 'Austria', 'currency_id' => 3], //
			['code' => 'BE', 'name' => 'Belgium', 'currency_id' => 3],//
			['code' => 'CY', 'name' => 'Cyprus', 'currency_id' => 3], //
			['code' => 'EE', 'name' => 'Estonia', 'currency_id' => 3], //
			['code' => 'FI', 'name' => 'Finland', 'currency_id' => 3],//
			['code' => 'FR', 'name' => 'France', 'currency_id' => 3], //
			['code' => 'GF', 'name' => 'French Guiana', 'currency_id' => 3], //
			['code' => 'DE', 'name' => 'Germany', 'currency_id' => 3], //
			['code' => 'GR', 'name' => 'Greece', 'currency_id' => 3], //
			['code' => 'GP', 'name' => 'Guadeloupe', 'currency_id' => 3], //
			['code' => 'ID', 'name' => 'Indonesia', 'currency_id' => 4],
			['code' => 'IE', 'name' => 'Ireland', 'currency_id' => 3], //
			['code' => 'IT', 'name' => 'Italy', 'currency_id' => 3], //
			['code' => 'XK', 'name' => 'Kosovo', 'currency_id' => 3], //
			['code' => 'LV', 'name' => 'Latvia', 'currency_id' => 3], //
			['code' => 'LT', 'name' => 'Lithuania', 'currency_id' => 3], //
			['code' => 'LU', 'name' => 'Luxembourg', 'currency_id' => 3], //
			['code' => 'MT', 'name' => 'Malta', 'currency_id' => 3], //
			['code' => 'MQ', 'name' => 'Martinique', 'currency_id' => 3], //
			['code' => 'TY', 'name' => 'Mayotte', 'currency_id' => 3], //
			['code' => 'MC', 'name' => 'Monaco', 'currency_id' => 3], //
			['code' => 'ME', 'name' => 'Montenegro', 'currency_id' => 3], //
			['code' => 'NL', 'name' => 'Netherlands', 'currency_id' => 3], //
			['code' => 'PT', 'name' => 'Portugal', 'currency_id' => 3], //
			['code' => 'PH', 'name' => 'Philippines', 'currency_id' => 1], //
			['code' => 'RE', 'name' => 'Reunion', 'currency_id' => 3], //
			['code' => 'BL', 'name' => 'Saint Barthélemy', 'currency_id' => 3], //
			['code' => 'SM', 'name' => 'San Marino', 'currency_id' => 3], //
			['code' => 'SK', 'name' => 'Slovakia', 'currency_id' => 3], //
			['code' => 'SI', 'name' => 'Slovenia', 'currency_id' => 3], //
			['code' => 'ES', 'name' => 'Spain', 'currency_id' => 3], //
			['code' => 'VA', 'name' => 'Vatican City State', 'currency_id' => 3], //
			['code' => 'ZW', 'name' => 'Zimbabwe', 'currency_id' => 3] //
		];

		DB::table('countries')->insert($countries);
	}
}