<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyOrderProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_product', function (Blueprint $table) {
            $table->dropColumn('cart_rule_discount_per_item');
            $table->dropColumn('amount_per_item_with_tax');


            $table->integer('specific_price_id')->unsigned()->nullable();
            $table->decimal('specific_price_reduction',13, 2)->nullable()->default(0);
            $table->enum('specific_price_discount_type', array('Amount', 'Percent'))->nullable();
            $table->dateTime('specific_price_start_date')->nullable();
            $table->dateTime('specific_price_expiration_date')->nullable();
            $table->decimal('amount_per_item');

             $table->foreign('specific_price_id')
                ->references('id')->on('specific_prices')
                ->onDelete('no action')
                ->onUpdate('no action');
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_product', function (Blueprint $table) {
            $table->dropForeign('order_product_specific_price_id_foreign');
            $table->dropColumn('specific_price_reduction');
            $table->dropColumn('specific_price_discount_type');
            $table->dropColumn('specific_price_start_date');
            $table->dropColumn('specific_price_expiration_date');
            $table->dropColumn('amount_per_item');
        });
    }
}
