<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCountryIdNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->integer('country_id')->unsigned()->nullable()->default(null)->change();
            // $table->dropForeign(['country_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->integer('country_id')->unsigned()->nullable(false)->change();
            // $table->foreign('country_id')
            //     ->references('id')->on('countries')
            //     ->onDelete('no action')
            //     ->onUpdate('no action');
        });
    }
}
