<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpecificPriceIdToOrderProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_product', function (Blueprint $table) {
            $table->decimal('cart_rule_discount_per_item')->nullable();
            $table->decimal('amount_per_item_with_tax');
            $table->decimal('total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

         Schema::table('order_product', function (Blueprint $table) {
      
            $table->dropColumn('total');
            if(Schema::hasColumn('order_product', 'cart_rule_discount_per_item'))
            {
                $table->dropColumn('cart_rule_discount_per_item');
            }
            if(Schema::hasColumn('order_product', 'amount_per_item_with_tax'))
            {
                $table->dropColumn('amount_per_item_with_tax');
            }
        });

    }
}
