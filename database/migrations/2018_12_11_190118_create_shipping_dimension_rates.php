<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingDimensionRates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_dimension_rates', function (Blueprint $table) {
            $table->increments('id');

            $table->decimal('min_length', 8, 2);
            $table->decimal('max_length', 8, 2);
            $table->decimal('min_width', 8, 2);
            $table->decimal('max_width', 8, 2);
            $table->decimal('min_height', 8, 2);
            $table->decimal('max_height', 8, 2);

            $table->enum('unit', ['cm', 'm']);
            $table->decimal('price', 8, 2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_dimension_rates');
    }
}
