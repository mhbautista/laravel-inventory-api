<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentMethodInOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->enum('payment_method', ['card', 'deposit']);
        });

        // Enable notifications in Pending Orders
        \DB::table('order_statuses')->where('name', 'Pending')->update(['notification' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('payment_method');
        });

        // Enable notifications in Pending Orders
        \DB::table('order_statuses')->where('name', 'Pending')->update(['notification' => 0]);
    }
}
