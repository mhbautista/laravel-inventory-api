<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderCartRulesNewFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_cart_rules', function(Blueprint $table) 
        {
            $table->string('name', 255);
            $table->string('code', 100);
            $table->dateTime('start_date');
            $table->dateTime('expiration_date');
            $table->boolean('free_delivery')->default(0);
            $table->enum('discount_type', array('Percent - order', 
                        'Percent - selected products', 
                        'Amount - selected products', 'Amount - order'));
            $table->decimal('reduction_amount', 13, 2)->nullable()->default(0);
            $table->integer('reduction_currency_id')->unsigned()->nullable();
            $table->decimal('total_discount', 13, 2);
            $table->integer('gift')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_cart_rules', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('code');
            $table->dropColumn('start_date');
            $table->dropColumn('expiration_date');
            $table->dropColumn('discount_type');
            $table->dropColumn('reduction_amount');
            $table->dropColumn('reduction_currency_id');
            $table->dropColumn('total_discount');
            $table->dropColumn('gift');

        });
    }
}
