<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressFieldsInAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->string('unit')->nullable()->default(null);
            $table->string('house')->nullable()->default(null);
            $table->string('building')->nullable()->default(null);
            $table->string('street')->nullable()->default(null);
            $table->string('barangay')->nullable()->default(null);
            $table->string('town')->nullable()->default(null);
            $table->string('state')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropColumn([
                'unit',
                'house',
                'building',
                'street',
                'barangay',
                'town',
                'state',
            ]);
        });
    }
}
