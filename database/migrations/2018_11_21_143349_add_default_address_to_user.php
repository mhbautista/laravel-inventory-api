<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultAddressToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('default_shipping_address_id')->nullable()->default(null);
            $table->integer('default_billing_address_id')->nullable()->default(null);
        });

        Schema::table('addresses', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'default_shipping_address_id',
                'default_billing_address_id',
            ]);
        });

        Schema::table('addresses', function (Blueprint $table) {
            $table->enum('type', ['billing', 'shipping'])->after('comment');
        });
    }
}
