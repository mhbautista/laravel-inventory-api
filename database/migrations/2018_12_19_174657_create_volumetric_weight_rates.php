<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolumetricWeightRates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_volumetric_weight_rates', function (Blueprint $table) {
            $table->increments('id');

            $table->decimal('min_volumetric_weight', 8, 2);
            $table->decimal('max_volumetric_weight', 8, 2);
            $table->enum('unit', ['kg']);
            $table->decimal('price', 8, 2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_volumetric_weight_rates');
    }
}
