<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropPrimaryInShoppingCart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shoppingcart', function (Blueprint $table) {
            $table->dropPrimary( 'PRIMARY KEY');
        });
        Schema::table('shoppingcart', function (Blueprint $table) {
            $table->increments( 'id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shoppingcart', function (Blueprint $table) {
            $table->drop('id');
        });
        Schema::table('shoppingcart', function (Blueprint $table) {
            $table->primary(['identifier', 'instance']);
        });
    }
}
