<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductGoesWellWith extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_goes_well_with', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_id')->unsigned();
        });


        Schema::create('product_product_goes_well_with', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_goes_well_with_id')->unsigned();
            $table->integer('product_id')->unsigned();


            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('no action')
                ->onUpdate('no action');
                $table->foreign('product_goes_well_with_id')
                ->references('id')->on('product_goes_well_with')
                ->onDelete('no action')
                ->onUpdate('no action');
        });

        // Schema::table('products', function (Blueprint $table) {
        //     $table->integer('product_goes_well_with_id')->nullable()->default(null)->after('group_id');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_goes_well_with');
        Schema::dropIfExists('product_product_goes_well_with');
        // Schema::table('products', function (Blueprint $table) {
        //     $table->dropColumn('product_goes_well_with_id');
        // });
    }
}
