<?php

return [

	'currency'				=> 'Currency',
	'currencies'			=> 'Currencies',
	'code'					=> 'Code',
	'name'					=> 'Name',
	'value'					=> 'Value',
	'default'				=> 'Default',
	'symbol'				=> 'Symbol',
	'no_default_currency'	=> 'No default currency defined',

];