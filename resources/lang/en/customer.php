<?php

return [

	'customer'                => 'Customer',
	'customers'               => 'Customers',
	'email'                 => 'E-mail',
	'name'                  => 'Name',
	'firt_name'             => 'First Name',
	'middle_name'           => 'Middle Name',
	'last_name'             => 'Last Name',
	'salutation'            => 'Salutation',
	'birthday'				=> 'Birthday',
	'gender'                => 'Gender',
	'male'                  => 'Male',
	'female'                => 'Female',
	'password'              => 'Password',
	'password_confirmation' => 'Password Confirmation',

	// Tabs
	'tab_general'			=> 'General',
	'tab_permissions'		=> 'Permissions',
	'tab_address'			=> 'Address',
	'tab_company'			=> 'Company',
];