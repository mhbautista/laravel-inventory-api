@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))
  {{-- FIELD EXTRA CSS  --}}
  {{-- push things in the after_styles section --}}

      @push('crud_fields_styles')
          <!-- no styles -->
      @endpush


  {{-- FIELD EXTRA JS --}}
  {{-- push things in the after_scripts section --}}

      @push('crud_fields_scripts')

        <script src="{{ asset('js/bigNumber.js') }}"></script>

        <script>
          var price = $('input[name="price"]');
          var price_with_vat = $('input[name="price_without_vat"]');
          var tax_value = $('#tax').select2({
                    theme: "bootstrap"
                }).find(':selected').data('value');

          // Calculate price with tax on document ready
          $(document).ready(function() {
            if (price.val().length > 0 ) {
                tax_val = new BigNumber(tax_value);
                price_val = new BigNumber(price.val());
                absolute_tax = (1 + (tax_val/100));
                priceWithoutTax = (price_val / absolute_tax).toFixed(2);

                price_with_vat.val(priceWithoutTax);
            }
          });

          // Calculate price with tax
          $(document).on('input', 'input[name="price"]', function() {
            if ($(this).val().length > 0) {
                tax_val = new BigNumber(tax_value);
                price_val = new BigNumber(price.val());
                absolute_tax = (1 + (tax_val/100));
                priceWithoutTax = (price_val / absolute_tax).toFixed(2);
                price_with_vat.val(priceWithoutTax);
            } else {
              price.val('');
            }
          });

          // Calculate price without on selected tax change
          $('#tax').select2({
                    theme: "bootstrap"
                }).on("change", function(e) {
            tax_value = $('#tax').select2({
                    theme: "bootstrap"
                }).find(':selected').data('value');

            tax_val = new BigNumber(tax_value);
            price_val = new BigNumber(price.val());
            absolute_tax = (1 + (tax_val/100));
            priceWithoutTax = (price_val / absolute_tax).toFixed(2);
            // compute_price_with_vat = price.val() + 100;
            price_with_vat.val(priceWithoutTax);
          });
        </script>

      @endpush
@endif