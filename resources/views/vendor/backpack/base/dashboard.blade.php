@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        {{ trans('backpack::base.dashboard') }}<small>Administration Overview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::base.dashboard') }}</li>
      </ol>
    </section>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 id="orders-today"><i class="fa fa-spin fa-refresh"></i></h3>

              <p>Orders Today</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 id="orders-this-week"><i class="fa fa-spin fa-refresh"></i></h3>

              <p>Orders this Week</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3 id="sales-today"><i class="fa fa-spin fa-refresh"></i></h3>

              <p>Sales Today</p>
            </div>
            <div class="icon">
              ₱
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3 id="sales-this-week"><i class="fa fa-spin fa-refresh"></i></h3>

              <p>Sales this Week</p>
            </div>
            <div class="icon">
              ₱
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Orders Status</h3>
				</div>
				<table class="table">
					<tbody>
						<tr>
							<td>Pending</td>
							<td class="text-right"><span id="orders-status-pending" class="badge"></span></td>
						</tr>
						<tr>
							<td>Paid</td>
							<td class="text-right"><span id="orders-status-paid" class="badge"></span></td>
						</tr>
						<tr>
							<td>Processing</td>
							<td class="text-right"><span id="orders-status-processing" class="badge"></span></td>
						</tr>
						<tr>
							<td>Delivered</td>
							<td class="text-right"><span id="orders-status-delivered" class="badge"></span></td>
						</tr>
						<tr>
							<td>Done</td>
							<td class="text-right"><span id="orders-status-done" class="badge"></span></td>
						</tr>
						<tr>
							<td>Cancelled</td>
							<td class="text-right"><span id="orders-status-cancelled" class="badge"></span></td>
						</tr>
						<tr>
							<td>Refunded</td>
							<td class="text-right"><span id="orders-status-refunded" class="badge"></span></td>
						</tr>
					</tbody>
				</table>
				<div class="overlay" id="orders-status-spin">
	            	<i class="fa fa-refresh fa-spin"></i>
	            </div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="box box-default">
        <div class="box-header">
          <h3 class="box-title">Top Products</h3>
        </div>
        <table class="table" id="top-products-table">
          <thead>
            <th>Product</th>
            <th>Orders</th>
            <th>Sales</th>
          </thead>
          <tbody>
          </tbody>
        </table>
        <div class="overlay" id="top-products-spin">
          <i class="fa fa-refresh fa-spin"></i>
        </div>
      </div>
		</div>
	</div>
  
@endsection

@push('after_scripts')
<script>
	$(document).ready(function(){
		// console.log('Document loaded');
		
		// Assign handlers immediately after making the request,
        // and remember the jqXHR object for this request
        var jqxhr = $.ajax({
            url: "{{ route('ajax.dashboard.index') }}",
            method: 'GET'
        })
        .done(function(data) {
            console.log("success");
            console.log(data);

            $("#orders-today").html(data.orders_today);
            $("#orders-this-week").html(data.orders_this_week);
            $("#sales-today").html(data.sales_today);
            $("#sales-this-week").html(data.sales_this_week);
            $("#orders-status-spin").hide();
            $("#top-products-spin").hide();

            $("#orders-status-pending").html(data.orders_status.pending);
            $("#orders-status-paid").html(data.orders_status.paid);
            $("#orders-status-processing").html(data.orders_status.processing);
            $("#orders-status-delivered").html(data.orders_status.delivered);
            $("#orders-status-done").html(data.orders_status.done);
            $("#orders-status-cancelled").html(data.orders_status.cancelled);
            $("#orders-status-refunded").html(data.orders_status.refunded);

            $.each(data.top_products, function(i, item) {
                var $tr = $('#top-products-table > tbody').append(
                    $('<tr>').append(
                      $('<td>').text(item.name),
                      $('<td>').text(item.orders_count),
                      $('<td>').text(item.orders_sum)
                    )
                ); //.appendTo('#records_table');
                // console.log($tr.wrap('<p>').html());
            });
            $("#top-products-table > tbody").append()
        })
        .fail(function(data) {
            console.log("error");
        })
        .always(function(data) {
            console.log("complete");
        });
         
        // Perform other work here ...
         
        // Set another completion function for the request above
        jqxhr.always(function() {
          console.log("second complete");
        });
	});
</script>
@endpush