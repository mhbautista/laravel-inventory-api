<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>

<li><a href='{{ backpack_url('order') }}'><i class='fa fa-tag'></i> <span>Orders</span></a></li>

<li><a href='{{ backpack_url('customer') }}'><i class='fa fa-users'></i> <span>Customers</span></a></li>

<li class="treeview">
    <a href="#"><i class="fa fa-list-alt"></i> <span>Products</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
	    @can('list_products')
	    	<li><a href='{{ backpack_url('product') }}'><i class="fa fa-list"></i> <span>{{ trans('product.products') }}</span></a></li>
	    @endcan
        @can('list_products')
            <li><a href='{{ backpack_url('productgoeswellwith') }}'><i class="fa fa-list"></i> <span>Goes well with</span></a></li>
        @endcan
	    @can('list_specific_prices')
			<li><a href='{{ backpack_url('product-discount') }}'><i class='fa fa-money'></i> <span>Discount</span></a></li>
		@endcan
	    @can('list_categories')
	        <li><a href='{{ backpack_url('category') }}'><i class='fa fa-bars'></i> <span>Categories</span></a></li>
	    @endcan
	    @can('list_attributes')
	        <li><a href='{{ backpack_url('attribute') }}'><i class="fa fa-tag"></i> <span>{{ trans('attribute.attributes') }}</span></a></li>
	    @endcan
	    @can('list_attribute_sets')
	        <li><a href='{{ backpack_url('attributes-sets') }}'><i class="fa fa-tags"></i> <span>{{ trans('attribute.attribute_sets') }}</span></a></li>
	    @endcan
    </ul>
</li>

<li class="treeview">
    <a href="#"><i class="fa fa-newspaper-o"></i> <span>News</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href="{{ backpack_url('article') }}"><i class="fa fa-newspaper-o"></i> <span>Articles</span></a></li>
      <li><a href="{{ backpack_url('tag') }}"><i class="fa fa-tag"></i> <span>Tags</span></a></li>
    </ul>
</li>

<li class="treeview">
    <a href="#"><i class="fa fa-gears"></i> <span>Settings</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">

        @can('list_cart_rules')
        <li><a href='{{ backpack_url('cart-rules') }}'><i class='fa fa-tag'></i> <span>Cart Rules</span></a></li>
        @endcan

    	@can('list_currencies')
		<li><a href='{{ backpack_url('currency') }}'><i class='fa fa-usd'></i> <span>Currencies</span></a></li>
		@endcan

		@can('list_carriers')
    	<li><a href='{{ backpack_url('carrier') }}'><i class='fa fa-truck'></i> <span>Carriers</span></a></li>
    	@endcan

    	@can('list_taxes')
    	<li><a href='{{ backpack_url('tax') }}'><i class='fa fa-balance-scale'></i> <span>Tax</span></a></li>
    	@endcan

    	@can('list_order_statuses')
			<li><a href='{{ backpack_url('order-status') }}'><i class='fa fa-tag'></i> <span>Order Status</span></a></li>
    	@endcan

        {{-- @can('list_countries') --}}
            <li><a href='{{ backpack_url('country') }}'><i class='fa fa-flag-o'></i> <span>Countries</span></a></li>
        {{-- @endcan --}}

    	@can('list_notification_templates')
    	<li><a href='{{ backpack_url('notification-templates') }}'><i class='fa fa-list'></i> <span>Notification Templates</span></a></li>
    	@endcan

        {{-- @can('list_shipping_volumetric_weight_rates') --}}
        <li><a href='{{ backpack_url('shipping-volumetric-weight-rate') }}'><i class='fa fa-cube'></i> <span>Volumetric Weight Rates</span></a></li>
        {{-- @endcan --}}

        {{-- @can('list_shipping_dimension_rates') --}}
        <li><a href='{{ backpack_url('shipping-dimension-rate') }}'><i class='fa fa-cube'></i> <span>Dimension Rates</span></a></li>
        {{-- @endcan --}}

        {{-- @can('list_shipping_weight_rates') --}}
        <li><a href='{{ backpack_url('shipping-weight-rate') }}'><i class='fa fa-balance-scale'></i> <span>Weight Rates</span></a></li>
        {{-- @endcan --}}

        {{-- @can('list_shipping_country_rates') --}}
        <li><a href='{{ backpack_url('shipping-country-rate') }}'><i class='fa fa-flag'></i> <span>Country Rates</span></a></li>
        {{-- @endcan --}}
    	
    	<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>

        {{-- System Settings --}}
        <li><a href='{{ url(config('backpack.base.route_prefix', 'admin') . '/setting') }}'><i class='fa fa-cog'></i> <span>Settings</span></a></li>
	</ul>
</li>

<li class="treeview">
    <a href="#"><i class="fa fa-group"></i> <span>Users, Roles, Permissions</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href='{{ backpack_url('user') }}'><i class='fa fa-user'></i> <span>Users</span></a></li>

      <li><a href="{{ backpack_url('role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>

      <li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key"></i> <span>Permissions</span></a></li>

    </ul>
</li>

<li class="treeview">
    <a href="#"><i class="fa fa-bar-chart"></i> <span>Reports</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href='{{ route('reports.crud.order-product.index') }}'><i class='fa fa-fw'></i> <span>Product Orders</span></a></li>
    </ul>
</li>

<li class="treeview">
    <a href="#"><i class="fa fa-cogs"></i> <span>Advanced</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href='{{ url(config('backpack.base.route_prefix', 'admin').'/backup') }}'><i class='fa fa-hdd-o'></i> <span>Backups</span></a></li>
    </ul>
</li>
