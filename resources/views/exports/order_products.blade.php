<?php

use Carbon\Carbon;

?>

<table>
	<thead>
		<tr>
			<th>Order #</th>
			<th>SKU</th>
			<th>Item Name</th>
			<th>Price</th>
			{{-- <th>Quantity</th> --}}
			<th>Status</th>
			<th>Date Ordered</th>
			<th>Date Cancelled</th>
			<th>Payment Method</th>
			<th>Magpie Charge Id</th>
			<th>Voucher</th>
			<th>Voucher Amount</th>
			<th>Shipping Fee</th>
			<th>Reference #</th>
		</tr>
	</thead>
	<tbody>
	@foreach($order_products as $order_product)
		{{-- Itemise display --}}
		@if($order_product->quantity > 1)
			@for ($i = 0; $i < $order_product->quantity; $i++)
			<tr>
				<td>{{ $order_product->order->id }}</td>
				<td>{{ $order_product->product->sku }}</td>
				<td>{{ $order_product->product->name }}</td>
				<td>{{ number_format($order_product->product->price, 2) }}</td>
				{{-- <td>{{ $order_product->quantity }}</td> --}}
				<td>{{ $order_product->status }}</td>
				<td>{{ Carbon::parse($order_product->order->created_at)->format('Y-m-d H:i:s') }}</td>
				<td>{{ isset($order_product->order->date_cancelled) ? Carbon::parse($order_product->order->date_cancelled)->format('Y-m-d H:i:s') : null }}</td>
				<td>{{ $order_product->order->payment_method }}</td>
				<td>{{ $order_product->order->magpie_charge_id }}</td>
				<td>{{ $order_product->order->voucher }}</td>
				<td>{{ number_format($order_product->order->voucher_amount, 2) }}</td>
				<td>{{ number_format($order_product->order->total_shipping, 2) }}</td>
				<td>{{ $order_product->order->invoice_no }}</td>
				<td></td>
			</tr>
			@endfor
		@else
			<tr>
				<td>{{ $order_product->order->id }}</td>
				<td>{{ $order_product->product->sku }}</td>
				<td>{{ $order_product->product->name }}</td>
				<td>{{ number_format($order_product->product->price, 2) }}</td>
				{{-- <td>{{ $order_product->quantity }}</td> --}}
				<td>{{ $order_product->status }}</td>
				<td>{{ Carbon::parse($order_product->order->created_at)->format('Y-m-d H:i:s') }}</td>
				<td>{{ isset($order_product->order->date_cancelled) ? Carbon::parse($order_product->order->date_cancelled)->format('Y-m-d H:i:s') : null }}</td>
				<td>{{ $order_product->order->payment_method }}</td>
				<td>{{ $order_product->order->magpie_charge_id }}</td>
				<td>{{ $order_product->order->voucher }}</td>
				<td>{{ number_format($order_product->order->voucher_amount, 2) }}</td>
				<td>{{ number_format($order_product->order->total_shipping, 2) }}</td>
				<td>{{ $order_product->order->invoice_no }}</td>
				<td></td>
			</tr>
		@endif
	@endforeach
	</tbody>
</table>