<!-- textarea -->
<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    @include('crud::inc.field_translatable_icon')
    <textarea
    	name="{{ $field['name'] }}"
        @include('crud::inc.field_attributes')

    	>{{ isset($crud->entry[$field['entity']][$field['attribute']]) ? $crud->entry[$field['entity']][$field['attribute']] : false }}</textarea>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>