@extends('backpack::layout')

@section('content-header')
	<section class="content-header">
	  <h1>
	    <span>{{ $crud->entity_name }}</span>
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
	    <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
	    <li class="active">{{ trans('backpack::crud.preview') }}</li>
	  </ol>
	</section>
@endsection

@section('content')
	@if ($crud->hasAccess('list'))
		<a href="{{ url($crud->route) }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a><br><br>
	@endif

	<div class="row">
		<div class="col-md-12 well">
			<h2>{{ trans('order.order') }} #{{ $order->id }} - {{ $order->user->fullname ?? $order->shippingAddress->name }}</h2>
			<h4>Magpie charge ID: {{ $order->magpie_charge_id }}</h4>
			<h4>Reference number: {{ $order->invoice_no }}</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-md-7">
			<div class="box">
			    <div class="box-header with-border">
			      <h3 class="box-title">
		            <span><i class="fa fa-ticket"></i> {{ trans('order.order_status') }}</span>
		          </h3>
			    </div>
			    <div class="box-body">
			    	<h4>
			    		Current status <br><br>
			    		<span class="label label-default">{{ $order->status->name }}</span>
			    	</h4>

					<hr>

					<h4>
						Status history
					</h4>
			    	@if (count($order->statusHistory) > 0)
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>{{ trans('order.status') }}</th>
									<th>{{ trans('common.date') }}</th>
								</tr>
							</thead>
							<tbody>
								@foreach($order->statusHistory as $statusHistory)
									<tr>
										<td>{{ $statusHistory->status->name }}</td>
										<td>{{ $statusHistory->created_at }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					@else
						<div class="alert alert-info">
							{{ trans('order.no_status_history') }}
						</div>
					@endif

					<hr>

					@if (count($orderStatuses) > 0)
						<form action="{{ route('updateOrderStatus') }}" method="POST">
							{!! csrf_field() !!}
							<input type="hidden" name="order_id" value="{{ $order->id }}">

							<div class="form-group">
								<select name="status_id" id="status_id" class="select2_field" style="width: 100%">
									@foreach($orderStatuses as $orderStatus)
										@if($orderStatus->id == $order->status_id)
											<option value="{{ $orderStatus->id }}" selected>{{ $orderStatus->name}}</option>
										@else
											<option value="{{ $orderStatus->id }}" >{{ $orderStatus->name}}</option>
										@endif
									@endforeach
								</select>
							</div>

							<button type="submit" class="btn btn-primary">{{ trans('order.update_status') }}</button>
						</form>
					@else
						<div class="alert alert-info">
							{{ trans('order.no_order_statuses') }}
						</div>
					@endif
			    </div>
		    </div>
		</div>
		<div class="col-md-5">
			<div class="box">
			    <div class="box-header with-border">
			      <h3 class="box-title">
		            <span><i class="fa fa-user"></i> {{ trans('user.customer') }}</span>
		          </h3>
			    </div>

			    <div class="box-body">
			    	<h3>{{ trans('user.tab_general') }}</h3>
					<div class="col-md-12 well">
						<div class="col-md-6">
							<i class="fa fa-user-circle-o"></i> {{ $order->shippingAddress->name ?? $order->user->fullname }} <br/>
							<i class="fa fa-envelope"></i> <a href="mailto:{{ $order->shippingAddress->email ?? $order->user->email }}">{{ $order->shippingAddress->email ?? $order->user->email }}</a><br/>
						</div>
						<div class="col-md-6">
							{{-- <i class="fa fa-birthday-cake"></i> {{ $order->user->birthday ? $order->user->birthday.' ('.$order->user->age().' '.strtolower(trans('common.years')).')': '-' }}
							<br> --}}
							@if(isset($order->user))
							{!! ($order->user->gender == 1) ? '<i class="fa fa-mars"></i> '.trans('user.male') : '<i class="fa fa-venus"></i> '.trans('user.female') !!}
							@endif
						</div>
					</div>
			    </div>
		    </div>

		    <div class="box">
			    <div class="box-header with-border">
			      <h3 class="box-title">
		            <span><i class="fa fa-user"></i> {{ trans('order.shipping_details') }}</span>
		          </h3>
			    </div>

			    <div class="box-body">
			    	<div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab-shipping-address" data-toggle="tab">{{ trans('order.shipping_address') }}</a></li>
                            <li><a href="#tab-billing-info" data-toggle="tab">{{ trans('order.billing_info') }}</a></li>
                        </ul>
                        @if ($order->shippingAddress)
	                        <div class="tab-content">
	                            <div class="tab-pane active" id="tab-shipping-address">
									<h4>{{ trans('order.shipping_address') }}</h4>
									<table class="table table-condensed table-hover">
										<tr>
											<td>{{ trans('address.contact_person') }}</td>
											<td>{{ $order->shippingAddress->name }}</td>
										</tr>
										{{--<tr>
											<td>{{ trans('address.address') }}</td>
											<td>
												{{ $order->shippingAddress->address1 }} <br>
												{{ $order->shippingAddress->address2 }}
											</td>
										</tr>--}}
										<tr>
											<td>Unit no. / Room no. </td>
											<td>{{ $order->shippingAddress->unit }}</td>
										</tr>
										<tr>
											<td>Building </td>
											<td>{{ $order->shippingAddress->building }}</td>
										</tr>
										<tr>
											<td>Street </td>
											<td>{{ $order->shippingAddress->street }}</td>
										</tr>
										<tr>
											<td>Barangay </td>
											<td>{{ $order->shippingAddress->barangay }}</td>
										</tr>
										<tr>
											<td>Province / {{ trans('address.county') }} </td>
											<td>{{ $order->shippingAddress->county }}</td>
										</tr>
										<tr>
											<td>{{ trans('address.city') }}</td>
											<td>{{ $order->shippingAddress->city }}</td>
										</tr>
										<tr>
											<td>{{ trans('address.mobile_phone') }}</td>
											<td>{{ $order->shippingAddress->mobile_phone }}</td>
										</tr>
										<tr>
											<td>{{ trans('address.postal_code') }}</td>
											<td>{{ $order->shippingAddress->postal_code }}</td>
										</tr>
										<tr>
											<td>{{ trans('address.phone') }}</td>
											<td>{{ $order->shippingAddress->phone }}</td>
										</tr>
										<tr>
											<td>{{ trans('address.state') }}</td>
											<td>{{ $order->shippingAddress->state_name }}</td>
										</tr>
										<tr>
											<td>{{ trans('address.country') }}</td>
											<td>{{ isset($order->shippingAddress->country_name)  }}</td>
										</tr>
										<tr>
											<td>{{ trans('address.comment') }}</td>
											<td>{{ $order->shippingAddress->comment }}</td>
										</tr>
									</table>
	                            </div>
	                            <div class="tab-pane" id="tab-billing-info">
									
									@if ($order->billingAddress)
										<h4>{{ trans('order.billing_address') }}</h4>
										<table class="table table-condensed table-hover">
											<tr>
												<td>{{ trans('address.contact_person') }}</td>
												<td>{{ $order->billingAddress->name }}</td>
											</tr>
											{{--<tr>
												<td>{{ trans('address.address') }}</td>
												<td>
													{{ $order->billingAddress->address1 }} <br>
													{{ $order->billingAddress->address2 }}
												</td>
											</tr>--}}
											<tr>
											<td>Unit no. / Room no. </td>
											<td>{{ $order->billingAddress->unit }}</td>
										</tr>
										<tr>
											<td>Building </td>
											<td>{{ $order->billingAddress->building }}</td>
										</tr>
										<tr>
											<td>Street </td>
											<td>{{ $order->billingAddress->street }}</td>
										</tr>
										<tr>
											<td>Barangay </td>
											<td>{{ $order->billingAddress->barangay }}</td>
										</tr>
										<tr>
											<td>Province / {{ trans('address.county') }} </td>
											<td>{{ $order->billingAddress->county }}</td>
										</tr>
										<tr>
											<td>{{ trans('address.city') }}</td>
											<td>{{ $order->billingAddress->city }}</td>
										</tr>
										<tr>
											<td>{{ trans('address.mobile_phone') }}</td>
											<td>{{ $order->billingAddress->mobile_phone }}</td>
										</tr>
										<tr>
											<td>{{ trans('address.postal_code') }}</td>
											<td>{{ $order->billingAddress->postal_code }}</td>
										</tr>
										<tr>
											<td>{{ trans('address.phone') }}</td>
											<td>{{ $order->shippingAddress->phone }}</td>
										</tr>
										<tr>
											<td>{{ trans('address.state') }}</td>
											<td>{{ $order->billingAddress->state_name }}</td>
										</tr>
										<tr>
											<td>{{ trans('address.country') }}</td>
											<td>{{ $order->billingAddress->country_name }}</td>
										</tr>
										<tr>
											<td>{{ trans('address.comment') }}</td>
											<td>{{ $order->shippingAddress->comment }}</td>
										</tr>
										</table>
									@endif
	                            </div>
	                        </div>
                        @endif
                    </div>
			    </div>
		    </div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="box">
			    <div class="box-header with-border">
			      <h3 class="box-title">
		            <span><i class="fa fa-truck"></i> {{ trans('carrier.carrier') }}</span>
		          </h3>
			    </div>

			    <div class="box-body">
					<table class="table table-condensed">
						<thead>
							<tr>
								<th style="width: 150px;">{{ trans('carrier.logo') }}</th>
								<th>{{ trans('carrier.carrier') }}</th>
								<th>{{ trans('carrier.price') }}</th>
								<th>{{ trans('carrier.delivery_text') }}</th>
							</tr>
						</thead>
						<tr>
							<td class="vertical-align-middle">
								<img src="{{ Storage::disk('carriers')->exists($order->carrier->logo) ? (url(config('filesystems.disks.carriers.simple_path')).'/'.$order->carrier->logo) : (url(config('filesystems.disks.carriers.simple_path')).'/default.png') }}" alt="" style="width: 100px;">
							</td>
							<td class="vertical-align-middle">{{ $order->carrier->name }}</td>
							<td class="vertical-align-middle">{!! $order->currency->symbol!!}{{ $order->total_shipping}}</td>
							<td class="vertical-align-middle">{{ $order->carrier->delivery_text }}</td>
						</tr>
					</table>
			    </div>
		    </div>
	    </div>
    </div>

	<div class="row">
		<div class="col-md-12">
			<div class="box">
			    <div class="box-header with-border">
			      <h3 class="box-title">
		            <span><i class="fa fa-shopping-cart"></i> {{ trans('product.products') }}</span>
		          </h3>
			    </div>

			    <div class="box-body">
			    	<div class="col-md-12">
				    	<table class="table table-striped table-hover display responsive nowrap">
							<thead>
								<tr>
									<th>{{ trans('product.product') }}</th>
									<th>{{ trans('product.price') }}</th>
									<th>{{ trans('product.discount') }} </th>
									<th>{{ trans('order.amount_per_item') }}</th>
									<th>{{ trans('order.quantity') }}</th>
									<th class="text-right">Subtotal</th>
								</tr>
							</thead>
							<tbody>
								@php
									$pruduct_sub_total  = 0;
								@endphp
								@foreach($order->products as $product)
									{{ $subtotal = decimalFormat($pruduct_sub_total += $product->pivot->amount_per_item*$product->pivot->quantity)}}
									<tr>
										<td class="vertical-align-middle">
											<a href="{{ route('crud.product.edit', $product->pivot->product_id) }}">{{ $product->pivot->name }}</a><br/>
											<span class="font-12">SKU: {{ $product->pivot->sku }}</span>
										</td>
										<td class="vertical-align-middle">
											
											{!! $order->currency->symbol!!} {{decimalFormat($product->pivot->price) }}
											
										</td>
										<td class="vertical-align-middle">
											@if($product->pivot->specific_price_discount_type == "Percent")

												{{decimalFormat($product->pivot->specific_price_reduction)  . '%'}}

											@else
												{!! $order->currency->symbol!!} {{decimalFormat($product->pivot->specific_price_reduction) }}
											@endif
											
										</td>
										<td class="vertical-align-middle">
											{!! $order->currency->symbol!!} {{ decimalFormat($product->pivot->amount_per_item)}}
										</td>
										<td class="vertical-align-middle">{{ $product->pivot->quantity }}</td>
										<td class="vertical-align-middle text-right">
											{!! $order->currency->symbol!!}  {{decimalFormat($product->pivot->amount_per_item*$product->pivot->quantity)}}
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>

					<div class="col-md-6 col-md-offset-6">
						<table class="table table-condensed">
							<tr>
								<td class="text-right"><strong><!--{{ trans('common.total') }}:--> </strong></td>
								<td class="text-right"><strong>{!! $order->currency->symbol!!}  {{ decimalFormat($pruduct_sub_total) }}</strong></td>
							</tr>
						</table>
					</div>

			    </div>
		    </div>
		</div>
	</div>
	@if(count($order->order_cart_rule))
	<div class="row">
		<div class="col-md-12">
			<div class="box">
			    <div class="box-header with-border">
			      <h3 class="box-title">
		            <span><i class="fa fa-tag"></i> Cart Rules </span>
		          </h3>
			    </div>

			    <div class="box-body">
			    	<div class="col-md-12">
				    	<table class="table table-striped table-hover display responsive nowrap">
							<thead>
								<tr>
									<th>{{ trans('cartrule.name') }}</th>
									<th>{{ trans('cartrule.discount_type') }}</th>
									<th>{{ trans('cartrule.reduction_amount') }} </th>
									<th>{{ trans('cartrule.free_delivery') }}</th>
									<th class="text-right">Subtotal</th>
								</tr>
							</thead>
							<tbody>
								@foreach($order->order_cart_rule as $cart_rule)

									<tr>
										<td class="vertical-align-middle">
											<a href="{{ route('crud.cart-rules.edit', $cart_rule->cart_rule_id) }}">{{ $cart_rule->name}}</a><br/>
											<span class="font-12">CODE: {{ $cart_rule->code }} </span>
										</td>

										<td class="vertical-align-middle">
											{{$cart_rule->discount_type}}
										</td>
										<td class="vertical-align-middle">
											@if($cart_rule->discount_type == "Percent - order" || $cart_rule->discount_type == "Percent - selected products" )

												{{decimalFormat($cart_rule->reduction_amount) . '%'}}

											@else
												{!! $order->currency->symbol !!} {{decimalFormat($cart_rule->reduction_amount) }}
											@endif
										</td>
										<td class="vertical-align-middle">
											{{ $cart_rule->free_delivery ? 'Yes' : 'No' }}
										</td>
										<td class="vertical-align-middle text-right">
											{!! $order->currency->symbol!!} {{ decimalFormat($cart_rule->total_discount)}}
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="col-md-6 col-md-offset-6">
						<table class="table table-condensed">
							<tr>
								<td class="text-right"><strong><!--{{ trans('common.total') }}:--> </strong></td>
								<td class="text-right"><strong>{!! $order->currency->symbol!!}  {{$order->total_discount}} </strong></td>
							</tr>
						</table>
					</div>
			    </div>
			</div>
		</div>
	</div>
	
	@endif
	<div class="row">
		<div class="col-md-12">
			<div class="box">
			    <div class="box-header">
			      <h3 class="box-title">
		            <span><i class="fa fa-money"></i> Total</span>
		          </h3>
			    </div>
			    <div class="box-body">
			    	<div class="col-md-6 col-md-offset-6">
						<table class="table table-condensed">
							<tr>
								<td class="text-right"><strong>Subtotal: </strong></td>
								<td class="text-right">{!! $order->currency->symbol!!}  {{$subtotal}} </td>
							</tr>
							<tr>
								<td class="text-right"><strong>Shipping Fee: </strong></td>
								<td class="text-right">{!! $order->currency->symbol!!}  {{$order->total_shipping}} </td>
							</tr>
							<tr>
								<td class="text-right"><strong>Total Discount: </strong></td>
								<td class="text-right">{!! $order->currency->symbol!!}  {{$order->total_discount}} </td>
							</tr>
							<tr>
								<td class="text-right"><strong>Total: </strong></td>
								<td class="text-right"><strong>{!! $order->currency->symbol!!}  {{$order->total}} </strong></td>
							</tr>
						</table>
					</div>
			    </div>

			</div>
		</div>
	</div>
@endsection


@section('after_styles')
	<link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/crud.css') }}">
	<link rel="stylesheet" href="{{ asset('vendor/backpack/crud/css/show.css') }}">

	<!-- include select2 css-->
	<link href="{{ asset('vendor/adminlte/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('after_scripts')
	<script src="{{ asset('vendor/backpack/crud/js/crud.js') }}"></script>
	<script src="{{ asset('vendor/backpack/crud/js/show.js') }}"></script>

	<!-- include select2 js -->
    <script src="{{ asset('vendor/adminlte/bower_components/select2/dist/js/select2.min.js') }}"></script>

	<script>
		$(document).ready(function () {
			@if (count($orderStatuses) > 0)
				$('.select2_field').select2({
                            theme: "bootstrap"
                        });
			@endif
		});
	</script>
@endsection