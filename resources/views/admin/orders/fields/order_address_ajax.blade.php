<!-- select2 from ajax -->
@php
    $connected_entity = new $field['model'];
    $connected_entity_key_name = $connected_entity->getKeyName();
    $old_value = old($field['name']) ? old($field['name']) : (isset($field['value']) ? $field['value'] : (isset($field['default']) ? $field['default'] : false ));
@endphp

<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    <?php $entity_model = $crud->model; ?>
    <select
        name="{{ $field['name'] }}"
        style="width: 100%"
        id="select2_ajax_{{ $field['name'] }}"
        @include('crud::inc.field_attributes', ['default_class' =>  'form-control'])
        >

        @if ($old_value)
            @php
                $item = $connected_entity->find($old_value);
            @endphp
            @if ($item)

            {{-- allow clear --}}
            @if ($entity_model::isColumnNullable($field['name']))
            <option value="" selected>
                {{ $field['placeholder'] }}
            </option>
            @endif

            <option value="{{ $item->getKey() }}" selected>
                {{ $item->{$field['attribute']} }}
            </option>
            @endif
        @endif
    </select>
    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>
@foreach ($field['columns'] as $column)
    @if(view()->exists('vendor.backpack.crud.fields.'.$column['type']))
        @include('vendor.backpack.crud.fields.'.$column['type'], array('field' => $column))
    @else
        @include('crud::fields.'.$column['type'], array('field' => $column))
    @endif
@endforeach


{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
    <!-- include select2 css-->
    <link href="{{ asset('vendor/adminlte/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    {{-- allow clear --}}
    @if ($entity_model::isColumnNullable($field['name']))
    <style type="text/css">
        .select2-selection__clear::after {
            content: ' {{ trans('backpack::crud.clear') }}';
        }
    </style>
    @endif
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
    <!-- include select2 js-->
    <script src="{{ asset('vendor/adminlte/bower_components/select2/dist/js/select2.min.js') }}"></script>
    @endpush

@endif

<!-- include field specific select2 js-->
@push('crud_fields_scripts')
<script>
    jQuery(document).ready(function($) {
        // trigger select2 for each untriggered select2 box
        var address = [];
        if($("#select2_ajax_{{ $field['name'] }}").val() > 0){
            inputEnableDisable(true);
        }
        $("#select2_ajax_{{ $field['name'] }}").each(function (i, obj) {
            if (!$(obj).hasClass("select2-hidden-accessible"))
            {

                $(obj).select2({
                    theme: 'bootstrap',
                    multiple: false,
                    placeholder: "{{ $field['placeholder'] }}",
                    minimumInputLength: "{{ $field['minimum_input_length'] }}",

                    {{-- allow clear --}}
                    @if ($entity_model::isColumnNullable($field['name']))
                    allowClear: true,
                    @endif
                    ajax: {
                        url: "{{ $field['data_source'] }}",
                        dataType: 'json',
                        quietMillis: 250,
                        data: function (params) {
                            user_id = $("#user_id").val();
                            return {
                                q: params.term, // search term
                                user_id: user_id,
                                page: params.page
                            };
                        },

                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            var result = {
                                results: $.map(data.data, function (item) {
                                    textField = "{{ $field['attribute'] }}";
                                    textField2 = "{{ $field['attribute2'] }}"
                                    console.log(item["{{ $connected_entity_key_name }}"]);
                                    return {
                                        text: item[textField] + ", " + item[textField2],
                                        id: item["{{ $connected_entity_key_name }}"],
                                    }
                                }),
                                more: data.current_page < data.last_page
                            };
                            address = data.data;
                            return result;
                        },
                        cache: true
                    },
                })
                {{-- allow clear --}}
                @if ($entity_model::isColumnNullable($field['name']))
                .on('select2:unselecting', function(e) {
                    inputEnableDisable(false);
                    
                     // $("#{{ $field['pivot_name'] }}_pivot_address1").val('').trigger('change').prop('disabled', false);
                     // $("#{{ $field['pivot_name'] }}_pivot_name").val('').trigger('change').prop('disabled', false);
                     // $("#{{ $field['pivot_name'] }}_pivot_city").val('').trigger('change').prop('disabled', false);
                     // $("#{{ $field['pivot_name'] }}_pivot_postal_code").val('').trigger('change').prop('disabled', false);
                     // $("#{{ $field['pivot_name'] }}_pivot_mobile_phone").val('').trigger('change').prop('disabled', false);
                     // $("#{{ $field['pivot_name'] }}_pivot_phone").val('').trigger('change').prop('disabled', false);
                     // $("#{{ $field['pivot_name'] }}_pivot_comment").val('').trigger('change').prop('disabled', false);
                     // $("#{{ $field['pivot_name'] }}_pivot_country").val('').trigger('change').prop('disabled', false);
                    $(this).val('').trigger('change');
                    e.preventDefault();
                })
                @endif

            }
        });
        $("#select2_ajax_{{ $field['name'] }}").on('change', function() {
             var index = address.findIndex(x => x.id==this.value);
             if(index >= 0 ) {
                $("#{{ $field['pivot_name'] }}_pivot_email").val(address[index].email).trigger('change').prop('disabled', true);
                $("#{{ $field['pivot_name'] }}_pivot_county").val(address[index].county).trigger('change').prop('disabled', true);
                $("#{{ $field['pivot_name'] }}_pivot_country").val(address[index].country_id).trigger('change').prop('disabled', true);
                 $("#{{ $field['pivot_name'] }}_pivot_unit").val(address[index].unit).trigger('change').prop('disabled', true);
                 $("#{{ $field['pivot_name'] }}_pivot_building").val(address[index].building).trigger('change').prop('disabled', true);
                 $("#{{ $field['pivot_name'] }}_pivot_street").val(address[index].street).trigger('change').prop('disabled', true);
                 $("#{{ $field['pivot_name'] }}_pivot_barangay").val(address[index].barangay).trigger('change').prop('disabled', true);
                 $("#{{ $field['pivot_name'] }}_pivot_region").val(address[index].region).trigger('change').prop('disabled', true);
                 $("#{{ $field['pivot_name'] }}_pivot_name").val(address[index].name).trigger('change').prop('disabled', true);
                 $("#{{ $field['pivot_name'] }}_pivot_city").val(address[index].city).trigger('change').prop('disabled', true);
                 $("#{{ $field['pivot_name'] }}_pivot_postal_code").val(address[index].postal_code).trigger('change').prop('disabled', true);
                 $("#{{ $field['pivot_name'] }}_pivot_mobile_phone").val(address[index].mobile_phone).trigger('change').prop('disabled', true);
                 $("#{{ $field['pivot_name'] }}_pivot_phone").val(address[index].phone).trigger('change').prop('disabled', true);
                 $("#{{ $field['pivot_name'] }}_pivot_comment").val(address[index].comment).trigger('change').prop('disabled', true);
             }
             
            // console.log(address[index].country_id)
              console.log($("#select2_ajax_{{ $field['name'] }}"));
            //  console.log($("#{{ $field['pivot_name'] }}_pivot_country"));
            // console.log(index)
        });

        function inputEnableDisable(boolean) {
            $("#{{ $field['pivot_name'] }}_pivot_email").prop('disabled', boolean);
            $("#{{ $field['pivot_name'] }}_pivot_county").prop('disabled', boolean);
            $("#{{ $field['pivot_name'] }}_pivot_unit").prop('disabled', boolean);
            $("#{{ $field['pivot_name'] }}_pivot_building").prop('disabled', boolean);
            $("#{{ $field['pivot_name'] }}_pivot_street").prop('disabled', boolean);
            $("#{{ $field['pivot_name'] }}_pivot_barangay").prop('disabled', boolean);
            $("#{{ $field['pivot_name'] }}_pivot_region").prop('disabled', boolean);
            $("#{{ $field['pivot_name'] }}_pivot_name").prop('disabled', boolean);
            $("#{{ $field['pivot_name'] }}_pivot_city").prop('disabled', boolean);
            $("#{{ $field['pivot_name'] }}_pivot_postal_code").prop('disabled', boolean);
            $("#{{ $field['pivot_name'] }}_pivot_mobile_phone").prop('disabled', boolean);
            $("#{{ $field['pivot_name'] }}_pivot_phone").prop('disabled', boolean);
            $("#{{ $field['pivot_name'] }}_pivot_comment").prop('disabled', boolean);
            $("#{{ $field['pivot_name'] }}_pivot_country").prop('disabled', boolean);
        }

    });
</script>
@endpush
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
