<!-- number input -->
<div class="input-group">
    @if(isset($field['currency']))
        <span class="input-group-addon setCurrency"></span>
    @endif
         <input
            ng-init="item.stock = item.stock + (item.{{ $field['name'] }} ? item.{{ $field['name'] }} : item.pivot.{{ $field['name'] }})"
            type="number"
            ng-if="item.pivot"
            ng-model="item.pivot.{{ $field['name'] }}"
            ng-change="checkMax($event,item)"
            ng-value="item.{{ $field['name'] }} ? item.{{ $field['name'] }} : item.pivot.{{ $field['name'] }}"
            @include('crud::inc.field_attributes')
            >
         <input
            type="number"
            ng-change="checkMax($event,item)"
            ng-if="!item.pivot"
            ng-model="item.{{ $field['name'] }}"
            ng-value="item.{{ $field['name'] }} ? item.{{ $field['name'] }} : item.pivot.{{ $field['name'] }}"
            @include('crud::inc.field_attributes')
            >
        @if(isset($field['suffix'])) <div class="input-group-addon">{!! $field['suffix'] !!}</div> @endif

    @if(isset($field['prefix']) || isset($field['suffix'])) </div> @endif
    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>
