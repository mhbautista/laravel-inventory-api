<!-- number input -->
<div class="input-group">
    <span ng-show="item.discount_type == 'Amount - order' || item.discount_type == 'Amount - selected products'" class="input-group-addon setCurrency"></span>
    @if(isset($field['prefix']) || isset($field['suffix'])) <div class="input-group"> @endif
        @if(isset($field['prefix'])) <div class="input-group-addon">{!! $field['prefix'] !!}</div> @endif

         <input
            ng-value="item.{{ $field['name'] }} + (item.discount_type=='Percent - selected products' || item.discount_type=='Percent - order' ? '%': '')"
            @include('crud::inc.field_attributes')
            >
        @if(isset($field['suffix'])) <div class="input-group-addon">{!! $field['suffix'] !!}</div> @endif

    @if(isset($field['prefix']) || isset($field['suffix'])) </div> @endif

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>

@if (!$crud->child_resource_included['number'])

    @push('crud_fields_styles')
        <style>
            .table input[type='number'] { text-align: right; padding-right: 5px; }
        </style>
    @endpush

    <?php $crud->child_resource_included['number'] = true; ?>
@endif