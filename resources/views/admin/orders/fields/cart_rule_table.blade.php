<!-- array input -->

<?php
    $max = isset($field['max']) && (int) $field['max'] > 0 ? $field['max'] : -1;
    $min = isset($field['min']) && (int) $field['min'] > 0 ? $field['min'] : -1;
    $item_name = strtolower(isset($field['entity_singular']) && !empty($field['entity_singular']) ? $field['entity_singular'] : $field['label']);

    $items = old($field['name']) ? (old($field['name'])) : (isset($field['value']) ? ($field['value']) : (isset($field['default']) ? ($field['default']) : '' ));
    if(is_array(json_decode($items))){
        
        if(count($items)){
            if(json_last_error() !== JSON_ERROR_NONE){
                $items = json_encode($items);
            }

            foreach ($items as $item) {
                $item->cart_rule->products->makeHidden('pivot');
                $item->cart_rule->customers->makeHidden('pivot');

            // $item->cart_rule->products->where('product_id',2)->count();
            // echo 'yahoo'. $item->order;
            // echo json_encode($item->cart_rule->products);
            // foreach (json_decode($item->cart_rule->products) as $cart) {
            //     echo json_encode($cart);
            // }
            }
        }
        

       //     $tmp_array = []; 
       // foreach ($items as $item) {
       //      $item->cart_rule;
       //      $obj1 = new \stdClass;
       //      foreach (json_decode($item->cart_rule) as $key=>$value) {
       //              $obj1->$key = $value;
       //      }
       //      $tmp_array [] = $obj1;
       //  } 
       //  $items = $tmp_array;
    }
    
    if (is_array($items)) {
        if (count($items)) {
            $items = json_encode($items);
        } else {
            $itemss = '[]';
        }
    } elseif (is_string($items) && !is_array(json_decode($items))) {
        $items = '[]';
    }

?>
<div ng-controller="cartRuleController" @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    @include('crud::inc.field_translatable_icon')
    <input class="array-json" type="hidden" id="{{ $field['name'] }}" name="{{ $field['name'] }}">

    <div class="array-container form-group">

        <table class="table table-bordered table-striped m-b-0" ng-init="field = '#{{ $field['name'] }}'; items = {{ $items }}; max = {{$max}}; min = {{$min}}; maxErrorTitle = '{{trans('backpack::crud.table_cant_add', ['entity' => $item_name])}}'; maxErrorMessage = '{{trans('backpack::crud.table_max_reached', ['max' => $max])}}'">
            <thead>
                <tr>

                    @foreach( $field['columns'] as $column )
                    <th style="font-weight: 600!important;">
                        {{ $column['label'] }}
                    </th>
                    @endforeach
                    <th style="font-weight: 600!important;"> 
                        Subtotal Discount
                    </th>
                    <th class="text-center" ng-if="max == -1 || max > 1"> Priority {{-- <i class="fa fa-sort"></i> --}} </th>
                    <th class="text-center" ng-if="max == -1 || max > 1"> {{-- <i class="fa fa-trash"></i> --}} </th>
                </tr>
            </thead>

            <tbody ui-sortable="sortableOptions" ng-model="items" class="table-striped">
                <tr ng-repeat="item in items | index track by $index" class="array-row" ><!-- style="pointer-events:none" -->
                    @foreach ($field['columns'] as $column)
                        <td  
                             class="
                                @if(isset($column['size']))  
                                    col-md-{{ $column['size'] }}
                                @endif
                                "
                            >
                        <!-- load the view from the application if it exists, otherwise load the one in the package -->
                        @if(view()->exists('vendor.backpack.crud.fields.'.$column['type']))
                            @include('vendor.backpack.crud.fields.'.$column['type'], array('field' => $column))
                        @elseif(view()->exists('admin.orders.fields.'.$column['type']))
                            @include('admin.orders.fields.'.$column['type'], array('field' => $column))
                        @else
                            @include('crud::fields.'.$column['type'], array('field' => $column))
                        @endif
                        </td>
                    @endforeach
                    <td > 
                        <div  class="input-group">
                             <span class="input-group-addon setCurrency"></span>
                            <input ng-if="!item.isError" ng-model="item.subtotal_discount" ng-value="item.subtotal_discount" type="text" readonly="1" ng-pattern="/^[1-9][0-9]{0,2}(?:,?[0-9]{3}){0,3}(?:\.[0-9]{1,2})?$/" step="0.01" class="form-control ng-pristine ng-valid ng-empty ng-valid-pattern ng-touched">
                            <button ng-if="item.isError" type="button" class="btn btn-danger form-control" data-toggle="modal" data-target="#modal-danger">Not Applicable
                        </button>
                        </div>
                        
                     </td>
                    <td ng-if="max == -1 || max > 1" > 
                        <input ng-model="item.priority" type="hidden" ng-value="$index">
                        <span ng-hide="min > -1 && $index < min" class="btn btn-sm btn-default sort-handle"><span class="sr-only">sort item</span><i class="fa fa-sort" role="presentation" aria-hidden="true"></i></span>
                    </td>
                    <td ng-if="max == -1 || max > 1">
                        <button ng-hide="min > -1 && $index < min" class="btn btn-sm btn-default" type="button" ng-click="removeItem(item);"><span class="sr-only">delete item</span><i class="fa fa-trash" role="presentation" aria-hidden="true"></i></button>
                    </td>
                </tr>
            </tbody>

        </table>

        <div class="array-controls btn-group m-t-10">
            <button ng-if="max == -1 || items.length < max" class="btn btn-sm btn-default" type="button" ng-click="addItem()"><i class="fa fa-plus"></i> {{trans('backpack::crud.add')}} {{ $item_name }}</button>
        </div>
    </div>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>

{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
    {{-- @push('crud_fields_styles')
        {{-- YOUR CSS HERE --}}
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
        {{-- YOUR JS HERE --}}
        <script>
            var cartRules = [];
            angularApp = window.angularApp || angular.module('backPackTableApp', ['ui.sortable','orderByFilter'], function(){
            }).directive('stringToNumber', function() {
              return {
                require: 'ngModel',
                link: function(scope, element, attrs, ngModel) {
                  ngModel.$parsers.push(function(value) {
                    return '' + value;
                  });
                  ngModel.$formatters.push(function(value) {
                    return parseFloat(value, 10);
                  });
                }
              };
            });
            angularApp.controller('cartRuleController', function($scope, orderByFilter, $http, ProductData){
                $scope.sortableOptions = {
                    handle: '.sort-handle',
                    axis: 'y',
                    start: function(e,ui){
                    },
                    stop: function(e,ui){
                         $scope.computeTotalCartRuleDiscount($scope.items);
                    },
                    helper: function(e, ui) {
                        ui.children().each(function() {
                            $(this).width($(this).width());
                        });
                        return ui;
                    },
                };
                $scope.cart_rules = [];
                $scope.addItem = function(){

                    if( $scope.max > -1 ){
                        if( $scope.items.length < $scope.max ){
                            var item = {};
                            $scope.items.push(item);
                        } else {
                            new PNotify({
                                title: $scope.maxErrorTitle,
                                text: $scope.maxErrorMessage,
                                type: 'error'
                            });
                        }
                    }
                    else {
                        var item = {};
                        $scope.items.push(item);
                    }
                }
                $scope.filterCartRuleUsed = function (){
                    cartRules = [];
                    angular.forEach($scope.items, function(item){
                                cartRules.push(item.cart_rule_id);
                                    });

                }
                $scope.removeItem = function(item){
                    var index = $scope.items.indexOf(item);
                    if(typeof item.cart_rule_id != "undefined"){
                        
                        var cart_rule_pos = $scope.cart_rules.map(function(e) { return e.id ? e.id : e.cart_rule_id; }).indexOf(parseInt(item.cart_rule_id));
                        $scope.cart_rules.splice(cart_rule_pos, 1);
                        cartRules.splice(cartRules.indexOf(item.cart_rule_id),1);
                        $scope.items.splice(index, 1);

                    }else{
                        $scope.items.splice(index, 1); 
                    }
                    $scope.computeTotalCartRuleDiscount($scope.items);
                   
                    
                }
                $scope.setCartRuleOnChange= function(index, item){
                    var isConditionOk = false;
                    var data = $("#select2_ajax_"+index).select2("data")[0].data;
                    if(typeof data=='undefined'){
                        $scope.items.splice(index, 1);
                        return;
                    }
                    if(typeof $scope.cart_rules[index] == 'undefined'){
                        $scope.cart_rules[index] = [];
                    }
                    if($scope.cart_rules[index].indexOf(data) < 0 ){
                       $scope.cart_rules[index].push(data);
                    }
                    $scope.items[index].name = data.name;
                    $scope.items[index].code = data.code;
                    $scope.items[index].cart_rule_id = data.id;;
                    $scope.items[index].reduction_amount = data.reduction_amount;
                    $scope.items[index].discount_type = data.discount_type;
                    $scope.items[index].customers = data.customers;
                    $scope.items[index].products = data.products;
                    $scope.items[index].start_date = data.start_date;
                    $scope.items[index].expiration_date = data.expiration_date;
                    $scope.items[index].reduction_currency_id = data.reduction_currency_id;
                    $scope.items[index].gift_product_id = data.gift_product_id;
                    $scope.items[index].free_delivery = data.free_delivery;

                    $http({
                            url: "/ajax/cart/rule/"+data.id,
                            method: "GET",
                            params:{
                                order_id: parseInt({!! json_encode(isset($entry->id) ? $entry->id : 0) !!}),
                                user_id: $("#user_id").val(),
                                products: angular.toJson(ProductData.getProductData())
                            },
                            headers: {
                                "Content-Type": "application/json"
                            }

                        }).success(function (response) {
                            $scope.items[index].isCondition = response.data.isCondition;
                            $scope.items[index].isError = response.data.isError;
                            $scope.items[index].subtotal_discount = response.data.subtotal_discount ? response.data.subtotal_discount  : 0;
                           $scope.computeTotalCartRuleDiscount($scope.items);

                        }).error(function (response) {
                            console.log("failed");
                            }
                        );
                     $scope.filterCartRuleUsed();
                    delete $scope.items[index].cart_rule;

                   
                }
                $scope.setCartRule = function(index, item){
                    item.cart_rule = item.cart_rule ? item.cart_rule : item;
                    $scope.items[index].reduction_amount = item.cart_rule.reduction_amount;
                    $scope.items[index].cart_rule_id = item.cart_rule.cart_rule_id ? item.cart_rule.cart_rule_id : item.cart_rule.id;
                    $scope.items[index].name = item.cart_rule.name;
                    $scope.items[index].code = item.cart_rule.code;
                    $scope.items[index].discount_type = item.cart_rule.discount_type;
                    $scope.items[index].customers = item.cart_rule.customers;
                    $scope.items[index].products = item.cart_rule.products;
                    $scope.items[index].start_date = item.cart_rule.start_date;
                    $scope.items[index].expiration_date = item.cart_rule.expiration_date;
                    $scope.items[index].reduction_currency_id = item.cart_rule.reduction_currency_id;
                    $scope.items[index].gift_product_id = item.cart_rule.gift_product_id;
                    $scope.items[index].free_delivery = item.cart_rule.free_delivery;
                    $http({
                            url: "/ajax/cart/rule/"+ (item.cart_rule.cart_rule_id ? item.cart_rule.cart_rule_id : item.cart_rule.id),
                            method: "GET",
                            params:{
                                order_id: parseInt({!! json_encode(isset($entry->id) ? $entry->id : 0) !!}),
                                user_id: $("#user_id").val(),
                                products: angular.toJson(ProductData.getProductData())
                            },
                            headers: {
                                "Content-Type": "application/json;  charset=UTF-8"
                            }

                        }).success(function (response) {
                            $scope.items[index].isCondition = response.data.isCondition;
                            $scope.items[index].isError = response.data.isError;
                            $scope.items[index].subtotal_discount = response.data.subtotal_discount ? response.data.subtotal_discount  : 0;

                            $scope.computeTotalCartRuleDiscount($scope.items);

                        
                        }).error(function (response) {
                            console.log("failed");
                            }
                        );
                    if(typeof $scope.cart_rules[index] == 'undefined'){
                        $scope.cart_rules[index] = [];
                    }
                    var cart_rule_pos = $scope.cart_rules[index].map(function(e) { return e.id ? e.id : e.cart_rule_id; }).indexOf(item.cart_rule.cart_rule_id ? item.cart_rule.cart_rule_id : item.cart_rule.id)
                    //$scope.cart_rules[index].indexOf(item.cart_rule) < 0 
                    if(cart_rule_pos < 0){
                       $scope.cart_rules[index].push(item.cart_rule);
                    }
                       $scope.filterCartRuleUsed();
                     delete $scope.items[index].cart_rule;
                }
                $scope.reComputeTotalCartRuleDiscount = function(cart_rules){
                    angular.forEach(orderByFilter(cart_rules, 'index', false), function(value, key) {

                        if(typeof value.index != 'undefined'){
                            $scope.setCartRule(value.index, value);
                        }
                    });

                }
                $scope.computeTotalCartRuleDiscount = function(cart_rules){
              
                    var tmpTotalCartRuleDiscount = 0;
                    angular.forEach(orderByFilter(cart_rules, 'index', false), function(value, key) {
                        
                        if(typeof value.subtotal_discount !== "undefined"){
                       
                        switch (value.discount_type) {
                            case 'Amount - order':
                                 tmpTotalCartRuleDiscount += parseFloat(value.subtotal_discount);
                                break;
                            case 'Percent - order':
                            var computed_reduction = parseFloat(((value.reduction_amount / 100) * ((ProductData.getTotalProductsPrice() - tmpTotalCartRuleDiscount).toFixed(2))));
                            tmpTotalCartRuleDiscount += computed_reduction;
                            var current_position = $scope.items.map(function(e) { return e.index}).indexOf(value.index);
                                $scope.items[current_position].subtotal_discount = computed_reduction;
                                break;
                            case 'Percent - selected products':
                                tmpTotalCartRuleDiscount +=  parseFloat(value.subtotal_discount);
                                break;
                            default://Amount - selected products
                                tmpTotalCartRuleDiscount +=  parseFloat(value.subtotal_discount);

                        }
                        }
                    });
                    var shipping_fee = parseFloat($("[name='total_shipping']").val() ? $("[name='total_shipping']").val() : 0);
                    var total_discount = ( parseFloat(ProductData.getTotalProductsPrice()) + shipping_fee - tmpTotalCartRuleDiscount).toFixed(2);

                    $("[name='total_discount']").val(tmpTotalCartRuleDiscount > 0 ? tmpTotalCartRuleDiscount : 0);
                    $("[name='total']").val(total_discount > 0 ? total_discount : 0);
                }
                // $scope.items.ProductData = ProductData.getTotalProductsPrice();
                $scope.$watch('items', function(a, b){
                    if( $scope.min > -1 ){
                        while($scope.items.length < $scope.min){
                            $scope.addItem();
                        }
                    }

                    if( typeof $scope.items != 'undefined' ){

                        if( typeof $scope.field != 'undefined'){
                            if( typeof $scope.field == 'string' ){
                                $scope.field = $($scope.field);
                            }
                            $scope.field.val( $scope.items.length ? angular.toJson($scope.items) : null );
                        }
                    }
                    
                }, true);
                $scope.$watch(function(){
                   return ProductData.getProductData();
                }, function(newValue, oldValue){
                     $scope.reComputeTotalCartRuleDiscount($scope.items);
                },true);

                if( $scope.min > -1 ){
                    for(var i = 0; i < $scope.min; i++){
                        $scope.addItem();
                    }
                }
            });

            

            // angular.element(document).ready(function(){
            //     angular.forEach(angular.element('[ng-app]'), function(ctrl){
            //         var ctrlDom = angular.element(ctrl);
            //         if( !ctrlDom.hasClass('ng-scope') ){
            //             angular.bootstrap(ctrl, [ctrlDom.attr('ng-app')]);
            //         }
            //     });
            // })
            angularApp.factory('ProductData', function () {

                var product_data = [];
                var total_produdcts_price = 0;

                return {
                    getProductData: function () {
                        return product_data;
                    },
                    setProductData: function (ProductDataList) {
                        product_data = ProductDataList;
                    },

                    getTotalProductsPrice: function () {
                        return total_produdcts_price;
                    },

                    setTotalProductsPrice: function (price) {
                        total_produdcts_price = price;
                    }
                };
            });


        </script>

    @endpush
@endif
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
