<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.
Route::group([
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
	'prefix' => 'ajax',
  	'namespace' => 'App\Http\Controllers\Admin'
], function() {
	// Get attributes by set id
	Route::post('attribute-sets/list-attributes', ['as' => 'getAttrBySetId', 'uses' => 'AttributeSetCrudController@ajaxGetAttributesBySetId']);

	// Product images upload routes
	Route::post('product/image/upload', ['as' => 'uploadProductImages', 'uses' => 'ProductCrudController@ajaxUploadProductImages']);
	Route::post('product/image/reorder', ['as' => 'reorderProductImages', 'uses' => 'ProductCrudController@ajaxReorderProductImages']);
	Route::post('product/image/delete', ['as' => 'deleteProductImage', 'uses' => 'ProductCrudController@ajaxDeleteProductImage']);

	// Get group products by group id
	Route::post('product-group/list/products', ['as' => 'getGroupProducts', 'uses' => 'ProductGroupController@getGroupProducts']);
	Route::post('product-group/list/ungrouped-products', ['as' => 'getUngroupedProducts', 'uses' => 'ProductGroupController@getUngroupedProducts']);
	Route::post('product-group/add/product', ['as' => 'addProductToGroup', 'uses' => 'ProductGroupController@addProductToGroup']);
	Route::post('product-group/remove/product', ['as' => 'removeProductFromGroup', 'uses' => 'ProductGroupController@removeProductFromGroup']);

	// Client address

	Route::get('client/specific/addresses', ['as' => 'searchSpeficAddress', 'uses' => 'ClientAddressController@getAddress']);
	Route::get('client/specific/addresses/{id}', ['as' => 'getSpeficAddress', 'uses' => 'ClientAddressController@show']);

	// Specific Cart rule
	Route::get('cart/rule', ['as' => 'searchSpeficCartRule', 'uses' => 'CartRuleCrudController@getSpecificCartRule']);
	Route::get('cart/rule/{id}', ['as' => 'getSpeficCartRule', 'uses' => 'CartRuleCrudController@setCartRule']);
	// Route::post('client/list/addresses', ['as' => 'getClientAddresses', 'uses' => 'ClientAddressController@getClientAddresses']);
	// Route::post('client/add/address', ['as' => 'addClientAddress', 'uses' => 'ClientAddressController@addClientAddress']);
	// Route::post('client/delete/address', ['as' => 'deleteClientAddress', 'uses' => 'ClientAddressController@deleteClientAddress']);

	// Client company
	// Route::post('client/list/companies', ['as' => 'getClientCompanies', 'uses' => 'ClientCompanyController@getClientCompanies']);
	// Route::post('client/add/company', ['as' => 'addClientCompany', 'uses' => 'ClientCompanyController@addClientCompany']);
	// Route::post('client/delete/company', ['as' => 'deleteClientCompany', 'uses' => 'ClientCompanyController@deleteClientCompany']);

	// Notification templates
	Route::post('notification-templates/list-model-variables', ['as' => 'listModelVars', 'uses' => 'NotificationTemplateCrudController@listModelVars']);

    Route::group(['as' => 'ajax.'], function(){
        Route::get('dashboard', 'Ajax\DashboardController@index')->name('dashboard.index');
    });
});

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    CRUD::resource('user', 'UserCrudController');
    CRUD::resource('product', 'ProductCrudController');
    CRUD::resource('tax', 'TaxCrudController');
    CRUD::resource('attribute', 'AttributeCrudController');
    CRUD::resource('attributes-sets', 'AttributeSetCrudController');
    CRUD::resource('carrier', 'CarrierCrudController');
    CRUD::resource('category', 'CategoryCrudController');
    CRUD::resource('currency', 'CurrencyCrudController');
    CRUD::resource('customer', 'CustomerCrudController');
    CRUD::resource('notification-templates', 'NotificationTemplateCrudController');
    CRUD::resource('product-discount', 'SpecificPriceCrudController');
    CRUD::resource('order-status', 'OrderStatusCrudController');
    CRUD::resource('order', 'OrderCrudController');
    CRUD::resource('cart-rules', 'CartRuleCrudController');
    CRUD::resource('productgoeswellwith', 'ProductGoesWellWithCrudController');

    CRUD::resource('shipping-volumetric-weight-rate', 'ShippingVolumetricWeightRateCrudController');
    CRUD::resource('shipping-dimension-rate', 'ShippingDimensionRateCrudController');
    CRUD::resource('shipping-weight-rate', 'ShippingWeightRateCrudController');
    CRUD::resource('shipping-country-rate', 'ShippingCountryRateCrudController');
    CRUD::resource('country', 'CountryCrudController');

    Route::post('order/update-status', ['as' => 'updateOrderStatus', 'uses' => 'OrderCrudController@updateStatus']);

    Route::group(['prefix' => 'reports', 'as' => 'reports.'], function(){
        Route::get('order-product/export-csv', 'OrderProductCrudController@exportCSV')->name('order_products.export_csv');
        CRUD::resource('order-product', 'OrderProductCrudController');    
    });

}); // this should be the absolute last line of this file