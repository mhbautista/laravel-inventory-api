<?php

/*
|--------------------------------------------------------------------------
| Custom Backpack\PermissionManager Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are
| handled by the Backpack\PermissionManager package.
|
*/

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware()],
], function () {
    CRUD::resource('permission', 'Backpack\PermissionManager\app\Http\Controllers\PermissionCrudController');
    CRUD::resource('role', 'Backpack\PermissionManager\app\Http\Controllers\RoleCrudController');

    // Modify only the UsersController
    CRUD::resource('user', 'App\Http\Controllers\Admin\UserCrudController');
});