<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::middleware('throttle:480,1')->group(function () {
	Route::get('/orders', 'Api\OrderController@index');
	Route::get('orders/{order_id}', 'Api\OrderController@show');
	Route::get('orders/status/{status}', 'Api\OrderController@showStatus');
	Route::patch('orders/{order_id}', 'Api\OrderController@update');
	Route::resource('products', 'Api\ProductsController');
	Route::get('categories', 'Api\CategoriesController@index');
	Route::get('categories/{category_id}', 'Api\CategoriesController@show');
	Route::patch('categories/{category_id}', 'Api\CategoriesController@update');
});
