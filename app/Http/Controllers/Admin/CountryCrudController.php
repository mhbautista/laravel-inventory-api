<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CountryRequest as StoreRequest;
use App\Http\Requests\CountryRequest as UpdateRequest;

use PragmaRX\Countries\Package\Countries;

/**
 * Class CountryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CountryCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Country');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/country');
        $this->crud->setEntityNameStrings('country', 'countries');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addFields([
            [ // select_from_array
                'name' => 'code',
                'label' => 'Code',
                'type' => 'select2_from_array',
                'options' => Countries::all()->pluck('cca2', 'cca2')->sortBy('name'),
                'allows_null' => false,
                'default' => null,
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
                'hint' => 'The 2-letter code of the country. Examples: PH, US, CN',
            ],
            [ // select_from_array
                'name' => 'name',
                'label' => 'Name',
                'type' => 'select2_from_array',
                'options' => Countries::all()->pluck('name.common', 'name.common')->sortBy('name'),
                'allows_null' => false,
                'default' => null,
                // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
                'hint' => 'The country common name. Examples: Philippines, United States, China',
            ],
            [  // Select
               'label' => "Currency",
               'type' => 'select2',
               'name' => 'currency_id', // the db column for the foreign key
               'entity' => 'currency', // the method that defines the relationship in your Model
               'attribute' => 'name', // foreign key attribute that is shown to user
               'model' => "App\Models\Currency",

               // optional
               'options'   => (function ($query) {
                    return $query->orderBy('name', 'ASC')->get();
                }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            ]
        ]);

        $this->crud->addColumns([
            [
               'name' => 'code',
               'label' => "Code",
               'type' => "text",
            ],
            [
               'name' => 'name',
               'label' => "Name",
               'type' => "text",
            ],
            [
               'name' => 'currency_id',
               'label' => "Currency Id",
               'type' => "text",
            ],
        ]);

        // add asterisk for fields that are required in CountryRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
