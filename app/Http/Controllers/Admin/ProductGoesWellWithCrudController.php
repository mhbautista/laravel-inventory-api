<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ProductGoesWellWithRequest as StoreRequest;
use App\Http\Requests\ProductGoesWellWithRequest as UpdateRequest;

/**
 * Class ProductGoesWellWithCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ProductGoesWellWithCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ProductGoesWellWith');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/productgoeswellwith');
        $this->crud->setEntityNameStrings('productgoeswellwith', 'Product Goes Well With');

        $this->setPermissions();
        $this->crud->addColumns([
            [
                'name'  => 'product_id',
                'label' => 'Product',
                'type' => "model_function",
                'function_name' => 'getProductName',
            ],
            [
               // n-n relationship (with pivot table)
               'label' => "Related Products", // Table column heading
               'type' => "select_multiple",
               'name' => 'products', // the method that defines the relationship in your Model
               'entity' => 'products', // the method that defines the relationship in your Model
               'attribute' => "name", // foreign key attribute that is shown to user
               'model' => "App\Models\Product", // foreign key model
            ],
        ]);

        $this->setFields();

        $this->crud->enableAjaxTable();

        // add asterisk for fields that are required in AttributeSetRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }
    public function setPermissions()
    {
        // Get authenticated user
        $user = backpack_auth()->user();

        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'delete']);

        // Allow list access
        if ($user->can('list_attribute_sets')) {
            $this->crud->allowAccess('list');
        }

        // Allow create access
        if ($user->can('create_attribute_set')) {
            $this->crud->allowAccess('create');
        }

        // Allow update access
        if ($user->can('update_attribute_set')) {
            $this->crud->allowAccess('update');
        }

        // Allow delete access
        if ($user->can('delete_attribute_set')) {
            $this->crud->allowAccess('delete');
        }
    }

      public function setFields()
    {
        $this->crud->addFields([
            [
                'name'      => 'product_id',
                'label'     => 'Product',
                'type'      => 'select2',
                'entity'    => 'product',
                'attribute' => 'name',
                'model'     => "App\Models\Product",
            ],
            [
                'type'      => 'select2_multiple',
                'label'     => trans('attribute.attributes'),
                'name'      => 'products',
                'entity'    => 'products',
                'attribute' => 'name',
                'model'     => "App\Models\Product",
                'pivot'     => true,
            ]
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
