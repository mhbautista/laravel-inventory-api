<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CustomerRequest as StoreRequest;
use App\Http\Requests\CustomerRequest as UpdateRequest;

/**
 * Class CustomerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CustomerCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\User');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/customer');
        $this->crud->setEntityNameStrings('customer', 'customers');
        $this->crud->addClause('whereHas', 'roles', function ($query) {
            $customerRoleName = env('CUSTOMER_ROLE_NAME');
            $query->whereName($customerRoleName ?: 'customer');
        });


        $this->crud->addColumns([
            [
                'name'        => 'salutation',
                'label'       => trans('customer.salutation'),
            ],
            [
                'name'  => 'first_name',
                'label' => trans('customer.name'),
            ],
            [
                'name'      => 'gender',
                'label'     => trans('customer.gender'),
                'type'      => 'boolean',
                'options'   => [
                    1 => trans('customer.male'),
                    2 => trans('customer.female'),
                ],
            ],
            [
                'name'  => 'email',
                'label' => trans('customer.email'),
            ],
            [
                'name'      => 'active',
                'label'     => trans('common.status'),
                'type'      => 'boolean',
                'options'   => [
                    0 => trans('common.inactive'),
                    1 => trans('common.active'),
                ],
            ]

        ]);

        $this->setFields();
        $this->crud->enableAjaxTable();
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }
    public function setFields()
    {
        $this->crud->addFields([
            [
                'name'  => 'salutation',
                'label' => trans('customer.salutation'),
                'type'  => 'text',

                'tab'   => trans('customer.tab_general'),
            ],
            [
                'name'  => 'first_name',
                'label' => trans('customer.first_name'),
                'type'  => 'text',

                'tab'   => trans('customer.tab_general'),
            ],
            [
                'name'  => 'middle_name',
                'label' => trans('customer.middle_name'),
                'type'  => 'text',

                'tab'   => trans('customer.tab_general'),
            ],
            [
                'name'  => 'last_name',
                'label' => trans('customer.last_name'),
                'type'  => 'text',

                'tab'   => trans('customer.tab_general'),
            ],
            [
                'name'  => 'mobile_phone',
                'label' => trans('customer.last_name'),
                'type'  => 'text',

                'tab'   => trans('customer.tab_general'),
            ],
            [
                'name'  => 'email',
                'label' => trans('customer.email'),
                'type'  => 'email',

                'tab'   => trans('customer.tab_general'),
            ],
            [
                'name'  => 'password',
                'label' => trans('customer.password'),
                'type'  => 'password',

                'tab'   => trans('customer.tab_general'),
            ],
            [
                'name'  => 'password_confirmation',
                'label' => trans('customer.password_confirmation'),
                'type'  => 'password',

                'tab'   => trans('customer.tab_general'),
            ],
            [
                'name'      => 'gender',
                'label'     => trans('customer.gender'),
                'type'      => 'select_from_array',
                'options'   => [
                    1 => trans('customer.male'),
                    2 => trans('customer.female'),
                ],

                'tab'   => trans('customer.tab_general'),
            ],
            [
                'name'  => 'birthday',
                'label' => trans('customer.birthday'),
                'type'  => 'date',

                'tab'   => trans('customer.tab_general'),
            ],
            [
                'name'      => 'active',
                'label'     => trans('common.status'),
                'type'      => 'select_from_array',
                'options'   => [
                    0 => trans('common.inactive'),
                    1 => trans('common.active'),
                ],

                'tab'   => trans('customer.tab_general'),
            ],
            [
            // two interconnected entities
            'label'             => trans('permissionmanager.user_role_permission'),
            'field_unique_name' => 'user_role_permission',
            'type'              => 'checklist_dependency',
            'name'              => 'roles_and_permissions',
            'subfields'         => [
                    'primary' => [
                        'label'            => trans('permissionmanager.roles'),
                        'name'             => 'roles',
                        'entity'           => 'roles',
                        'entity_secondary' => 'permissions',
                        'attribute'        => 'name',
                        'model'            => config('laravel-permission.models.role'),
                        'pivot'            => true,
                        'number_columns'   => 3, //can be 1,2,3,4,6
                    ],
                    'secondary' => [
                        'label'          => ucfirst(trans('permissionmanager.permission_singular')),
                        'name'           => 'permissions',
                        'entity'         => 'permissions',
                        'entity_primary' => 'roles',
                        'attribute'      => 'name',
                        'model'          => "Backpack\PermissionManager\app\Models\Permission",
                        'pivot'          => true,
                        'number_columns' => 3, //can be 1,2,3,4,6
                    ],
                ],

                'tab'   => trans('customer.tab_permissions'),
            ],
        ]);
    }
    
    public function store(StoreRequest $request)
    {
        $customerRoleName = env('CUSTOMER_ROLE_NAME');

        $this->handlePasswordInput($request);

        $redirect_location = parent::storeCrud($request);
        // $customerRoleID = \DB::table('roles')->whereName($customerRoleName ?: 'customer')->first()->id;
        // $this->crud->entry->roles()->attach($customerRoleID);

        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $this->handlePasswordInput($request);

        $redirect_location = parent::updateCrud($request);

        return $redirect_location;
    }

    protected function handlePasswordInput(CrudRequest $request)
    {
        // Remove fields not present on the user.
        $request->request->remove('password_confirmation');

        // Encrypt password if specified.
        if ($request->input('password')) {
            $request->request->set('password', bcrypt($request->input('password')));
        } else {
            $request->request->remove('password');
        }
    }
}
