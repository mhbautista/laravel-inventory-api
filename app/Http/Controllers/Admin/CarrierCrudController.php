<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CarrierRequest as StoreRequest;
use App\Http\Requests\CarrierRequest as UpdateRequest;

/**
 * Class CarrierCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CarrierCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Carrier');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/carrier');
        $this->crud->setEntityNameStrings('carrier', 'carriers');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
         $this->crud->addColumns([
            [
                'name'  => 'name',
                'label' => trans('category.name'),
            ],
            [
                'name' => 'price',
                'label' => 'Price'
            ]
        ]);
        $this->setPermissions();
        $this->setFields();

        // add asterisk for fields that are required in CarrierRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }
      public function setPermissions()
    {
        // Get authenticated user
        $user =  backpack_auth()->user();

        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'delete']);

        // Allow list access
        if ($user->can('list_carriers')) {
            $this->crud->allowAccess('list');
        }

        // Allow create access
        if ($user->can('create_carrier')) {
            $this->crud->allowAccess('create');
        }

        // Allow update access
        if ($user->can('update_carrier')) {
            $this->crud->allowAccess('update');
        }

        // Allow delete access
        if ($user->can('delete_carrier')) {
            $this->crud->allowAccess('delete');
        }
    }

    public function setFields()
    {
        $this->crud->addFields([
            [
                'name'  => 'name',
                'label' => trans('carrier.name'),
                'type'  => 'text',
            ],
            [
                'name'       => 'price',
                'label'      => trans('carrier.price'),
                'type'       => 'number',
                'attributes' => [
                    'step' => 'any'
                ]
            ],
            [
                'name'  => 'delivery_text',
                'label' => trans('carrier.delivery_text'),
                'type'  => 'text',
            ],
            [
                'name'    => "logo",
                'label'   => trans('carrier.logo'),
                'type'    => 'image',
                'upload'  => true,
                'crop'    => false,
                'default' => 'default.png',
                'prefix'  => 'uploads/carriers/'
            ]
        ]);
    }
    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
