<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ShippingCountryRateRequest as StoreRequest;
use App\Http\Requests\ShippingCountryRateRequest as UpdateRequest;

/**
 * Class ShippingCountryRateCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ShippingCountryRateCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ShippingCountryRate');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/shipping-country-rate');
        $this->crud->setEntityNameStrings('Shipping Country Rate', 'Shipping Country Rates');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();
        
        $this->crud->addFields([
            [
                'name' => 'country',
                'label' => 'Country',
                'type' => 'text',
                'hint' => 'The country common name. Eg. Philippines, Brazil, United States',
            ],
            [
                'name' => 'state',
                'label' => 'State',
                'type' => 'text',
                'hint' => 'The state or province name. Eg. Abra, California',
            ],
            [
                'name' => 'price',
                'label' => 'Price',
                'type' => 'number',
                'attributes' => ["step" => "any"], // allow decimals
            ],
        ]);

        $this->crud->addColumns([
            [
               'name' => 'country',
               'label' => "Country",
               'type' => "text",
            ],
            [
               'name' => 'state',
               'label' => "State",
               'type' => "text",
            ],
            [
               'name' => 'price',
               'label' => "Price",
               'type' => "number",
               'decimals' => 2,
            ],
        ]);

        // add asterisk for fields that are required in ShippingCountryRateRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
