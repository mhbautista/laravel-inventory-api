<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ShippingVolumetricWeightRateRequest as StoreRequest;
use App\Http\Requests\ShippingVolumetricWeightRateRequest as UpdateRequest;

/**
 * Class ShippingVolumetricWeightRateCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ShippingVolumetricWeightRateCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ShippingVolumetricWeightRate');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/shipping-volumetric-weight-rate');
        $this->crud->setEntityNameStrings('Shipping Volumetric Weight Rate', 'Shipping Volumetric Weight Rates');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->addFields([
            [
                'name' => 'min_volumetric_weight',
                'label' => 'Min Volumetric Weight',
                'type' => 'number',
                'attributes' => ["step" => "any"], // allow decimals
            ],
            [
                'name' => 'max_volumetric_weight',
                'label' => 'Max Volumetric Weight',
                'type' => 'number',
                'attributes' => ["step" => "any"], // allow decimals
            ],
            [
                'name' => 'unit',
                'label' => 'Unit',
                'type' => 'enum',
            ],
            [
                'name' => 'price',
                'label' => 'Price',
                'type' => 'number',
                'attributes' => ["step" => "any"], // allow decimals
            ],
        ]);

        $this->crud->addColumns([
            [
               'name' => 'min_volumetric_weight',
               'label' => 'Min Volumetric Weight',
               'type' => "number",
               'decimals' => 2,
            ],
            [
               'name' => 'max_volumetric_weight',
               'label' => 'Max Volumetric Weight',
               'type' => "number",
               'decimals' => 2,
            ],
            [
               'name' => 'unit',
               'label' => "Unit",
               'type' => "text",
            ],
            [
               'name' => 'price',
               'label' => "Price",
               'type' => "number",
               'decimals' => 2,
            ],
        ]);

        // add asterisk for fields that are required in ShippingVolumetricWeightRateRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
