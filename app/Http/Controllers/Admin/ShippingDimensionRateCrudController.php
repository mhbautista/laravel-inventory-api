<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ShippingDimensionRateRequest as StoreRequest;
use App\Http\Requests\ShippingDimensionRateRequest as UpdateRequest;

/**
 * Class ShippingDimensionRateCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ShippingDimensionRateCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ShippingDimensionRate');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/shipping-dimension-rate');
        $this->crud->setEntityNameStrings('Shipping Dimension Rate', 'Shipping Dimension Rates');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();
        
        $this->crud->addFields([
            [   // Number
                'name' => 'min_length',
                'label' => 'Min Length',
                'type' => 'number',
                // optionals
                'attributes' => ["step" => "any"], // allow decimals
                // 'prefix' => "$",
                // 'suffix' => ".00",
            ],
            [
                'name' => 'max_length',
                'label' => 'Max Length',
                'type' => 'number',
                'attributes' => ["step" => "any"], // allow decimals
            ],
            [
                'name' => 'min_width',
                'label' => 'Min Width',
                'type' => 'number',
                'attributes' => ["step" => "any"], // allow decimals
            ],
            [
                'name' => 'max_width',
                'label' => 'Max Width',
                'type' => 'number',
                'attributes' => ["step" => "any"], // allow decimals
            ],
            [
                'name' => 'min_height',
                'label' => 'Min Height',
                'type' => 'number',
                'attributes' => ["step" => "any"], // allow decimals
            ],
            [
                'name' => 'max_height',
                'label' => 'Max Height',
                'type' => 'number',
                'attributes' => ["step" => "any"], // allow decimals
            ],
            [   // Enum
                'name' => 'unit',
                'label' => 'Unit',
                'type' => 'enum'
            ],
            [   // Enum
                'name' => 'price',
                'label' => 'Price',
                'type' => 'number',
                'attributes' => ["step" => "any"], // allow decimals
            ],
        ]);

        $this->crud->addColumns([
            [
               'name' => 'min_length', // The db column name
               'label' => "Min Length", // Table column heading
               'type' => "number",
               // 'prefix' => "$",
               // 'suffix' => " EUR",
               // 'decimals' => 2,
            ],
            [
               'name' => 'max_length',
               'label' => "Max Length",
               'type' => "number",
               'decimals' => 2,
            ],
            [
               'name' => 'min_width',
               'label' => "Min Width",
               'type' => "number",
               'decimals' => 2,
            ],
            [
               'name' => 'max_width',
               'label' => "Max Width",
               'type' => "number",
               'decimals' => 2,
            ],
            [
               'name' => 'min_height',
               'label' => "Min Height",
               'type' => "number",
               'decimals' => 2,
            ],
            [
               'name' => 'max_height',
               'label' => "Max Height",
               'type' => "number",
               'decimals' => 2,
            ],
            [
               'name' => 'unit',
               'label' => "Unit",
               'type' => "text",
            ],
            [
               'name' => 'price',
               'label' => "Price",
               'type' => "number",
               'decimals' => 2,
            ],
        ]);

        // add asterisk for fields that are required in ShippingDimensionRateRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
