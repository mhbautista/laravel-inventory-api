<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CategoryRequest as StoreRequest;
use App\Http\Requests\CategoryRequest as UpdateRequest;

/**
 * Class CategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CategoryCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Category');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/category');
        $this->crud->setEntityNameStrings('category', 'categories');
         $this->crud->enableReorder('name', 0);
        $this->crud->addColumns([
            [
                'type'      => "select",
                'label'     => trans('category.parent'),
                'name'      => 'parent_id',
                'entity'    => 'parent',
                'attribute' => "name",
                'model'     => "App\Models\Category",
            ],
            [
                'name'  => 'name',
                'label' => trans('category.name'),
            ],
            [
                'name'  => 'slug',
                'label' => trans('category.slug'),
            ]
        ]);

        $this->setPermissions();
        $this->setFields();

        // add asterisk for fields that are required in CategoryRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }
    public function setPermissions()
    {
        // Get authenticated user
         $user = backpack_auth()->user();

        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // Allow list access
        if ($user->can('list_categories')) {
            $this->crud->allowAccess('list');
        }

        // Allow create access
        if ($user->can('create_category')) {
            $this->crud->allowAccess('create');
        }

        // Allow update access
        if ($user->can('update_category')) {
            $this->crud->allowAccess('update');
        }

        // Allow reorder access
        if ($user->can('reorder_categories')) {
            $this->crud->allowAccess('reorder');
        }

        // Allow delete access
        if ($user->can('delete_category')) {
            $this->crud->allowAccess('delete');
        }
    }

    public function setFields()
    {
        $this->crud->addFields([
            [
                'name'  => 'name',
                'label' => trans('category.name'),
                'type'  => 'text',
            ],
            [
                'name'  => 'slug',
                'label' => trans('category.slug'),
                'type'  => 'text',
            ]
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
