<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AttributeSetRequest as StoreRequest;
use App\Http\Requests\AttributeSetRequest as UpdateRequest;
use Illuminate\Http\Request;
use App\Models\Attribute;

/**
 * Class AttributeSetCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class AttributeSetCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\AttributeSet');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/attributes-sets');
        $this->crud->setEntityNameStrings('attribute set', 'Attribute Sets');
        $this->setPermissions();
        $this->crud->addColumns([
            [
                'name'  => 'name',
                'label' => trans('attribute.name'),
            ]
        ]);

        $this->setFields();

        $this->crud->enableAjaxTable();

        // add asterisk for fields that are required in AttributeSetRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }
    public function setPermissions()
    {
        // Get authenticated user
        $user = backpack_auth()->user();

        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'delete']);

        // Allow list access
        if ($user->can('list_attribute_sets')) {
            $this->crud->allowAccess('list');
        }

        // Allow create access
        if ($user->can('create_attribute_set')) {
            $this->crud->allowAccess('create');
        }

        // Allow update access
        if ($user->can('update_attribute_set')) {
            $this->crud->allowAccess('update');
        }

        // Allow delete access
        if ($user->can('delete_attribute_set')) {
            $this->crud->allowAccess('delete');
        }
    }

    public function setFields()
    {
        $this->crud->addFields([
            [
                'name'      => 'name',
                'label'     => trans('attribute.name'),
                'type'      => 'text',
            ],
            [
                'type'      => 'select2_multiple',
                'label'     => trans('attribute.attributes'),
                'name'      => 'attributes',
                'entity'    => 'attributes',
                'attribute' => 'name',
                'model'     => "App\Models\Attribute",
                'pivot'     => true,
            ]
        ]);
    }

    public function ajaxGetAttributesBySetId(Request $request, Attribute $attribute)
    {
        // Init old as an empty array
        $old = [];

        // Set old inputs as array from $request
        if (isset($request->old)) {
            $old = json_decode($request->old, true);
        }

        // Get attributes with values by set id
        $attributes = $attribute->with('values')->whereHas('sets', function ($q) use ($request) {
            $q->where('id', $request->setId);
        })->get();

        return view('renders.product_attributes', compact('attributes', 'old'));
    }

    public function store(StoreRequest $request)
    {
        $redirect_location = parent::storeCrud();

        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $redirect_location = parent::updateCrud();

        return $redirect_location;
    }
}
