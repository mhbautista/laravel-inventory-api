<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\OrderProductRequest as StoreRequest;
use App\Http\Requests\OrderProductRequest as UpdateRequest;

use App\Exports\OrderProductExport;
use App\Exports\OrderProductExportFromView;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

/**
 * Class OrderProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class OrderProductCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\OrderProduct');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/reports/order-product');
        $this->crud->setEntityNameStrings('Product Order', 'Product Orders');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->setColumns([
            [
               'label' => "Order #",
               'type' => "select",
               'name' => 'order_id',
               'entity' => 'order',
               'attribute' => "id",
               'model' => "App\Models\Order",
            ],
            [
               'label' => "SKU",
               'type' => "select",
               'name' => 'product_id',
               'key' => 'product_sku',
               'entity' => 'product',
               'attribute' => "sku",
               'model' => "App\Models\Product",
            ],
            [
               'label' => "Product",
               'type' => "select",
               'name' => 'product_id',
               'key' => 'product_name',
               'entity' => 'product',
               'attribute' => "name",
               'model' => "App\Models\Product",
            ],
            [
               'label' => "Price",
               'type' => "select",
               'name' => 'product_id',
               'entity' => 'product',
               'key' => 'product_price',
               'attribute' => "price",
               'model' => "App\Models\Product",
            ],
            [
               'name' => 'quantity',
               'label' => "Qty",
               'type' => 'text',
            ],
            [
               'name' => 'status',
               'label' => "Status",
               'type' => 'text',
            ],
            [
               'label' => "Date Ordered",
               'type' => "select",
               'name' => 'order_id',
               'key' => 'order_created_at',
               'entity' => 'order',
               'attribute' => "created_at",
               'model' => "App\Models\Order",
            ],
            [
               'name' => 'order.date_cancelled',
               'label' => "Date Cancelled",
               'type' => 'text',
            ],
            [
               'label' => "Payment",
               'type' => "select",
               'name' => 'order_id',
               'key' => 'order_payment_method',
               'entity' => 'order',
               'attribute' => "payment_method",
               'model' => "App\Models\Order",
            ],
            [
              // Magpie Charge Id
               'label' => "Magpie CI",
               'type' => "select",
               'name' => 'order_id',
               'key' => 'order_magpie_charge_id',
               'entity' => 'order',
               'attribute' => "magpie_charge_id",
               'model' => "App\Models\Order",
            ],
            [
               'name' => 'order.voucher',
               'label' => "Voucher",
               'type' => 'text',
            ],
            [
               'name' => 'order.voucher_amount',
               'label' => "Discount",
               'type' => 'text',
            ],
            [
               'label' => "Shipping Fee",
               'type' => "select",
               'name' => 'order_id',
               'key' => 'order_total_shipping',
               'entity' => 'order',
               'attribute' => "total_shipping",
               'model' => "App\Models\Order",
            ],
            [
               'name' => 'order.invoice_no',
               'label' => "Ref #",
               'type' => 'text',
            ],
        ]);

        // Do some ordering...
        // if (!$this->request->has('order')) {
        //     $this->crud->orderBy();
        // }
        
        // Add some buttons
        // add a button whose HTML is returned by a method in the CRUD model
        $this->crud->addButtonFromView('top', 'order_product_export_xlsx', 'order_product_export_xlsx', 'beginning');

        // Disable some buttons
        $this->crud->removeButton('create');
        $this->crud->removeAllButtonsFromStack('line');

        $this->crud->with(['order', 'product']);
        // $this->crud->with('product');
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreRequest $request)
    {
        abort(404);
    }

    public function update(UpdateRequest $request)
    {
        abort(404);
    }

    public function exportCSV()
    {
        // Export from class only
        // return Excel::download(new OrderProductExportF, 'Product Orders '.Carbon::now()->toDateTimeString().'.csv');
        
        // Export from view
        return Excel::download(new OrderProductExport, 'Product Orders '.Carbon::now()->toDateTimeString().'.xlsx');
    }
}
