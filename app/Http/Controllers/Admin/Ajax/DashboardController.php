<?php

namespace App\Http\Controllers\Admin\Ajax;

use App\Models\Product;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        $orders_today     = number_format(Order::whereDate('created_at', Carbon::today())->count());
        $orders_this_week = number_format(Order::whereDate('created_at', '>=', Carbon::now()->startOfWeek())->count());
        $sales_today      = number_format(Order::whereDate('created_at', Carbon::today())->sum('total'), 2);
        $sales_this_week  = number_format(Order::whereDate('created_at', '>=', Carbon::now()->startOfWeek())->sum('total'), 2);

        $orders_status = [
            'pending'    => Order::where('status_id', 1)->count(),
            'paid'       => Order::where('status_id', 2)->count(),
            'processing' => Order::where('status_id', 3)->count(),
            'delivered'  => Order::where('status_id', 4)->count(),
            'done'       => Order::where('status_id', 5)->count(),
            'cancelled'  => Order::where('status_id', 6)->count(),
            'refunded'   => Order::where('status_id', 7)->count(),
        ];

        $top_products = Product::whereHas('orders')
            ->with('orders')
            ->withCount('orders')
            ->withCount(['orders as orders_sum' => function($query) {
                $query->select(DB::raw('sum(price)'));
            }])
            ->orderBy('orders_count', 'desc')
            ->get()
            ->map(function($product){
                return $product->only('name', 'orders_count', 'orders_sum');
            });

        return response()->json(compact(
            'orders_today',
            'orders_this_week',
            'sales_today',
            'sales_this_week',
            'orders_status',
            'top_products'
        ));
    }
}
