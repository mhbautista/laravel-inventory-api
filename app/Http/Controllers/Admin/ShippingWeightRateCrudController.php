<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ShippingWeightRateRequest as StoreRequest;
use App\Http\Requests\ShippingWeightRateRequest as UpdateRequest;

/**
 * Class ShippingWeightRateCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ShippingWeightRateCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ShippingWeightRate');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/shipping-weight-rate');
        $this->crud->setEntityNameStrings('Shipping Weight Rate', 'Shipping Weight Rates');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();
        
        $this->crud->addFields([
            [
                'name' => 'min_weight',
                'label' => 'Min Weight',
                'type' => 'number',
                'attributes' => ["step" => "any"], // allow decimals
            ],
            [
                'name' => 'max_weight',
                'label' => 'Max Weight',
                'type' => 'number',
                'attributes' => ["step" => "any"], // allow decimals
            ],
            [
                'name' => 'unit',
                'label' => 'Unit',
                'type' => 'enum',
            ],
            [
                'name' => 'price',
                'label' => 'Price',
                'type' => 'number',
                'attributes' => ["step" => "any"], // allow decimals
            ],
        ]);

        $this->crud->addColumns([
            [
               'name' => 'min_weight',
               'label' => "Min Weight",
               'type' => "number",
               'decimals' => 2,
            ],
            [
               'name' => 'max_weight',
               'label' => "Max Weight",
               'type' => "number",
               'decimals' => 2,
            ],
            [
               'name' => 'unit',
               'label' => "Unit",
               'type' => "text",
            ],
            [
               'name' => 'price',
               'label' => "Price",
               'type' => "number",
               'decimals' => 2,
            ],
        ]);

        // add asterisk for fields that are required in ShippingWeightRateRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->orderBy('max_weight');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
