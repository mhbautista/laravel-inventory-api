<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ProductRequest as StoreRequest;
use App\Http\Requests\ProductRequest as UpdateRequest;
use App\Models\Attribute;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\ProductImage;
use App\Models\ProductBadge;
use App\Models\ProductMeasurement;
use App\Models\Tax;
use App\Models\SpecificPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Product');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/product');
        $this->crud->setEntityNameStrings('product', 'products');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->setPermissions();
         $this->crud->addColumns([
            [
            'name'  => 'name',
            'label' => trans('product.name'),
            ],
            [
            'type'      => "select_multiple",
            'label'     => trans('category.categories'),
            'name'      => 'categories',
            'entity'    => 'categories',
            'attribute' => "name",
            'model'     => "App\Models\Category",
            ],
            [
            'name'  => 'sku',
            'label' => trans('product.sku'),
            ],
            [
            'name'  => 'price',
            'label' => trans('product.price'),
            ],
            [
            'name'  => 'stock',
            'label' => trans('product.stock'),
            ],
            [
            'name'      => 'active',
            'label'     => trans('common.status'),
            'type'      => 'boolean',
            'options'   => [
            0 => trans('common.inactive'),
            1 => trans('common.active')
            ],
            ]
            ]);
        $this->setFields();
        $this->crud->enableAjaxTable();

        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }
    public function setFields()
    {
        $this->crud->addFields([
            [
                'name'  => 'sku',
                'label' => trans('product.sku'),
                'type'  => 'text',

                    // TAB
                'tab'   => trans('product.general_tab'),
            ],
            [
                'name'  => 'name',
                'label' => trans('product.name'),
                'type'  => 'text',

                    // TAB
                'tab'   => trans('product.general_tab'),
            ],
            [
                'name'  => 'slug',
                'label' => trans('product.slug'),
                'type'  => 'text',

                    // TAB
                'tab'   => trans('product.general_tab'),
            ],
            [
                'name'  => 'description',
                'label' => trans('product.description'),
                'type'  => 'ckeditor',
                // 'type'  => 'textarea',

                    // TAB
                'tab'   => trans('product.general_tab'),
            ],
            [
                'name'      => 'categories',
                'label'     => trans('category.categories'),
                'hint'      => trans('product.hint_category'),
                'type'      => 'select2_multiple',
                'entity'    => 'categories',
                'attribute' => 'name',
                'model'     => "App\Models\Category",
                'pivot'     => true,

                    // TAB
                'tab'   => trans('product.general_tab'),
            ],
            // [
            //     'name'      => 'fake_field',
            //     'label'     => 'Goes well with',
            //     'type'      => 'select2_multiple',
            //     'entity'    => 'product',
            //     'attribute' => 'name',
            //     'model'     => "App\Models\Product",
            //     'fake'      =>true,

            //         // TAB
            //     'tab'   => trans('product.general_tab'),
            // ],
            
            [
                'name'  => 'stock',
                'label' => trans('product.stock'),
                'type'  => 'number',

                    // TAB
                'tab'   => trans('product.general_tab'),
            ],
            
            [
                'name'  => 'price',
                'label' => trans('product.price'),
                'prefix' => '&#8369;',
                'type'  => 'number',
                'attributes' => [
                    'step' => 'any',
                ],

                    // TAB
                'tab'   => trans('product.general_tab'),
            ],

            [
                'type'           => 'select2_tax',
                'label'          => trans('tax.tax'),
                'name'           => 'tax_id',
                'entity'         => 'tax',
                'attribute'      => 'name',
                'data_value'     => 'value',
                'model'          => "App\Models\Tax",
                'attributes'     => [
                'id'    => 'tax',
            ],

                    // TAB
                'tab'   => trans('product.general_tab'),
            ],
            [
                'name'          => 'price_without_vat',
                'label'         => trans('product.price_without_tax'),
                'type'          => 'text',
                'prefix' => '&#8369;',
                'attributes'    => [
                    'readonly'  => 'readonly',
                ],

                    // TAB
                'tab'   => trans('product.general_tab'),
            ],
            [
                'name'  => 'price_vat_calculator',
                'type'  => 'product_vat',
                'tab'   => trans('product.general_tab'),

            ],
            [
                'name'    => 'active',
                'label'   => trans('common.status'),
                'type'    => 'select_from_array',
                'options' => [
                                '0' => trans('common.inactive'),
                                '1' => trans('common.active'),
                            ],
                'default' => 1,

                    // TAB
                'tab'   => trans('product.general_tab'),
            ],
            [
                'name'       => 'attribute_set_id',
                'label'      => trans('attribute.attribute_sets'),
                'type'       => 'select2',
                'entity'     => 'attribute_set',
                'attribute'  => 'name',
                'model'      => "App\Models\AttributeSet",
                'attributes' => [
                'id'    => 'attributes-set'
            ],

                   // TAB
                'tab'   => trans('product.attributes_tab'),
            ],
            [
                'name'  => 'attribute_types',
                'label' => trans('attribute.name'),
                'type'  => 'product_attributes',
                'entity'     => 'attributes',
                'attribute'  => 'name',
                'model'      => "App\Models\AttributeSet",

                    // TAB
                'tab'   => trans('product.attributes_tab'),
            ]
            ]);

        $this->crud->addField([
                'name'          => 'dropzone',
                'type'          => 'dropzone',
                'disk'          => 'products', // disk where images will be uploaded
                'mimes'         => [
                'image/*'
                ],
                'filesize'      => 5, // maximum file size in MB

                    // TAB
                'tab'           => 'Images',
            ], 'update');

        // $this->crud->addField([
        //         'name'          => 'product_group',
        //         'type'          => 'product_group',
        //         'model'         => 'App\Models\Product',
        //             // TAB
        //         'tab'           => trans('product.group_tab'),
        //     ], 'update');

        // Badge functionality
        $this->crud->addFields([
            [
                'name'  => 'product_badge[title]',
                'label' => 'Title',
                'model' =>'App\Models\ProductBadge',
                'entity' => 'product_badge',
                'attribute' => 'title',
                'type'  => 'product_pivot_text',
                'allows_null' => true,
                    // TAB
                'tab'   => 'Badge'
            ],
                         
            [
                'name'  => 'product_badge[description]',
                'label' => 'Description',
                'type'  => 'product_pivot_textarea',
                'entity' => 'product_badge',
                'model' => 'App\Models\ProductBadge',
                'attribute' => 'description',
                    // TAB
                'tab'   => 'Badge'
            ],
            [
                'name'  => 'product_badge[status]',
                'label' => 'Active',
                'model' => 'App\Models\ProductBadge',
                'type'  => 'product_pivot_checkbox',
                'entity' => 'product_badge',
                'attribute' => 'status',
                'default' => 0,
                    // TAB
                'tab'   => 'Badge'
            ],
        ]);
        $this->crud->addFields([
            [
                'name'  => 'product_measurement[weight]',
                'label' => 'Weight (kg)',
                'model' =>'App\Models\ProductMeasuremenfattributest',
                'entity' => 'product_measurement',
                'attribute' => 'weight',
                'type'  => 'product_pivot_text',
                'allows_null' => true,
                'attributes' => [
                                    // 'required' => true,
                                ],
                    // TAB
                'tab'   => 'Measurement'
            ],
                         
            [
                'name'  => 'product_measurement[length]',
                'label' => 'Length (cm)',
                'type'  => 'product_pivot_text',
                'entity' => 'product_measurement',
                'model' => 'App\Models\ProductMeasurement',
                'attribute' => 'length',
                'attributes' => [
                                    // 'required' => true,
                                ],
                    // TAB
                'tab'   => 'Measurement'
            ],
            [
                'name'  => 'product_measurement[width]',
                'label' => 'Width (cm)',
                'model' => 'App\Models\ProductMeasurement',
                'type'  => 'product_pivot_text',
                'entity' => 'product_measurement',
                'attribute' => 'width',
                'attributes' => [
                                    // 'required' => true,
                                ],
                    // TAB
                'tab'   => 'Measurement'
            ],
             [
                'name'  => 'product_measurement[height]',
                'label' => 'Height (cm)',
                'model' => 'App\Models\ProductMeasurement',
                'type'  => 'product_pivot_text',
                'entity' => 'product_measurement',
                'attribute' => 'height',
                'attributes' => [
                                    // 'required' => true,
                                ],
                    // TAB
                'tab'   => 'Measurement'
            ],

            [ // Table for Video Embeds
                'name' => 'video_embeds',
                'label' => 'Video Embeds',
                'type' => 'table',
                'entity_singular' => 'video embed', // used on the "Add X" button
                'columns' => [
                    'code' => 'Code',
                ],
                'max' => 5, // maximum rows allowed in the table
                'min' => 0, // minimum rows allowed in the table
                'hint' => 'Add video embed codes here. Learn more at https://support.google.com/youtube/answer/171780?hl=en.',
                'tab' => 'Videos',
            ],
        ]);

    }
    public function setPermissions() {
        // Get authenticated user
        $user = backpack_auth()->user();

        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'delete']);

        // Allow list access
        if ($user->can('list_products')) {
            $this->crud->allowAccess('list');
        }

        // Allow create access
        if ($user->can('create_product')) {
            $this->crud->allowAccess('create');
            $this->crud->allowAccess('show');
        }

        // Allow update access
        if ($user->can('update_product')) {
            $this->crud->allowAccess('update');
            $this->crud->allowAccess('show');
        }

        // // Allow clone access
        // if ($user->can('clone_product')) {
        //     $this->crud->addButtonFromView('line', trans('product.clone'), 'clone_product', 'end');
        // }

        // Allow delete access
        if ($user->can('delete_product')) {
            $this->crud->allowAccess('delete');
        }
    }

    public function ajaxUploadProductImages(Request $request, Product $product)
    {
        $images = [];
        $disk   = "products";

        if ($request->file && $request->id) {
            $product = $product->find($request->id);
            $productImages = $product->images->toArray();

            if ($productImages) {
                $ord = count($productImages);
            } else {
                $ord = 0;
            }

            foreach ($request->file as $file) {
                $encoding        = 'jpg';
                $filename_hash   = md5(uniqid('', true));
                $filename_large  = $filename_hash.'-lg.'.$encoding;
                $filename_medium = $filename_hash.'.'.$encoding;
                $filename_small  = $filename_hash.'-sm.'.$encoding;

                // Store large image version
                $image = \Image::make($file)->resize(1600, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->encode($encoding)->interlace();
                Storage::disk($disk)->put($filename_large, $image->stream());

                // Store medium image version (square)
                $image = \Image::make($file)->fit(800)->resize(800, 800, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->encode($encoding)->interlace();
                Storage::disk($disk)->put($filename_medium, $image->stream());

                // Store small image version (square)
                $image = \Image::make($file)->fit(120)->resize(120, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->encode($encoding)->interlace();
                Storage::disk($disk)->put($filename_small, $image->stream());

                $images[] = [
                    'product_id'    => $product->id,
                    'name'          => $filename_medium,
                    'order'         => $ord++
                ];
            }

            $product->images()->insert($images);

            // Return the product's images, specifically the small versions.
            return response()->json(
                collect($product->load('images')->images->toArray())
                ->map(function ($image) {
                    return collect($image)->merge([
                        'name' => str_replace('.jpg', '-sm.jpg', $image['name'])
                    ]);
                })
            );
        }
    }

    public function ajaxReorderProductImages(Request $request, ProductImage $productImage)
    {
        if ($request->order) {
            foreach ($request->order as $position => $id) {
                $productImage->find($id)->update(['order' => $position]);
            }
        }
    }

    public function ajaxDeleteProductImage(Request $request, ProductImage $productImage)
    {
        $disk = "products";

        if ($request->id) {
            $productImage = $productImage->find($request->id);

            if (Storage::disk($disk)->has($productImage->name)) {
                if (Storage::disk($disk)->delete($productImage->name)) {
                    $productImage->delete();

                    // Delete image versions
                    Storage::disk($disk)->delete(str_replace('.jpg', '-lg.jpg', $productImage->name));
                    Storage::disk($disk)->delete(str_replace('.jpg', '-sm.jpg', $productImage->name));

                    return response()->json(['success' => true, 'message' => trans('product.image_deleted')]);
                }
            }

            return response()->json(['success' => false, 'message' => trans('product.image_not_found')]);
        }
    }

    public function store(StoreRequest $request, ProductGroup $productGroup, SpecificPrice $specificPrice)
    {
        // Create group entry
        $productGroup = $productGroup->create();

        $request->merge([
            'group_id' => $productGroup->id
        ]);

        $redirect_location = parent::storeCrud($request);

        // Save product's attribute values
        if ($request->input('attributes')) {
            foreach ($request->input('attributes') as $key => $attr_value) {
                if (is_array($attr_value)) {
                    foreach ($attr_value as $value) {
                        $this->crud->entry->attributes()->attach([$key => ['value' => $value]]);
                    }
                } else {
                    $this->crud->entry->attributes()->attach([$key => ['value' => $attr_value]]);
                }
            }
        }

        if($request->has('product_badge')){
            $product_badge = new ProductBadge;
            $product_badge->product_id = $this->crud->entry->id;
            // Log::info($request->shipping_address['address1']);
            foreach ($request->product_badge as $key => $value) {
                $product_badge->$key = $value;
            }
            $product_badge->save();
        }
        if($request->has('product_measurement')){
            $product_measurement = new ProductMeasurement;
            $product_measurement->product_id = $this->crud->entry->id;
            // Log::info($request->shipping_address['address1']);
            foreach ($request->product_measurement as $key => $value) {
                $product_measurement->$key = $value;
            }
            $product_measurement->save();
        }
        
        return $redirect_location;
    }


    public function update(UpdateRequest $request, Attribute $attribute, Product $product)
    {
        // Get current product data
        $redirect_location = parent::updateCrud($request);
        $product = $product->findOrFail($this->crud->request->id);
        $discountType = $request->input('discount_type');
        $reduction = $request->input('reduction');
        $startDate = $request->input('start_date');
        $expirationDate = $request->input('expiration_date');
        $productId = $this->crud->entry->id;
       
        // Add product attributes ids and values in attribute_product_value (pivot)
        if ($request->input('attributes')) {

            // Set attributes upload disk
            $disk = 'attributes';

            // Get old product atrribute values
            $oldAttributes = [];

            foreach ($this->crud->entry->attributes as $oldAttribute) {
                $oldAttributes[$oldAttribute->id] = $oldAttribute->pivot->value;
            }

            // Check if attribute set was changed and delete uploaded data from disk on attribute type media
            if ($product->attribute_set_id != $this->crud->request->attribute_set_id) {
                foreach ($oldAttributes as $key => $oldAttribute) {
                    if (\Storage::disk($disk)->has($oldAttribute) && $attribute->find($key)->values->first()->value != $oldAttribute) {
                        \Storage::disk($disk)->delete($oldAttribute);
                    }
                }
            }

            $this->crud->entry->attributes()->detach();

            foreach ($request->input('attributes') as $key => $attr_value) {
                if (is_array($attr_value)) {
                    foreach ($attr_value as $value) {
                        $this->crud->entry->attributes()->attach([$key => ['value' => $value]]);
                    }
                } else {
                    if(starts_with($attr_value, 'data:image')) {
                        // 1. Delete old image
                        if ($product->attribute_set_id == $this->crud->request->attribute_set_id) {
                            if (\Storage::disk($disk)->has($oldAttributes[$key]) && $attribute->find($key)->values->first()->value != $oldAttributes[$key]) {
                                \Storage::disk($disk)->delete($oldAttributes[$key]);
                            }
                        }
                        // 2. Make the image
                        $image = \Image::make($attr_value);
                        // 3. Generate a filename.
                        $filename = md5($attr_value.time()).'.jpg';
                        // 4. Store the image on disk.
                        \Storage::disk($disk)->put($filename, $image->stream());
                        // 5. Update image filename to attributes_value
                        $attr_value = $filename;
                    }

                    $this->crud->entry->attributes()->attach([$key => ['value' => $attr_value]]);
                }
            }
        }
        if($request->has('product_badge')){
            $product_badge = ProductBadge::where('product_id',$this->crud->entry->id)->first() ?? new ProductBadge;
            $product_badge->product_id = $this->crud->entry->id;
            if($product_badge){
                foreach ($request->product_badge as $key => $value) {
                    $product_badge->$key = $value;
                }
                $product_badge->save();
            }
        }
        if($request->has('product_measurement')){
            $product_measurement = ProductMeasurement::where('product_id',$this->crud->entry->id)->first() ?? new ProductMeasurement; 
            $product_measurement->product_id = $this->crud->entry->id;
            // Log::info($request->shipping_address['address1']);
           if($product_measurement){
                foreach ($request->product_measurement as $key => $value) {
                    $product_measurement->$key = $value;
                }
                $product_measurement->save();
           }
        }
        return $redirect_location;
    }

    /**
     * @param Product $product
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function cloneProduct(Product $product, Request $request)
    {
        $id = $request->input('product_id');
        $cloneSku = $request->input('clone_sku');

        // Check if cloned product has sku
        if (!$cloneSku) {
            \Alert::error(trans('product.sku_required'))->flash();

            return redirect()->back();
        }

        // Check if product sku exist
        if ($product->where('sku', $cloneSku)->first()) {
            \Alert::error(trans('product.sku_unique'))->flash();

            return redirect()->back();
        }

        // Find product and load relations specified in
        $product = $product->loadCloneRelations()->find($id);

        // Redirect back if product what need to be cloned doesn't exist
        if (!$product) {
            \Alert::error(trans('product.cannot_find_product'))->flash();

            return redirect()->back();
        }

        // Create clone object
        $clone = $product->replicate();
        $clone->sku = $cloneSku;

        // Save cloned product
        $clone->push();

        // Clone product relations
        foreach ($product->getRelations() as $relationName => $values){
            $relationType = getRelationType($product->{$relationName}());

            switch($relationType) {
                case 'hasMany':
                if (count($product->{$relationName}) > 0) {
                    foreach ($product->{$relationName} as $relationValue) {
                        $clone->{$relationName}()->create($relationValue->toArray());
                    }
                }
                break;

                case 'hasOne':
                if ($product->{$relationName}) {
                    $clone->{$relationName}()->create($values->toArray());
                }
                break;

                case 'belongsToMany':
                $clone->{$relationName}()->sync($values);
                break;
            }
        }

        \Alert::success(trans('product.clone_success'))->flash();

        return redirect()->back();
    }

    /**
     * Validate if the price after reduction is not less than 0
     *
     * @return boolean
     */
    public function validateReductionPrice($productId, $reduction,
        $discountType)
    {

        $product = Product::find($productId);
        $oldPrice = $product->price;
        if($discountType == 'Amount') {
            $newPrice = $oldPrice - $reduction;
        }
        if($discountType == 'Percent') {
            $newPrice = $oldPrice - $reduction/100.00 * $oldPrice;
        }

        if($newPrice < 0) {
            return false;
        }
        return true;
    }

    /**
     * Check if it doesn't already exist a specific price reduction for the same
     * period for a product
     *
     * @return boolean
     */
    public function validateProductDates($productId, $startDate, $expirationDate)
    {
        $specificPrice = SpecificPrice::where('product_id', $productId)->get();

        foreach ($specificPrice as $item) {
            $existingStartDate = $item->start_date;
            $existingExpirationDate = $item->expiration_date;
             if($expirationDate >= $existingStartDate
                && $startDate <= $existingExpirationDate) {
                return false;
            }
            if($expirationDate >= $existingStartDate
                && $startDate <= $existingExpirationDate) {
                return false;
            }
            if($startDate <= $existingStartDate
                && $expirationDate >= $existingExpirationDate) {
                return false;
            }
        }

        return true;
    }
}
