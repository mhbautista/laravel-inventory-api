<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\OrderRequest as StoreRequest;
use App\Http\Requests\OrderRequest as UpdateRequest;
use App\Models\OrderStatus;
use App\Models\OrderStatusHistory;
use Illuminate\Http\Request;
use Log;
use App\Models\Address;
use App\Models\Product;
use App\Models\Tax;
use App\Models\Order;
use App\Models\OrderCartRule;
use App\Models\Magpie;
/**
 * Class OrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class OrderCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Order');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/order');
        $this->crud->setEntityNameStrings('order', 'orders');

        $this->crud->addColumns([
            [
                'name'  => 'id',
                'label' => '#',
            ],
            [
                'label'     => trans('customer.customers'),
                'type'      => 'select',
                'name'      => 'user_id',
                'entity'    => 'user',
                'attribute' => 'email',
                'model'     => 'App\User',
            ],
            [
                'label'     => trans('order.status'),
                'type'      => 'select',
                'name'      => 'status_id',
                'entity'    => 'status',
                'attribute' => 'name',
                'model'     => 'App\Models\OrderStatus',
            ],
            [
                'name'  => 'total',
                'label' => trans('common.total'),
            ],
            [
                'label'     => trans('currency.currency'),
                'type'      => 'select',
                'name'      => 'currency_id',
                'entity'    => 'currency',
                'attribute' => 'name',
                'model'     => 'App\Models\Currency',
            ],
            [
                'name'  => 'created_at',
                'label' => trans('order.created_at'),
            ]
        ]);
        $this->crud->child_resource_included = ['select' => false, 'number' => false];
        $this->crud->addFields([
            [
                'name'  => 'user_id',
                'label' => trans('customer.name'),
                'type'  => 'select2_order_user',
                'entity' => 'user',
                'attribute' => 'first_name',
                'last_name' => 'last_name',
                'model' => 'App\User',
                'attributes' => ['id'=>'user_id'],
                'allows_null' => true,
                'tab'   => 'General',
            ],
            [
                'name'  => 'carrier_id',
                'label' =>  'Carrier',
                'type'  => 'select2',
                'entity' => 'carrier',
                'attribute' => 'name',
                'model' => 'App\Models\Carrier',
                'tab'   => 'General'
            ],
            [
                'name'  => 'status_id',
                'label' =>  'Order Status',
                'type'  => 'select2',
                'entity' => 'status',
                'attribute' => 'name',
                'model' => 'App\Models\OrderStatus',
                'tab'   => 'General'
            ],
            [
                'name'  => 'currency_id',
                'label' =>  'Currency',
                'type'  => 'select2_currency_order',
                'entity' => 'currency',
                'attribute' => 'name',
                'model' => 'App\Models\Currency',
                'attributes' => ['id'=>'currency_id'],
                'tab'   => 'General'
            ],
            [
                'name'  => 'carrier_id',
                'label' =>  'Carrier',
                'type'  => 'select2',
                'entity' => 'carrier',
                'attribute' => 'name',
                'model' => 'App\Models\Carrier',
                'tab'   => 'General',
                'attributes'    => [
                            'id'  => 'carrier_id',
                        ],
            ],
            [ // Table products
                'name' => 'products',
                'label' => 'Products',
                'type' => 'product_table',
                'custom_model' => "App\Models\Product",
                'entity_singular' => 'Products', // used on the "Add X" button
                'tab'   => 'General',
                'columns' => [
                    [
                        'label' => 'Product',
                        'type' => 'product_select2',
                        'name' => 'product_id',
                        'entity' => 'products',
                        'attribute' => 'sku',
                        'size' => '3',
                        'model' => "App\Models\Product",
                        'attributes'    => [
                            'required'  => true,
                        ],
                    ],
                    [
                        'label' => 'Price',
                        'name' => 'price',
                        'type' => 'product_text',
                        'currency' => true,
                        'attributes' => [
                            'type' => 'text',
                            'readonly' =>true,
                            'ng-pattern'=> "/^[1-9][0-9]{0,2}(?:,?[0-9]{3}){0,3}(?:\.[0-9]{1,2})?$/" ,
                            'step' => "0.01",
                        ]
                    ],
                    [
                        'name' => 'quantity',
                        'label' => 'Quantity',
                        'type' => 'product_number',   
                        'attributes'    => [
                            'required'  => true,
                            'min'       => 1,
                        ],

                    ],
                    
                ],
                'max' => 1000, // maximum rows allowed in the table
                'min' => 1 // minimum rows allowed in the table
            ],

        ]);

        $this->crud->addFields([
            [ // Table card rule
                'name' => 'order_cart_rule',
                'label' => 'Cart rules',
                'type' => 'cart_rule_table',
                'entity_singular' => 'Cart Rule', // used on the "Add X" button
                'tab'   => 'General',
                'columns' => [
                    [
                        'label' => 'Search a name/code',
                        'name' => 'cart_rule_id',
                        'type' => 'cart_rule_select2_from_ajax',
                        'entity' => 'order_cart_rule',
                        'attribute' => 'name',
                        'model' => "App\Models\CartRule",
                        'data_source' => url("ajax/cart/rule"),
                        'placeholder' => 'Search cart rule name/code',
                        'minimum_input_length' => 3 

                    ],
                    // [
                    //     'name' => 'name',
                    //     'label' => 'Name',
                    //     'type' => 'cart_rule_text',   
                    //     'attributes'    => [
                    //         'readonly'  => true,
                    //         'min'       => 1
                    //     ],

                    // ],
                    [
                        'name' => 'code',
                        'label' => 'Code',
                        'type' => 'cart_rule_text',   
                        'attributes'    => [
                            'readonly'  => true,
                            'min'       => 1
                        ],

                    ],

                    [
                        'label' => 'Reduction',
                        'name' => 'reduction_amount',
                        'type' => 'cart_rule_discount_type',
                        'attributes' => [
                            'type' => 'text',
                            'readonly' =>true,
                        ]
                    ],
                    [
                        'label' => 'Discount type',
                        'name' => 'discount_type',
                        'type' => 'cart_rule_text',
                        'attributes'    => [
                            'readonly'  => true,
                        ],
                    ],
                    
                ],
                'max' => 1000, // maximum rows allowed in the table
                'min' => 0 // minimum rows allowed in the table
            ],
                
        ]);
        $this->crud->addFields([
            [
                'label' => 'Shipping fee',
                'type' => 'text',
                'name' => 'total_shipping',
                'tab'   => 'General',
                // 'prefix' => 'currency',
                'currency' => true,
                'attributes'    => [
                        'readonly'  => 'readonly',
                    ],
            ],
             [
                'label' => 'Discount',
                'type' => 'text',
                'name' => 'total_discount',
                'tab'   => 'General',
                // 'prefix' => 'currency',
                'currency' => true,
                'attributes'    => [
                        'readonly'  => 'readonly',
                    ],
            ],
            [
                'label' => 'Total',
                'type' => 'text',
                'name' => 'total',
                'tab'   => 'General',
                // 'prefix' => 'currency',
                'currency' => true,
                'attributes'    => [
                        'readonly'  => 'readonly',
                    ],
            ],
                
        ]);

        $this->crud->addFields([
            // [
            //     'name'       => 'shipping_address_id',
            //     'label'      => 'Select a existing address',
            //     'type'       => 'select2',
            //     'entity'     => 'shippingAddress',
            //     'attribute'  => 'address1',
            //     'model'      => "App\Models\Address",
            //     'attributes' => ['id'=>'shipping_address_id'],

            //        // TAB
            //     'tab'   => 'Shipping Address',
            // ],
            [
                'label' => 'Search a existing user address',
                'type' => 'order_address_ajax',
                'name' => 'shipping_address_id',
                'entity' => 'shippingAddress',
                'attribute' => "address1",
                'attribute2' => 'city',
                'pivot_name' => 'shipping_address',
                'model'      => "App\Models\Address",
                'data_source' => url("ajax/client/specific/addresses"),
                'placeholder' => "Search a user address",
                'minimum_input_length' => 3,
                'tab'   => 'Shipping Address',
                'columns' => [

                                [
                                    'label' => 'Contact Person',
                                    'type' => 'pivot_text',
                                    'name' => 'shipping_address[name]',
                                    'attribute' => 'name',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'shipping_address_pivot_name'
                                    ],
                                ],
                                [
                                    'label' => 'Mobile phone',
                                    'type' => 'pivot_text',
                                    'name' => 'shipping_address[mobile_phone]',
                                    'attribute' => 'mobile_phone',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'shipping_address_pivot_mobile_phone'
                                    ],
                                ],
                                [
                                    'label' => 'Email',
                                    'type' => 'pivot_text',
                                    'name' => 'shipping_address[email]',
                                    'attribute' => 'email',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'shipping_address_pivot_email'
                                    ],
                                ],
                                [
                                    'label' => 'Country',
                                    'type' => 'pivot_select2',
                                    'name' => 'shipping_address[country_id]',
                                    'entity' => 'country',
                                    'attribute' => 'name',
                                    'model' => "App\Models\Country",
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'shipping_address_pivot_country'
                                    ],
                                ],
                                [
                                    'label' => 'Province / County',
                                    'type' => 'pivot_text',
                                    'name' => 'shipping_address[county]',
                                    'attribute' => 'county',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'shipping_address_pivot_county'
                                    ],
                                ],
                                [
                                    'label' => 'Unit no. / Home no.',
                                    'type' => 'pivot_text',
                                    'name' => 'shipping_address[unit]',
                                    'attribute' => 'unit',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'shipping_address_pivot_unit'
                                    ],
                                ],
                                [
                                    'label' => 'Building',
                                    'type' => 'pivot_text',
                                    'name' => 'shipping_address[building]',
                                    'attribute' => 'building',
                                    'attributes'    => [
                                        'id'    => 'shipping_address_pivot_building'
                                    ],
                                ],
                                [
                                    'label' => 'Street',
                                    'type' => 'pivot_text',
                                    'name' => 'shipping_address[street]',
                                    'attribute' => 'street',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'shipping_address_pivot_street'
                                    ],
                                ],
                                 [
                                    'label' => 'Barangay',
                                    'type' => 'pivot_text',
                                    'name' => 'shipping_address[barangay]',
                                    'attribute' => 'barangay',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'shipping_address_pivot_barangay'
                                    ],
                                ],
                                [
                                    'label' => 'City',
                                    'type' => 'pivot_text',
                                    'name' => 'shipping_address[city]',
                                    'attribute' => 'city',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'shipping_address_pivot_city'
                                    ],
                                ],
                                [
                                    'label' => 'Postal Code',
                                    'type' => 'pivot_text',
                                    'name' => 'shipping_address[postal_code]',
                                    'attribute' => 'postal_code',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'shipping_address_pivot_postal_code'
                                    ],
                                ],
                                [
                                    'label' => 'Region',
                                    'type' => 'pivot_text',
                                    'name' => 'shipping_address[region]',
                                    'attribute' => 'region',
                                    'attributes'    => [
                                        'id'    => 'shipping_address_pivot_region'
                                    ],
                                ],
                                [
                                    'label' => 'Phone',
                                    'type' => 'pivot_text',
                                    'name' => 'shipping_address[phone]',
                                    'attribute' => 'phone',
                                    'attributes'    => [
                                        'id'    => 'shipping_address_pivot_phone'
                                    ],
                                ],
                                [
                                    'label' => 'Comment',
                                    'type' => 'pivot_textarea',
                                    'name' => 'shipping_address[comment]',
                                    'attribute' => 'comment',
                                    'attributes'    => [
                                        'id'    => 'shipping_address_pivot_comment'
                                    ],
                                ],
                            ],
                                             
            ]
           
        ]);
        $this->crud->addFields([
            // [
            //     'name'       => 'billing_address_id',
            //     'label'      => 'Select a existing address',
            //     'type'       => 'select2',
            //     'entity'     => 'shippingAddress',
            //     'attribute'  => 'address1',
            //     'model'      => "App\Models\Address",
            //     'attributes' => ['id'=>'billing_address_id'],

            //        // TAB
            //     'tab'   => 'Shipping Address',
            // ],
            [
                'label' => 'Search a existing address',
                'type' => 'order_address_ajax',
                'name' => 'billing_address_id',
                'entity' => 'billingAddress',
                'attribute' => "address1",
                'attribute2' => 'city',
                'pivot_name' => 'billing_address',
                'model'      => "App\Models\Address",
                'data_source' => url("ajax/client/specific/addresses"),
                'placeholder' => "Search a user address",
                'minimum_input_length' => 3,
                'tab'   => 'Billing Address',
                'columns' => [

                                [
                                    'label' => 'Contact Person',
                                    'type' => 'pivot_text',
                                    'name' => 'billing_address[name]',
                                    'attribute' => 'name',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'billing_address_pivot_name'
                                    ],
                                ],
                                [
                                    'label' => 'Mobile phone',
                                    'type' => 'pivot_text',
                                    'name' => 'billing_address[mobile_phone]',
                                    'attribute' => 'mobile_phone',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'billing_address_pivot_mobile_phone'
                                    ],
                                ],
                                [
                                    'label' => 'Country',
                                    'type' => 'pivot_select2',
                                    'name' => 'billing_address[country_id]',
                                    'entity' => 'country',
                                    'attribute' => 'name',
                                    'model' => "App\Models\Country",
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'billing_address_pivot_country'
                                    ],
                                ],
                                [
                                    'label' => 'Province / County',
                                    'type' => 'pivot_text',
                                    'name' => 'billing_address[county]',
                                    'attribute' => 'county',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'billing_address_pivot_county'
                                    ],
                                ],
                                [
                                    'label' => 'Unit no. / Home no.',
                                    'type' => 'pivot_text',
                                    'name' => 'billing_address[unit]',
                                    'attribute' => 'unit',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'billing_address_pivot_unit'
                                    ],
                                ],
                                [
                                    'label' => 'Building',
                                    'type' => 'pivot_text',
                                    'name' => 'billing_address[building]',
                                    'attribute' => 'building',
                                    'attributes'    => [
                                        'id'    => 'billing_address_pivot_building'
                                    ],
                                ],
                                [
                                    'label' => 'Street',
                                    'type' => 'pivot_text',
                                    'name' => 'billing_address[street]',
                                    'attribute' => 'street',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'billing_address_pivot_street'
                                    ],
                                ],
                                 [
                                    'label' => 'Barangay',
                                    'type' => 'pivot_text',
                                    'name' => 'billing_address[barangay]',
                                    'attribute' => 'barangay',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'billing_address_pivot_barangay'
                                    ],
                                ],
                                [
                                    'label' => 'City',
                                    'type' => 'pivot_text',
                                    'name' => 'billing_address[city]',
                                    'attribute' => 'city',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'billing_address_pivot_city'
                                    ],
                                ],
                                [
                                    'label' => 'Postal Code',
                                    'type' => 'pivot_text',
                                    'name' => 'billing_address[postal_code]',
                                    'attribute' => 'postal_code',
                                    'attributes'    => [
                                        // 'required'  => true,
                                        'id'    => 'billing_address_pivot_postal_code'
                                    ],
                                ],
                                [
                                    'label' => 'Region',
                                    'type' => 'pivot_text',
                                    'name' => 'billing_address[region]',
                                    'attribute' => 'region',
                                    'attributes'    => [
                                        'id'    => 'billing_address_pivot_region'
                                    ],
                                ],
                                [
                                    'label' => 'Phone',
                                    'type' => 'pivot_text',
                                    'name' => 'billing_address[phone]',
                                    'attribute' => 'phone',
                                    'attributes'    => [
                                        'id'    => 'billing_address_pivot_phone'
                                    ],
                                ],
                                [
                                    'label' => 'Comment',
                                    'type' => 'pivot_textarea',
                                    'name' => 'billing_address[comment]',
                                    'attribute' => 'comment',
                                    'attributes'    => [
                                        'id'    => 'billing_address_pivot_comment'
                                    ],
                                ],
                            ],
                                             
            ]
           
        ]);

        /*
        |--------------------------------------------------------------------------
        | PERMISSIONS
        |-------------------------------------------------------------------------
        */
        $this->setPermissions();

        /*
        |--------------------------------------------------------------------------
        | FIELDS
        |--------------------------------------------------------------------------
        */
        // $this->setFields();

        /*
        |--------------------------------------------------------------------------
        | AJAX TABLE VIEW
        |--------------------------------------------------------------------------
        */
        // $this->crud->enableAjaxTable();

        // Exclude orders that are still in Cart status
        $this->crud->addClause('whereHas', 'status', function($query) {
            $query->where('name', '!=', 'Cart');
        });
    }

    public function setPermissions()
    {
        // Get authenticated user
        $user = backpack_auth()->user();

        // Deny all accesses
        $this->crud->denyAccess(['delete']);

        // Allow access to show and replace preview button with view
        $this->crud->allowAccess('show');
       
        // $this->crud->addButtonFromView('line', 'view', 'view', 'end');
        // $this->crud->removeButton('preview');
    }

    // public function setFields()
    // {
    // }

    public function show($id)
    {
        $this->crud->hasAccessOrFail('show');

        $order = $this->crud->getEntry($id);
        $orderStatuses = OrderStatus::get();
        $crud = $this->crud;

        return view('admin.orders.view', compact('crud', 'order', 'orderStatuses'));
    }

    public function updateStatus(Request $request, OrderStatusHistory $orderStatusHistory)
    {
        // $request->request->add(['created_by' => backpack_auth()->user()->id]);

        // $orderStatusHistory->create(array_merge($request->except('_token'), ['created_by' => backpack_auth()->user()->id]));

        $order_update_status = $this->crud->update($request->input('order_id'), ['status_id' => $request->input('status_id')]);
        if($order_update_status && $request->input('status_id') == 7){
            $magpie = new Magpie();
            $magpie->refundCharge($order_update_status->magpie_charge_id, $order_update_status->total);
        }
        \Alert::success(trans('order.status_updated'))->flash();

        return redirect()->back();
    }

    public function store(StoreRequest $request)
    {
        // \Log::info($request);
        // return;
        $redirect_location = parent::storeCrud();
        $products = json_decode($request->input('products'));
        $cart_rules = json_decode($request->input('order_cart_rule'));
        if(!empty($cart_rules)){
            foreach ($cart_rules as $cart_rule) {
                $cartRule = new OrderCartRule;
                $cartRule->order_id = $this->crud->entry->id;
                $cartRule->cart_rule_id = $cart_rule->cart_rule_id;
                $cartRule->priority = $cart_rule->index;
                $cartRule->name = $cart_rule->name;
                $cartRule->code = $cart_rule->code;
                $cartRule->start_date = $cart_rule->start_date;
                $cartRule->expiration_date = $cart_rule->expiration_date;
                $cartRule->free_delivery = $cart_rule->free_delivery;
                $cartRule->discount_type = $cart_rule->discount_type;
                $cartRule->reduction_amount = $cart_rule->reduction_amount;
                $cartRule->reduction_currency_id = $cart_rule->reduction_currency_id;
                $cartRule->gift = $cart_rule->gift_product_id;
                $cartRule->total_discount = $cart_rule->subtotal_discount;
                $cartRule->save();
            } 
        }
        
        foreach ($products as $product) {
            $check_product = Product::find(isset($product->product_id) ? $product->product_id : $product->id);
            $tax = Tax::find($check_product->tax_id);
            $amount_per_item_with_tax = 0;

            //specific price variables;
            $s_id = $product->specificPrice->id ?? null;
            $s_discount_type = $product->specificPrice->discount_type ?? null;
            $s_reduction = $product->specificPrice->reduction ?? null;
            $s_start_date = $product->specificPrice->start_date ?? null;
            $s_expiration_date = $product->specificPrice->expiration_date ?? null;
            $this->crud->entry->products()->attach($check_product->id,['order_id'=>$this->crud->entry->id,
                                                             'product_id'=>$check_product->id,
                                                             'name' => $check_product->name,
                                                             'sku' => $check_product->sku,
                                                             'price' => $check_product->price,
                                                             'price_without_tax' => $check_product->price/1.12,
                                                             'quantity' => $product->quantity,
                                                             'specific_price_id' => $s_id ,
                                                             'specific_price_reduction' => $s_reduction,
                                                             'specific_price_discount_type' => $s_discount_type,
                                                             'specific_price_start_date' =>$s_start_date ,
                                                             'specific_price_expiration_date' => $s_expiration_date,
                                                             'amount_per_item' => $this->computeAmountPerItem($check_product->price,$s_reduction, $s_discount_type),
                                                             'total' => $this->computeAmountPerItem($check_product->price,$s_reduction, $s_discount_type) * ($product->quantity ?? $product->pivot->quantity)  , 
                                                            ]
                                                    );
        }
        if(!$request->has('shipping_address_id')){
            $address = new Address;
            // Log::info($request->shipping_address['address1']);
            foreach ($request->shipping_address as $key => $value) {
                $address->$key = $value;
            }
            if($address->save()){
               $this->crud->entry->shipping_address_id = $address->id;
                $this->crud->entry->save();
            }
        }
        if(!$request->has('billing_address_id')){
            $address = new Address;
            // Log::info($request->shipping_address['address1']);
            foreach ($request->billing_address as $key => $value) {
                $address->$key = $value;
            }
            if($address->save()){
               $this->crud->entry->billing_address_id = $address->id; 
               $this->crud->entry->save();
            }
        }
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {   
        // \Log::info($request);
        // return;
        $products = json_decode($request->input('products'));
        $order = Order::find($request->id);
        $order->status_id = 6;
        $order->save();
        $redirect_location = parent::storeCrud($request);
        $cart_rules = json_decode($request->input('order_cart_rule'));
        if(count($cart_rules)> 0){
            foreach ($cart_rules as $cart_rule) {
                $cartRule = new OrderCartRule;
                $cartRule->order_id = $this->crud->entry->id;
                $cartRule->cart_rule_id = $cart_rule->cart_rule_id;
                $cartRule->priority = $cart_rule->index;
                $cartRule->name = $cart_rule->name;
                $cartRule->code = $cart_rule->code;
                $cartRule->start_date = $cart_rule->start_date;
                $cartRule->expiration_date = $cart_rule->expiration_date;
                $cartRule->free_delivery = $cart_rule->free_delivery;
                $cartRule->discount_type = $cart_rule->discount_type;
                $cartRule->reduction_amount = $cart_rule->reduction_amount;
                $cartRule->reduction_currency_id = $cart_rule->reduction_currency_id;
                $cartRule->gift = $cart_rule->gift_product_id;
                $cartRule->total_discount = $cart_rule->subtotal_discount;
                $cartRule->save();
            } 
        }
        
        foreach ($products as $product) {
            $s_id = $product->specificPrice->id ?? ($product->specificPrice->specific_price_id ?? null);
            $s_discount_type = $product->specificPrice->discount_type ?? ($product->specificPrice->specific_price_discount_type ?? null);
            $s_reduction = $product->specificPrice->reduction ?? ($product->specificPrice->specific_price_reduction ?? null);
            $s_start_date = $product->specificPrice->start_date ?? ($product->specificPrice->specific_price_start_date ?? null);
            $s_expiration_date = $product->specificPrice->expiration_date ?? ($product->specificPrice->specific_price_expiration_date ?? null);
            if(isset($product->pivot)){
                $check_product = Product::find(isset($product->product_id) ? $product->product_id : $product->pivot->product_id);
                $tax = Tax::find($check_product->tax_id);
                $this->crud->entry->products()->attach($check_product->id,[
                                                             'order_id'=>$this->crud->entry->id,
                                                             'product_id'=> $check_product->id,
                                                             'name' => $check_product->name,
                                                             'sku' => $check_product->sku,
                                                             'price' => $check_product->price,
                                                             'price_without_tax' => $check_product->price / 1.12,
                                                             'quantity' => isset($product->quantity) ? $product->quantity : $product->pivot->quantity,
                                                             'specific_price_id' => $s_id,
                                                             'specific_price_reduction' => $s_reduction,
                                                             'specific_price_discount_type' => $s_discount_type,
                                                             'specific_price_start_date' => $s_start_date,
                                                             'specific_price_expiration_date' => $s_expiration_date,
                                                             'amount_per_item' =>  $this->computeAmountPerItem($check_product->price,$s_reduction, $s_discount_type),
                                                             'total' => $check_product->price * ($product->quantity ?? $product->pivot->quantity), 
                                                            ]);  
            }else{
                $check_product = Product::find(isset($product->product_id) ? $product->product_id : $product->id);
                $tax = Tax::find($check_product->tax_id);
                $this->crud->entry->products()->attach($check_product->id,['order_id'=>$this->crud->entry->id,
                                                             'product_id'=>$check_product->id,
                                                             'name' => $check_product->name,
                                                             'sku' => $check_product->sku,
                                                             'price' => $check_product->price,
                                                             'price_without_tax' => $check_product->price / 1.12,
                                                             'quantity' => isset($product->quantity) ? $product->quantity : $product->pivot->quantity,
                                                             'specific_price_id' => $s_id,
                                                             'specific_price_reduction' => $s_reduction,
                                                             'specific_price_discount_type' => $s_discount_type,
                                                             'specific_price_start_date' => $s_start_date,
                                                             'specific_price_expiration_date' => $s_expiration_date,
                                                             'amount_per_item' =>  $this->computeAmountPerItem($check_product->price,$s_reduction, $s_discount_type),
                                                             'total' => $check_product->price * ($product->quantity ?? $product->pivot->quantity), 
                                                         ]);
            }
      
        }
        if($request->has('shipping_address') && $request->input('shipping_address_id') == ''){
            $address = new Address;
            // Log::info($request->shipping_address['address1']);
            foreach ($request->shipping_address as $key => $value) {
                $address->$key = $value;
            }
            if($address->save()){
               $this->crud->entry->shipping_address_id = $address->id;
                $this->crud->entry->save();
            }
        }
        if($request->has('billing_address') && $request->input('billing_address_id') == ''){
            $address = new Address;
            // Log::info($request->shipping_address['address1']);
            foreach ($request->billing_address as $key => $value) {
                $address->$key = $value;
            }
            if($address->save()){
               $this->crud->entry->billing_address_id = $address->id; 
               $this->crud->entry->save();
            }
        }
        return $redirect_location;
    }

    public function computeAmountPerItem($price,$reduction,$reduction_type){
        if($reduction_type =="Percent"){
            return $price - ( ($reduction/100) * $price);
        }else{
            return $price - $reduction;
        }
    }

    public function create()
    {
        $this->crud->hasAccessOrFail('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = trans('backpack::crud.add').' '.$this->crud->entity_name;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('admin.orders.create', $this->data);
    }
    public function edit($id)
    {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('admin.orders.edit', $this->data);
    }



    // private function computeSpecificDiscountPerItem($specificPrice, $price, $quantity){
    //     $discount = 0;
    //     if($pecificPrice->discount_type == 'Percent'){
    //         $discount = ($specificPrice->reduction / 100) * $price
    //             return $discount;  
    //         }else{
    //             $discount = $specificPrice->reduction)
    //         return $discount; 
    //         }
             
    //     }

    // }

}
