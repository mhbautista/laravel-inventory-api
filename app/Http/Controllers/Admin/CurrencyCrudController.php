<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CurrencyRequest as StoreRequest;
use App\Http\Requests\CurrencyRequest as UpdateRequest;

/**
 * Class CurrencyCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CurrencyCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Currency');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/currency');
        $this->crud->setEntityNameStrings('currency', 'currencies');

        $this->crud->addColumns([
            [
                'name'  => 'name',
                'label' => trans('currency.name'),
            ],
            [
                'name'  => 'code',
                'label' => trans('currency.code'),
            ],
            [
                'name'  => 'symbol',
                'label' => trans('currency.symbol'),
            ],
        
        ]);

        $this->setPermissions();
        $this->setFields();
        $this->crud->enableAjaxTable();
        // add asterisk for fields that are required in CurrencyRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }
     public function setPermissions()
    {
        // Get authenticated user
        $user = auth()->user();

        // Deny all accesses
        $this->crud->denyAccess(['list', 'create', 'update', 'delete']);

        // Allow list access
        if ($user->can('list_currencies')) {
            $this->crud->allowAccess('list');
        }

        // Allow create access
        if ($user->can('create_currency')) {
            $this->crud->allowAccess('create');
        }

        // Allow update access
        if ($user->can('update_currency')) {
            $this->crud->allowAccess('update');
        }

        // Allow delete access
        if ($user->can('delete_currency')) {
            $this->crud->allowAccess('delete');
        }
    }

    public function setFields()
    {
        $this->crud->addFields([
            [
                'name'  => 'name',
                'label' => trans('currency.name'),
                'type'  => 'text',
            ],
            [
                'name'  => 'code',
                'label' => trans('currency.code'),
                'type'  => 'text',
            ],
            [
                'name'  => 'symbol',
                'label' => trans('currency.symbol'),
                'type' => 'select_from_array',
                'options' => ['&#8369;' => '&#8369; PHP', '&#36;' => '&#36; USD', '&#8364;' => '&#8364; EUR', '&#82;&#112;' => '&#82;&#112; IDR'],
                'allows_null' => true,
            ],
        ]);
    }

    public function store(StoreRequest $request)
    {
        if ($request->default == 1) {
            $table = $this->crud->model->getTable();
            DB::table($table)->update(['default' => 0]);
        }

        $redirect_location = parent::storeCrud($request);

        if ($request->default == 1) {
            $this->crud->model->find($this->crud->entry->id)->update(['value' => 1]);
        }
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
         if ($request->default == 1) {
            $table = $this->crud->model->getTable();
            DB::table($table)->update(['default' => 0]);
        }

        $redirect_location = parent::updateCrud($request);

        if ($request->default == 1) {
            $this->crud->model->find($request->id)->update(['value' => 1]);
        }
        return $redirect_location;
    }
}
