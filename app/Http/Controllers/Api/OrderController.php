<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::whereHas('status', function ($query) {
                $query->where('name', 'Paid');
            })
            ->with([
                'user',
                'products',
                'carrier',
                'shippingAddress',
                'billingAddress',
                'shippingAddress.country',
                'billingAddress.country',
            ])
            ->get();
        
        return response()->json($orders);
    }

    public function show(Request $request, $id)
    {
        $order = Order::with([
            'user',
            'status',
            'carrier',
            'shippingAddress',
            'billingAddress',
            'products',
        ])->find($id);

        return response()->json($order);
    }

    public function showStatus(Request $request, $status)
    {
        $orders = Order::whereHas('status', function ($query) use ($status) {
                $query->where('name', $status);
            })
            ->with([
                'user',
                'products',
                'carrier',
                'shippingAddress',
                'billingAddress',
                'shippingAddress.country',
                'billingAddress.country',
            ])
            ->get();
        
        return response()->json($orders);
    }

    //   public function show($id){
    //       $post = Post::find($id);
    //       $post_meta_thumbnail_id = PostMeta::where('post_id', $post->ID)->where('meta_key','_thumbnail_id')->first();
    // $feature_image = NULL;
    // if(!is_null($post_meta_thumbnail_id)){
    //     $feature_image = Post::find($post_meta_thumbnail_id->meta_value);
    // }

    // if(!is_null($feature_image)) {
    //     $feature_image = $feature_image->guid;
    // }
    // $getPost = array('id'=>$post->ID,
    //                  'title'=>$post->post_title,
    //                  'content'=>$post->post_content,
    //                  'feature_image'=> $feature_image,
    //                  'datetime'=>$post->post_date
    //                 );
    //       return response()->json($getPost);
    //   }

    public function update(Request $request, $id)
    {
        $order = Order::find($id);

        $order->update($request->all());

        return response()->json($order);
    }
}
