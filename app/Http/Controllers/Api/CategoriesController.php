<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function index()
    {
    	$categories = Category::all();

    	return response()->json($categories);
    }

    public function show(Request $request, $id)
    {
        $category = Category::find($id);

        $category->update($request->all());

        return response()->json($category);
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        $category->update($request->all());

        return response()->json($category);
    }
}
