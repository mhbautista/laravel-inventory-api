<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\UpdateProductRequest;
use App\Models\Product;

class ProductsController extends Controller
{
	public function index()
	{
		$products = Product::all();

		return response()->json($products);
	}

    public function update(UpdateProductRequest $request, $id)
    {
        $product = Product::findOrFail($id);

        $product->update($request->all());

        return response()->json($product);
    }
}
