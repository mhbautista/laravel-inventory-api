<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class ShippingDimensionRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'min_length' => 'required|numeric|min:0',
            'max_length' => 'required|numeric|min:0|gt:min_length',
            'min_width' => 'required|numeric|min:0',
            'max_width' => 'required|numeric|min:0|gt:min_width',
            'min_height' => 'required|numeric|min:0',
            'max_height' => 'required|numeric|min:0|gt:min_height',
            'unit' => 'required|in:cm,m',
            'price' => 'required|numeric|min:1',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            // 
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
