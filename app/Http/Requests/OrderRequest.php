<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shipping_address.name' => 'required',
            'shipping_address.country_id' => 'required',
            'shipping_address.mobile_phone' => 'required',
            'shipping_address.unit' => 'required',
            'shipping_address.street' => 'required',
            'shipping_address.barangay' => 'required',
            'shipping_address.city' => 'required',
            'shipping_address.county' => 'required',
            'shipping_address.postal_code' => 'required',
            'shipping_address.region' => 'required',
            'shipping_address.email' => 'required',
            'billing_address.name' => 'required',
            'billing_address.country_id' => 'required',
            'billing_address.mobile_phone' => 'required',
            'billing_address.unit' => 'required',
            'billing_address.street' => 'required',
            'billing_address.barangay' => 'required',
            'billing_address.city' => 'required',
            'billing_address.county' => 'required',
            'billing_address.postal_code' => 'required',
            'billing_address.region' => 'required',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            // 'shipping_address.name.required' => 'Shipping address is required',
        ];
    }
}
