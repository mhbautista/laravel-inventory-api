<?php

namespace App\Http\Middleware;

use Closure;

class CheckIfAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check()) {
            if (!\Auth::user()->isAdmin()) {
                if ($request->ajax() || $request->wantsJson()) {
                    return response(trans('backpack::base.unauthorized'), 401);
                } else {
                    \Auth::logout();
                    return redirect()->guest(backpack_url('login'));
                }
            }
        } else {
            if ($request->ajax() || $request->wantsJson()) {
                return response(trans('backpack::base.unauthorized'), 401);
            } else {
                return redirect()->guest(backpack_url('login'));
            }
        }

        return $next($request);
    }
}
