<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Product extends Model
{
    use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

    protected $table = 'products';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
    	'group_id',
    	'attribute_set_id',
    	'name',
    	'description',
    	'price',
    	'tax_id',
    	'sku',
    	'stock',
    	'active',
    	'created_at',
    	'updated_at',
        'country_id',
        'slug',
        'video_embeds',
	];
    // protected $hidden = array('pivot');
    // protected $dates = [];

    /*
	|--------------------------------------------------------------------------
	| EVENTS
	|--------------------------------------------------------------------------
	*/
	protected static function boot()
    {
        parent::boot();

        static::deleting(function($model) {
            $model->categories()->detach();
            $model->attributes()->detach();

            // Delete product images
            $disk = 'products';

            foreach ($model->images as $image) {
                // Delete image from disk
                if (\Storage::disk($disk)->has($image->name)) {
                    \Storage::disk($disk)->delete($image->name);
                }

                // Delete image from db
                $image->delete();
            }
        });
    }

    /*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

    /*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/

	public function categories()
	{
		return $this->belongsToMany('App\Models\Category');
	}

    public function attribute_set(){
        return $this->belongsTo('App\Models\AttributeSet');
    }

	public function attributes()
	{
		return $this->belongsToMany('App\Models\Attribute', 'attribute_product_value', 'product_id', 'attribute_id')->withPivot('value');
	}

	public function tax()
	{
		return $this->belongsTo('App\Models\Tax');
	}

    public function country()
    {
        return $this->hasOne('App\Models\Country');
    }

	public function images()
	{
		return $this->hasMany('App\Models\ProductImage')->orderBy('order', 'ASC');
	}

    public function group()
    {
        return $this->belongsTo('App\Models\ProductGroup');
    }

    public function cartRules()
    {
        return $this->belongsToMany('App\Models\CartRule');
    }

    public function specificPrice()
    {
        return $this->hasOne('App\Models\SpecificPrice');
    }

    public function product_badge(){
        return $this->hasOne('App\Models\ProductBadge');
    }

    public function product_measurement(){
        return $this->hasOne('App\Models\ProductMeasurement');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\OrderProduct');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeLoadCloneRelations($query)
    {
        $query->with('categories', 'attributes', 'images');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }


    /*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

    /**
     * Get the image path for the specified image size context.
     * @param  string $size
     * @return string
     */
    public function getImagePath($size = 'md')
    {
        $image = $this->images()->orderBy('order', 'desc')->first();
        $image_name = str_replace(['-lg.jpg', '.jpg', '-sm.jpg'], '', $image->name);
        $image_path = null;

        if ($size == 'lg')
            return url('uploads/products/' . $image_name . '-lg.jpg');
        if ($size == 'md')
            return url('uploads/products/' . $image_name . '.jpg');
        if ($size == 'sm')
            return url('uploads/products/' . $image_name . '-sm.jpg');
    }
    
    public function getThumbnailPathAttribute()
    {
        $first_image = $this->images()->orderBy('order', 'desc')->first();

        if ($first_image)
            return url('uploads/products/' . str_replace('.jpg', '-sm.jpg', $first_image->name));
        else
            return null;
    }

    /*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}
