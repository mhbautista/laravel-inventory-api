<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use PragmaRX\Countries\Package\Countries;

class Address extends Model
{
    use CrudTrait;

    /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

    protected $table = 'addresses';
    //protected $primaryKey = 'id';
    // public $timestamps = true;
    // protected $guarded = ['id'];
    protected $fillable = [
    	'user_id',
    	'country_id',
    	'name',
    	'address1',
    	'address2',
    	'county',
    	'city',
    	'postal_code',
    	'phone',
    	'mobile_phone',
    	'comment',
    	'email',
    	'unit',
    	'house',
    	'building',
    	'street',
    	'barangay',
    	'town',
    	'state',
        'country_cca2',
        'state_postal',
	];
    // protected $hidden = [];
    // protected $dates = [];

    /*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

    /*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/
	public function country()
	{
		return $this->hasOne('App\Models\Country', 'id', 'country_id');
	}

    /*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

    /*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

    /**
     * Get the state_name attribute
     * @return string
     */
    public function getStateNameAttribute()
    {
        if (isset($this->cca2))
            return Countries::all()->where('cca2', $this->country_cca2)->first()->hydrateStates()->states[$this->state_postal]['name'];
        else
            return 'Philippines';
    }

    /**
     * Get the country_name attribute
     * @return string
     */
    public function getCountryNameAttribute()
    {
        if (isset($this->cca2))
            return Countries::all()->where('cca2', $this->country_cca2)->first()->name->common;
        else
            return 'Philippines';
    }

    /*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}
