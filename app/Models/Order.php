<?php

namespace App\Models;

use App\Mail\NotificationTemplateMail;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\OrderStatusHistory;
class Order extends Model
{

    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'orders';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'status_id',
        'comment',
        'invoice_date',
        'delivery_date',
        'shipping_address_id',
        'billing_address_id',
        'total_discount',
        'total_discount_tax',
        'total_shipping',
        'total_shipping_tax',
        'total',
        'total_tax',
        'carrier_id',
        'currency_id',
        'user_id',
        'magpie_charge_id'
    ];
    // protected $hidden = [
    //     'id',
    // ];

    // protected $hidden = [];
    // protected $dates = [];
    public $notificationVars = [
        'userSalutation',
        'userName',
        'userEmail',
        'carrier',
        'total',
        'status'
    ];

    /*
    |--------------------------------------------------------------------------
    | NOTIFICATIONS VARIABLES
    |--------------------------------------------------------------------------
    */
    public function notificationVariables()
    {
        return [
            'userSalutation' => $this->user->salutation,
            'userName'       => $this->user->name,
            'userEmail'      => $this->user->email,
            'total'          => $this->total(),
            'carrier'        => $this->carrier()->first()->name,
            'status'         => $this->status->name
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | EVENTS
    |--------------------------------------------------------------------------
    */
    protected static function boot()
    {
        parent::boot();
        static::created(function($order) {
            $order_status_history = new OrderStatusHistory;
            $order_status_history->status_id = $order->status_id;
            $order_status_history->order_id = $order->id;
            $order_status_history->created_by = \Auth::user()->id;
            $order_status_history->save();
        });
        static::updated(function($order) {
            $oldStatus = $order->getOriginal();
            if ($order->status_id != $oldStatus['status_id']){
                $order_status_history = new OrderStatusHistory;
                $order_status_history->status_id = $order->status_id;
                $order_status_history->order_id = $order->id;
                $order_status_history->created_by = null !== \Auth::user() ? \Auth::user()->id : 0;
                $order_status_history->save(); 
            }
            
            // Send notification when order status was changed
            // if ($order->status_id != $oldStatus['status_id'] && $order->status->notification != 0) {
            //     // example of usage: (be sure that a notification template mail with the slug "example-slug" exists in db)
            //     return \Mail::to($order->user->email)->send(new NotificationTemplateMail($order, "order-status-changed"));
            // }
        });
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function total()
    {
        return decimalFormat($this->products->sum(function ($product) {
            return $product->pivot->amount_per_item * $product->pivot->quantity;
        }, 0) + $this->carrier->price);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function user()
    {
        // return $this->belongsTo('App\User');
        return $this->belongsTo('App\User');
    }

    // public function order_status()
    // {
    //     return $this->belongsTo('App\Models\OrderStatus', 'status_id', 'id');
    // }
    public function status()
    {
        return $this->belongsTo('App\Models\OrderStatus');
    }

    public function statusHistory()
    {
        return $this->hasMany('App\Models\OrderStatusHistory')->orderBy('created_at', 'DESC');
    }

    public function carrier()
    {
        return $this->belongsTo('App\Models\Carrier');
    }

    public function shippingAddress()
    {
        return $this->belongsTo('App\Models\Address');
    }

    public function billingAddress()
    {
        return $this->belongsTo('App\Models\Address');
    }

    public function billingCompanyInfo()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function currency()
    {
        return $this->belongsTo('App\Models\Currency');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product')->withPivot('name', 'sku', 'price', 'price_without_tax', 'quantity','specific_price_id','specific_price_reduction','specific_price_discount_type','specific_price_start_date','specific_price_expiration_date','amount_per_item')->select(['id','stock']);
    }
    public function showFullname()
    {
        return $this->user()->fullname; // make sure you call fresh instance or you'll get an error that fullname is not found or something like that...
    }
     public function order_cart_rule()
    {
        return $this->hasMany('App\Models\OrderCartRule')->orderBy('priority', 'ASC');
    }

    public function order_products()
    {
        return $this->hasMany('App\Models\OrderProduct');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */
   
    /**
     * Get the date_cancelled attribute
     * @return string
     */
    public function getDateCancelledAttribute()
    {
        $cancellation = $this->statusHistory()->where('status_id', 6)->first();

        if ($cancellation) {
            return $cancellation->created_at;
        } else {
            return null;
        }
    }

    /**
     * Get the voucher attribute
     * @return string
     */
    public function getVoucherAttribute()
    {
        if ($this->order_cart_rule()->count())
            return $this->order_cart_rule()->first()->code;
        else
            return null;
    }

    /**
     * Get the voucher_amount attribute
     * @return string
     */
    public function getVoucherAmountAttribute()
    {
        if ($this->order_cart_rule()->count())
            return $this->order_cart_rule()->first()->total_discount;
        else
            return null;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('d-m-Y H:i:s');
    }

    public function createdOrderHistory($order){
        $order_status_history = new OrderStatusHistory;
        $order_status_history->order_id = $order->id;
        $order_status_history->created_by = \Auth::user()->id;
        $order_status_history->save();
    }
}
