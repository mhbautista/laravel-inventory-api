<?php

namespace App\Models;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class Magpie {

	protected $testPublicKey;
	protected $testSecretKey;
	protected $livePublicKey;
	protected $liveSecretKey;

	protected $magpieClient;

	public function __construct()
	{
		$this->testPublicKey = env('MAGPIE_TEST_PUBLISHABLE_KEY');
		$this->testSecretKey = env('MAGPIE_TEST_SECRET_KEY');
		$this->livePublicKey = env('MAGPIE_LIVE_PUBLISHABLE_KEY');
		$this->liveSecretKey = env('MAGPIE_LIVE_SECRET_KEY');

		// The HTTP client for the OMS API.
        $this->magpieClient = new Client([
            // Base URI is used with relative requests
            'base_uri' => env('MAGPIE_BASE_URI'),
            'headers' => [
            	'Accept' => 'application/json',
            	'Content-Type' => 'application/json',
            ],
        ]);
	}

	/**
	 * Creates a single use token that wraps the details of a credit card.
	 * These tokens can only be used once: by creating a new charge object, or attaching them to a customer.
	 * @param  array  $card
	 * @return stdClass
	 */
	public function createToken(array $card)
	{
		// dd($card);
		try {
			$response = $this->magpieClient->post('tokens', [
	            'auth' => [$this->testPublicKey, null],
	            'form_params' => [
	                'card' => $card,
	                'address_line1' => null,
	                'address_line2' => null,
	                'address_state' => null,
	                'address_zip' => null,
	            ],
	        ]);
		} catch (ConnectException $e) {
            throw $e;
        } catch (ClientException $e) {
        	
        	return json_decode($e->getResponse()->getBody()->getContents(),true);
        	
        } catch (ServerException $e) {
        	throw $e;
        }

        return json_decode($response->getBody());
	}

	/**
	 * Retrieves the token with the given ID.
	 * @param  string $token_id
	 * @return stdClass
	 */
	private function retrieveToken($token_id)
	{
		try {
			$response = $this->magpieClient->get('tokens/' . $token_id, [
	            'auth' => [$this->testPublicKey, null],
	        ]);
		} catch (ConnectException $e) {
            throw $e;
        } catch (ClientException $e) {
    	 	return json_decode($e->getResponse()->getBody()->getContents(),true);
        } catch (ServerException $e) {
        	throw $e;
        }

        return json_decode($response->getBody());
	}

	/**
	 * To charge a credit card, you create a charge object.
	 * If your API key is in test mode, the supplied card won’t actually be charged,
	 * though everything else will occur as if in live mode.
	 * 
	 * @param  string $token_id
	 * @param  string $charge
	 * @return stdClass
	 */
	private function createCharge(string $token_id, array $charge)
	{
		// Sample charge data
		// $charge = [
		// 	'amount' => 100000,
		// 	'currency' => 'php',
		// 	'source' => $token_id,
		// 	'description' => 'Some Teviant products people',
		// 	'statement_descriptor' => 'Teviant',
		// 	'capture' => true,
		// ];

		if (!isset($token_id)) {
			$token_id = $this->createToken($card)->id;
		}

		try {
			$response = $this->magpieClient->post('charges', [
	            'auth' => [$this->testSecretKey, null],
	            'form_params' => array_merge($charge, [
	            	'currency' => 'php',
	            	'source' => $token_id,
	            	'description' => 'Product purchase',
	            	'statement_descriptor' => config('app.name'),
	            	'capture' => true,
	            ])
	        ]);
		} catch (ConnectException $e) {
            throw $e;
        } catch (ClientException $e) {
        	return json_decode($e->getResponse()->getBody()->getContents(),true);
        } catch (ServerException $e) {
        	throw $e;
        }

        return json_decode($response->getBody());
	}

	public function retrieveCharge($charge_id)
	{
		try {
			$response = $this->magpieClient->get('charges/' . $charge_id, [
	            'auth' => [$this->testSecretKey, null],
	        ]);
		} catch (ConnectException $e) {
            throw $e;
        } catch (ClientException $e) {
        	return json_decode($e->getResponse()->getBody()->getContents(),true);
        } catch (ServerException $e) {
        	throw $e;
        }

        return json_decode($response->getBody());
	}
	public function refundCharge(string $charge_id,  float $amount)
	{

		try {
			$response = $this->magpieClient->post('charges/'.$charge_id.'/refund', [
	            'auth' => [$this->testSecretKey, null],
	            'form_params' => array('amount' => $amount)
					            
	        ]);
		} catch (ConnectException $e) {
            throw $e;
        } catch (ClientException $e) {
        	return json_decode($e->getResponse()->getBody()->getContents(),true);
        } catch (ServerException $e) {
        	throw $e;
        }
        \Log::info(json_decode($response->getBody(),true));
        return json_decode($response->getBody());
	}

	/**
	 * Pay an amount via a card
	 * 
	 * @param  number     $amount
	 * @param  array      $card
	 * @param  array|null $options
	 * @return stdClass
	 */
	public function pay($amount, $token, array $options = [])
	{
		// $token = $this->createToken($card);

		$charge = array_merge([
			// Convert the amount into cents
			'amount' => $amount,
			'source' => $token,
		], $options);

		return $this->createCharge($token, $charge);
	}
}