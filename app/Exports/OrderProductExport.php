<?php

namespace App\Exports;

use App\Models\OrderProduct;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
// use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class OrderProductExport implements FromCollection, WithHeadings, ShouldAutoSize, WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	$order_products = OrderProduct::with(['order', 'product', 'order.statusHistory', 'order.status', 'order.order_cart_rule'])->get();
    	$order_products->sortByDesc('order.created_at');
    	$collection = collect([]);

    	// Explode order_product that has 2+ quantity
    	$order_products->each(function($order_product, $key) use ($collection) {
    		for($i = 0; $i < $order_product->quantity; $i++) {
    			// dd(Carbon::parse($order_product->order->created_at)->format('Y-m-d H:i:s'));
    			$collection->push([
    				$order_product->order->id,
    				$order_product->product->sku,
    				$order_product->product->name,
    				$order_product->product->price,
    				$order_product->order->status->name,
    				Carbon::parse(Carbon::parse($order_product->order->created_at)->format('Y-m-d H:i:s')),
    				Carbon::parse(Carbon::parse($order_product->order->date_cancelled)->format('Y-m-d H:i:s')),
    				$order_product->order->payment_method,
    				$order_product->order->magpie_charge_id,
    				$order_product->order->voucher,
    				$order_product->order->voucher_amount,
    				$order_product->order->total_shipping,
    				"" . $order_product->order->invoice_no,
    			]);
    		}
    	});

    	return $collection;
    }

    public function headings(): array
    {
        return [
            'Order #',
			'SKU',
			'Item Name',
			'Price',
			'Status',
			'Date Ordered',
			'Date Cancelled',
			'Payment Method',
			'Magpie Charge Id',
			'Voucher',
			'Voucher Amount',
			'Shipping Fee',
			'Reference #',
        ];
    }
    
    public function columnFormats(): array
    {

        return [
        	'A' => NumberFormat::FORMAT_TEXT,
        	'B' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'D' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'L' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'M' => NumberFormat::FORMAT_TEXT
        ];
    }
}
