<?php

namespace App\Exports;

use App\Models\OrderProduct;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class OrderProductExportFromView implements FromView, ShouldAutoSize, WithHeadings
{
    public function view(): View
    {
    	$order_products = OrderProduct::with(['order', 'product', 'order.statusHistory', 'order.order_cart_rule'])->get();

    	$order_products->sortByDesc('order.created_at');

        return view('exports.order_products', compact('order_products'));
    }

    public function headings(): array
    {
        return [
            'Order #',
			'SKU',
			'Item Name',
			'Price',
			'Status',
			'Date Ordered',
			'Date Cancelled',
			'Payment Method',
			'Magpie Charge Id',
			'Voucher',
			'Voucher Amount',
			'Shipping Fee',
			'Reference #',
        ];
    }

    // public function map($order_product): array
    // {
    //     return [
    //         $invoice->invoice_number,
    //         Date::dateTimeToExcel($invoice->created_at),
    //         $invoice->total
    //     ];
    // }
}
