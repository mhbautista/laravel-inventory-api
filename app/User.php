<?php

namespace App;

use Backpack\CRUD\CrudTrait;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;

class User extends Authenticatable
{
    use CrudTrait, HasRoles, Notifiable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    protected $table = 'users';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'password',
        'mobile_phone',
        'phone',
        'salutation',
        'birthday',
        'gender',
        'active',
        'default_shipping_address_id',
        'default_billing_address_id',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public $notificationVars = [
        'userSalutation',
        'userFirstname',
        'userEmail',
        'age',
    ];

    /*
    |--------------------------------------------------------------------------
    | NOTIFICATIONS VARIABLES
    |--------------------------------------------------------------------------
    */
    public function notificationVariables()
    {
        return [
            'userSalutation' => $this->user->salutation,
            'userFirstname'       => $this->user->first_name,
            'userEmail'      => $this->user->email,
            'age'            => $this->age(),
        ];
    }


    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function age()
    {
        if ($this->birthday) {
            return \Carbon\Carbon::createFromFormat('d-m-Y', $this->birthday)->age;
        }
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function addresses()
    {
        return $this->hasMany('App\Models\Address');
    }

    public function defaultShippingAddress()
    {
        return $this->belongsTo('App\Models\Address', 'id', 'default_shipping_address_id');
    }

    public function defaultBillingAddress()
    {
        return $this->belongsTo('App\Models\Address', 'id', 'default_billing_address_id');
    }

    public function companies()
    {
        return $this->hasMany('App\Models\Company');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function cartRules()
    {
        return $this->belongsToMany('App\Models\CartRule');
    }
    public function getFullnameAttribute() 
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function getBirthdayAttribute($value)
    {
        if ($value) {
            return \Carbon\Carbon::createFromFormat('Y-m-d', $value)->format('d-m-Y');
        }
    }

    public function isAdmin()
    {
        if ($this->roles->where('name', 'Administrator')->first())
            return true;
        else
            return false;
    }

}
