<?php

namespace App\Observers;

use App\Models\Product;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class ProductObserver
{
    private $oms_client_token;

    public function __construct()
    {
        // The HTTP client for the OMS API.
        $this->oms_client = new Client([
            // Base URI is used with relative requests
            'base_uri' => env('OMS_BASE_URI'),
            // You can set any number of default request options.
            // 'timeout'  => 2.0,
        ]);

        // Get token from env
        $this->oms_client_token = env('OMS_TOKEN', null);

        try {
            // Get token from API with env credentials
            if (!isset($oms_client_token)) {
                $response = $this->oms_client->post('oauth/token', [
                    'form_params' => [
                        'grant_type'    => 'client_credentials',
                        'client_id'     => env('OMS_CLIENT_ID'),
                        'client_secret' => env('OMS_CLIENT_SECRET'),
                        'scope'         => null,
                    ],
                ]);

                $this->oms_client_token = json_decode($response->getBody())->access_token;
            }
        } catch (\Exception $e) {
            Log::alert('OMS Authentication failed.', ['message' => $e->getMessage()]);
        }
    }

    public function created(Product $product)
    {
        // Store the new product in the OMS app.
        try {
            $response = $this->oms_client->post('api/products/', [
                'form_params' => [
                	'metric_id'   => 1,
                	'category_id' => request()->categories[0],
                	'name'        => $product->name,
                	'sku_code'    => $product->sku,
                	'description' => $product->description,
                ],
                'headers' => [
                	'Accept' => 'application/json',
                	'Authorization' => 'Bearer '.$this->oms_client_token,
                ],
            ]);
            Log::info('Stored product #' . $product->id . ' in OMS.');
        } catch (\Exception $e) {
            Log::alert('Failed to store product #' . $product->id . ' in OMS.', ['message' => $e->getMessage()]);
        }
    }
}
