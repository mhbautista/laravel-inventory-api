<?php

namespace App\Observers;

use App\Models\Category;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class CategoryObserver
{
    private $oms_client_token;

    public function __construct()
    {
        // The HTTP client for the OMS API.
        $this->oms_client = new Client([
            // Base URI is used with relative requests
            'base_uri' => env('OMS_BASE_URI'),
            // You can set any number of default request options.
            // 'timeout'  => 2.0,
        ]);

        // Get token from env
        $this->oms_client_token = env('OMS_TOKEN', null);

        try {
            // Get token from API with env credentials
            if (!isset($oms_client_token)) {
                $response = $this->oms_client->post('oauth/token', [
                    'form_params' => [
                        'grant_type'    => 'client_credentials',
                        'client_id'     => env('OMS_CLIENT_ID'),
                        'client_secret' => env('OMS_CLIENT_SECRET'),
                        'scope'         => null,
                    ],
                ]);

                $this->oms_client_token = json_decode($response->getBody())->access_token;
            }
        } catch (\Exception $e) {
            Log::alert('OMS Authentication failed.', ['message' => $e->getMessage()]);
        }
    }

    public function created(Category $category)
    {
        // Store the new category in the OMS app.
        try {
            $response = $this->oms_client->post('api/categories/', [
                'form_params' => $category->getAttributes(),
                'headers' => [
                	'Accept' => 'application/json',
                	'Authorization' => 'Bearer '.$this->oms_client_token,
                ],
            ]);
            Log::info('Stored category #' . $category->id . ' in OMS.');
        } catch (\Exception $e) {
            Log::alert('Failed to store category #' . $category->id . ' in OMS.', ['message' => $e->getMessage()]);
        }
    }
}
