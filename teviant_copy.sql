-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 01, 2018 at 09:14 AM
-- Server version: 5.6.35
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `teviant_prod_copy`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `county` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `unit` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `house` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `building` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barangay` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `town` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` enum('ncr','luzon','visayas','mindanao','island area') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `user_id`, `country_id`, `name`, `address1`, `address2`, `county`, `city`, `postal_code`, `phone`, `mobile_phone`, `comment`, `created_at`, `updated_at`, `unit`, `house`, `building`, `street`, `barangay`, `town`, `state`, `email`, `region`) VALUES
(1, 1, 1, 'Thaniel Jay Duldulao', '#70 Dr. Jose Fabella St. Brgy. Plainview, Mandalayung City, Philippinnes.', '', 'Metro Manila', 'Mandalayung', '1550', '', '+639065045197', '', '2018-11-28 18:16:23', NULL, '#70', NULL, NULL, 'Dr. Jose Fabella', 'Plainview', NULL, NULL, NULL, 'ncr'),
(2, NULL, 27, 'Thaniel Jay Duldulao', NULL, NULL, 'Metro Manila', 'City', '11115', NULL, '09476442400', NULL, '2018-11-28 20:31:13', '2018-11-28 20:31:13', '100', NULL, NULL, 'Fabella', 'Fabella', NULL, NULL, 'tjay.developer@gmail.com', 'ncr'),
(3, NULL, 27, 'Don Christopher Cadavona', NULL, NULL, 'National Capital Region', 'Quezon City', '1116', '123123', '09778375746', NULL, '2018-11-28 20:44:54', '2018-11-28 20:44:54', '10', NULL, '10', 'Botocan', 'Tandang Sora', NULL, NULL, 'dcadavona@gmail.com', 'visayas'),
(4, 10, 27, NULL, NULL, NULL, 'Metro Manila', 'Pasig City', '1605', NULL, NULL, NULL, '2018-11-28 14:38:33', '2018-11-28 14:38:33', '18', NULL, NULL, 'Gen. Delgado c/o Manlapaz Store, San Antonio Village', 'San Antonio', NULL, NULL, NULL, 'ncr'),
(5, 10, 27, NULL, NULL, NULL, 'Metro Manila', 'Pasig City', '1605', NULL, NULL, NULL, '2018-11-28 14:38:33', '2018-11-28 14:38:33', '18', NULL, NULL, 'Gen. Delgado c/o Manlapaz Store, San Antonio Village', 'San Antonio', NULL, NULL, NULL, 'ncr'),
(6, 9, 27, NULL, NULL, NULL, NULL, 'Quezon City', '1109', NULL, NULL, NULL, '2018-11-28 14:39:52', '2018-11-28 14:39:52', '629, Tower 1', NULL, 'Sun Residences', 'P.Florentino St.', 'Brgy. Sta. Teresita', NULL, NULL, NULL, 'ncr'),
(7, 9, 27, NULL, NULL, NULL, NULL, 'Quezon City', '1109', NULL, NULL, NULL, '2018-11-28 14:39:52', '2018-11-28 14:39:52', '629, Tower 1', NULL, 'Sun Residences', 'P. Florentino St.', 'Brgy. Sta. Teresita', NULL, NULL, NULL, 'ncr'),
(8, 13, 27, NULL, NULL, NULL, NULL, 'Quezon City', '1100', NULL, NULL, NULL, '2018-11-28 14:56:26', '2018-11-28 14:56:26', '47', NULL, NULL, 'Muralla Street, New Intramuros Village', 'Barangay Old Balara', 'Metro Manila', NULL, NULL, 'ncr'),
(9, 13, 27, NULL, NULL, NULL, NULL, 'Quezon City', '1100', NULL, NULL, NULL, '2018-11-28 14:56:26', '2018-11-28 14:56:26', '47', NULL, NULL, 'Muralla Street, New Intramuros Village', 'Barangay Old Balara', 'Metro Manila', NULL, NULL, 'ncr'),
(10, 14, 27, NULL, NULL, NULL, 'Metro Manila', 'Quezon City', '9810', NULL, NULL, NULL, '2018-11-28 14:57:49', '2018-11-28 14:57:49', 'Bldg 1 Unit 9', NULL, 'Magnitude Place', 'No. 186 E. Rodriguez Jr. Ave', 'Bagumbayan', NULL, NULL, NULL, 'ncr'),
(11, 14, 27, NULL, NULL, NULL, 'Metro Manila', 'Quezon City', '9810', NULL, NULL, NULL, '2018-11-28 14:57:49', '2018-11-28 14:57:49', 'Unit 9 Bldg. 1', NULL, 'Magnitude Place', 'No. 186 E. Rodriguez Jr. Avenue', 'Bagumbayan', NULL, NULL, NULL, 'ncr'),
(12, NULL, 27, 'Michiko Godiaco', NULL, NULL, 'Metro Manila', 'San Juan', '1500', NULL, '09178900838', NULL, '2018-11-28 15:05:07', '2018-11-28 15:05:07', '349', NULL, NULL, 'CM Recto St, San Juan', 'Addition Hills', NULL, NULL, 'michiko_kengi@yahoo.com', 'ncr'),
(13, 17, 27, NULL, NULL, NULL, 'Davao del Norte', 'Panabo', '8105', NULL, NULL, NULL, '2018-11-28 15:18:17', '2018-11-28 15:18:17', '997', NULL, NULL, 'Manuel roxas street', 'Gredu', NULL, NULL, NULL, 'ncr'),
(14, 17, 27, NULL, NULL, NULL, 'Davao del Norte', 'Panabo', '8105', NULL, NULL, NULL, '2018-11-28 15:18:17', '2018-11-28 15:18:17', '997', NULL, NULL, 'Manuel roxas street', 'gredu', NULL, NULL, NULL, 'ncr'),
(15, 20, 27, 'Gie Mapua', NULL, NULL, 'Metro Manila', 'Quezon City', '1100', NULL, '09178840815', NULL, '2018-11-28 15:42:36', '2018-11-28 15:42:36', '8', NULL, NULL, 'Sparrow St Greenmeadows Subdivision', 'Ugong Norte', NULL, NULL, 'angeline.belle@gmail.com', 'ncr'),
(16, 23, 27, 'Pauline Lim', NULL, NULL, 'Metro Manila', 'Quezon City', '1110', NULL, NULL, NULL, '2018-11-28 17:00:31', '2018-11-28 17:00:31', '9 Bautista Street', NULL, 'Corinthian Gardens', 'Bautista Street', 'Ugong Norte', NULL, NULL, 'pauline_mae_lim@yahoo.com', 'luzon'),
(17, NULL, 27, 'CLAUDINE BACCAY', NULL, NULL, 'PASAY CITY', 'PASAY CITY', '1630', '09273819878', '09273819878', NULL, '2018-11-28 17:46:05', '2018-11-28 17:46:05', 'Blk 6A LOT 27 KALAYAAN VILLAGE', NULL, 'BLK 6A LOT 27 KALAYAAN VILLAGE', 'GATE 1 KALAYAAN VILLAGE', 'BRGY 201', NULL, NULL, 'dindin.claudine@gmail.com', 'ncr'),
(18, 39, 27, 'Valerie ann Imperial', NULL, NULL, 'Metro Manila', 'Muntinlupa city', '1770', NULL, NULL, NULL, '2018-11-28 21:23:46', '2018-11-28 21:23:46', '326A', NULL, 'Centropolis Communities', 'Villongco st.', 'Brgy sucat', NULL, NULL, 'leonne0312@gmail.com', 'ncr'),
(19, 16, 27, 'elizabeth ang', NULL, NULL, 'metro manila', 'quezon city', '1112', NULL, '639228268331', NULL, '2018-11-28 23:41:03', '2018-11-30 07:38:59', NULL, NULL, NULL, '53 9th st new manila rolling hills', 'damayang lagi', NULL, NULL, 'lizamunoz_ang@yahoo.com', 'ncr'),
(20, 16, 27, 'elizabeth ang', NULL, NULL, 'metro manila', 'quezon city', '1112', NULL, '639228268331', NULL, '2018-11-28 23:41:03', '2018-11-30 07:38:59', NULL, NULL, NULL, '53 9th st new manila rolling hills', 'damayang lagi', NULL, NULL, 'lizamunoz_ang@yahoo.com', 'ncr'),
(21, NULL, 27, 'ZYRA EVE LEE', NULL, NULL, 'METRO MANILA', 'MAKATI', '1229', NULL, '09178557725', NULL, '2018-11-29 02:17:46', '2018-11-29 02:17:46', 'UNIT 20H TOWER 1', NULL, 'PASEO PARKVIEW SUITES', 'VALERO COR. SEDENO', 'BEL-AIR', NULL, NULL, 'eve_032002@yahoo.com', 'ncr'),
(22, NULL, 27, 'Kristine Takemura', NULL, NULL, 'NCR', 'Muntinlupa', '1700', NULL, '+818040624896', NULL, '2018-11-29 04:17:15', '2018-11-29 04:17:15', 'Block 7 Lot 18', NULL, NULL, 'Jose Abad Santos St.  Katarungan Village', 'Poblacion', NULL, NULL, 'khim', 'ncr'),
(23, 21, 27, 'Milagros Flores', NULL, NULL, 'Metro Manila', 'Taguig City', '1634', NULL, '+639178220116', NULL, '2018-11-29 04:49:01', '2018-11-29 04:49:01', '21G Tower Domenico', NULL, 'The Venice Luxury Residences', '8 Venezia Drive, McKinley Hill', 'Pinagsama', NULL, NULL, 'lalaflores2003@yahoo.com', 'ncr'),
(24, 21, 27, 'Milagros Flores', NULL, NULL, 'Metro Manila', 'Taguig City', '1634', NULL, '+639178220116', NULL, '2018-11-29 04:49:01', '2018-11-29 04:49:01', '21G TOWER DOMENICO', NULL, 'The Venice Luxury Residences', '8 Venezia Drive, McKinley Hill', 'Pinagsama', NULL, NULL, 'lalaflores2003@yahoo.com', 'ncr'),
(25, NULL, 27, 'Martin Bautista', NULL, NULL, 'Manila', 'Mandaluyong City', '1552', '+639175665662', '+639175665662', NULL, '2018-11-29 05:40:25', '2018-11-29 05:40:25', 'Unit 803', NULL, 'Princeville condominium', 'Laurel St', 'Pleasant Hills', NULL, NULL, 'martinmbautista@gmail.com', 'ncr'),
(26, 64, 27, NULL, NULL, NULL, NULL, 'Quezon City', '1105', NULL, NULL, NULL, '2018-11-29 06:20:36', '2018-11-29 06:20:36', '42', NULL, NULL, 'Osmeña Street', 'San Francisco Del Monte', NULL, NULL, NULL, 'ncr'),
(27, 64, 27, NULL, NULL, NULL, NULL, 'Quezon City', '1105', NULL, NULL, NULL, '2018-11-29 06:20:36', '2018-11-29 06:20:36', '42', NULL, NULL, 'Osmeña Street', 'San Francisco Del Monte', NULL, NULL, NULL, 'ncr'),
(28, 63, 27, NULL, NULL, NULL, 'Pampanga', 'Pampanga', '2001', NULL, NULL, NULL, '2018-11-29 06:21:21', '2018-11-29 06:21:21', NULL, NULL, NULL, 'Zone 3', 'Macabacle', 'Bacolor', NULL, NULL, 'ncr'),
(29, 63, 27, NULL, NULL, NULL, 'Pampanga', 'Pampanga', '2001', NULL, NULL, NULL, '2018-11-29 06:21:21', '2018-11-29 06:21:21', NULL, NULL, NULL, 'Zone 3', 'Macabacale', 'Bacolor', NULL, NULL, 'ncr'),
(30, 66, 27, NULL, NULL, NULL, 'Cebu', 'Cebu city', '6046', NULL, NULL, NULL, '2018-11-29 06:38:39', '2018-11-29 06:38:39', NULL, NULL, NULL, 'Vito road', 'Poblacion ward 4 minglanilla cebu', 'Minglanilla', NULL, NULL, 'ncr'),
(31, 66, 27, NULL, NULL, NULL, 'Cebu', 'Cebu city', '6046', NULL, NULL, NULL, '2018-11-29 06:38:39', '2018-11-29 06:38:39', NULL, NULL, NULL, 'Vito road', 'Poblacion ward 4 minglanilla cebu', 'Minglanilla', NULL, NULL, 'ncr'),
(32, 70, 27, 'Teresita Lopez', NULL, NULL, 'Metro Manila', 'Makati', '1229', NULL, NULL, NULL, '2018-11-29 07:11:30', '2018-11-29 07:11:30', '6th Floor', NULL, 'Philippine Veterans Bank', '101 V.A. Rufino cor. dela Rosa Sts., Legaspi Vill.', 'San Lorenzo', NULL, NULL, 'tes3klopez@yahoo.com', 'ncr'),
(33, 71, 27, 'Beth Villamil', NULL, NULL, 'Luzon', 'Makati City', '1225', NULL, NULL, NULL, '2018-11-29 07:45:12', '2018-11-29 07:45:12', '6/F', NULL, 'Zuellig Building', 'Paseo de Roxas corner Makati Avenue', 'Bel Air', NULL, NULL, 'socialinbox@yahoo.com', 'ncr'),
(34, 72, 27, NULL, NULL, NULL, 'Cavite', 'Cavite City', '4100', NULL, NULL, NULL, '2018-11-29 08:17:37', '2018-11-29 08:17:37', '520', NULL, 'Lopez Jaena St.,', 'Caridad', 'Barangay 30 Bid-bid', 'Caridad', NULL, NULL, 'ncr'),
(35, 72, 27, NULL, NULL, NULL, 'Cavite', 'Cavite City', '4100', NULL, NULL, NULL, '2018-11-29 08:17:37', '2018-11-29 08:17:37', '520', NULL, 'Lopez', 'Jaena St.,', 'Barangay 30 Bid-bid', 'Caridad', NULL, NULL, 'ncr'),
(36, NULL, 27, 'Anjie Gogna', NULL, NULL, 'Manila', 'Pasig', '1600', NULL, '09178317288', NULL, '2018-11-29 09:08:49', '2018-11-29 09:08:49', 'C134', NULL, 'Alexandra Condominium', 'Meralco avenue', 'San Antonio', NULL, NULL, 'anjiegogna@gmail.com', 'ncr'),
(37, NULL, 27, 'Andrew Bautista', NULL, NULL, 'Pangasinan', 'Binmaley', '2417', NULL, '00639154217377', NULL, '2018-11-29 09:16:30', '2018-11-29 09:16:30', '265', NULL, NULL, 'Baybay Polong', 'Baybay Polong', NULL, NULL, 'abbieaab@gmail.com', 'luzon'),
(38, 74, 27, NULL, NULL, NULL, 'Metro Manila', 'Marikina', '1807', NULL, NULL, NULL, '2018-11-29 09:22:39', '2018-11-29 09:22:39', '5', NULL, 'Marikit Homes Subd.', 'Ruby', 'Concepcion Uno', 'N/A', NULL, NULL, 'ncr'),
(39, 74, 27, NULL, NULL, NULL, 'Metro Manila', 'Marikina', '1807', NULL, NULL, NULL, '2018-11-29 09:22:39', '2018-11-29 09:22:39', '5', NULL, 'Marikit Homes Subd.', 'Ruby St', 'Concepcion Uno', 'N/a', NULL, NULL, 'ncr'),
(40, 29, 27, 'Jennifer Garcia', NULL, NULL, 'Metro Manila', 'Mandaluyong City', '1550', NULL, NULL, NULL, '2018-11-29 10:24:08', '2018-11-29 10:24:08', '57-D South Tower', NULL, 'One Shangri-La Place', 'Internal Road Shangri-La Place', 'Brgy. Wack-Wack', NULL, NULL, 'jennyvgarcia530@gmail.com', 'ncr'),
(41, 76, 27, NULL, NULL, NULL, 'NCR- 4th District', 'Makati City', '1235', NULL, NULL, NULL, '2018-11-29 10:25:54', '2018-11-29 10:26:41', '3568-F', NULL, NULL, 'Durango', 'Barangay Palanan', NULL, NULL, NULL, 'ncr'),
(42, 76, 27, NULL, NULL, NULL, 'NCR- 4th District', 'Makati City', '1235', NULL, NULL, NULL, '2018-11-29 10:25:54', '2018-11-29 10:29:08', '3568-F', NULL, NULL, 'Durango', 'Barangay Palanan', NULL, NULL, NULL, 'ncr'),
(43, 61, 27, 'Frances Reyes', NULL, NULL, 'Manila', 'Pasig', '1600', NULL, NULL, NULL, '2018-11-29 10:35:28', '2018-11-29 10:35:28', '#43', NULL, NULL, 'Baywood St. Greenwoods, Phase 6', 'Pinagbuhatan', NULL, NULL, 'mylittlechini@gmail.com', 'ncr'),
(44, 81, 27, NULL, NULL, NULL, 'Leyte', NULL, '6511', NULL, NULL, NULL, '2018-11-29 11:48:29', '2018-11-29 11:48:29', 'NA', NULL, NULL, 'Supiengco Street', 'Poblacion Zone 2', 'Javier', NULL, NULL, 'ncr'),
(45, 81, 27, NULL, NULL, NULL, 'Leyte', NULL, '6511', NULL, NULL, NULL, '2018-11-29 11:48:29', '2018-11-29 11:48:29', 'NA', NULL, NULL, 'Supiengco Street', 'Poblacion Zone 2', 'Javier', NULL, NULL, 'ncr'),
(46, NULL, 27, 'Samantha Lim', NULL, NULL, 'Zamboanga del Norte', 'Dipolog', '7100', '065 2125785', '09177242898', NULL, '2018-11-29 13:49:04', '2018-11-29 13:49:04', 'Ag and sons', NULL, 'Better living appliance center', 'Echavez st', 'Central Barangay', NULL, NULL, 'Samanthavlim@yahoo.com', 'mindanao'),
(47, 92, 27, 'mabi cayetano', NULL, NULL, 'metro manila', 'taguig', '1630', NULL, '09178676886', NULL, '2018-11-29 16:57:38', '2018-11-29 16:57:38', 'unit 801', NULL, 'one maridien', '9th ave. cor 26th st.', 'fort', NULL, NULL, 'mabicalalang@gmail.com', 'ncr'),
(48, NULL, 27, 'Athina mae Cayaba', NULL, NULL, 'Pasig city', 'Pasig city', '1800', NULL, '09289569655', NULL, '2018-11-29 19:23:10', '2018-11-29 19:23:10', 'Unit 308', NULL, 'Bldg 1', 'Kaimitoville', 'Ugong', NULL, NULL, 'Yapnorielbon@yahoo.com', 'ncr'),
(49, 93, 27, NULL, NULL, NULL, 'Bataan', 'Balanga City', '2100', NULL, NULL, NULL, '2018-11-29 19:53:01', '2018-11-29 19:53:01', 'Unit 3', NULL, 'Almontero Apartment', 'Masikap Street', 'Cupang West', NULL, NULL, NULL, 'ncr'),
(50, 93, 27, NULL, NULL, NULL, 'Bataan', 'Balanga City', '2100', NULL, NULL, NULL, '2018-11-29 19:53:01', '2018-11-29 19:53:01', 'Unit 3', NULL, 'Almontero Apartment', 'Masikap Street', 'Cupang West', NULL, NULL, NULL, 'ncr'),
(51, 60, 27, 'Juvi Katrina Ramos', NULL, NULL, 'Makati City', 'Legaspi Village Makati City', '1229', NULL, '09988828757', NULL, '2018-11-30 01:23:07', '2018-11-30 01:23:07', '6th Flr', NULL, 'Exchange Corner Building', 'cor Esteban and Bolanos Sts.', 'San Lorenzo', NULL, NULL, 'juviramos@yahoo.com', 'ncr'),
(52, NULL, 27, 'Dorothy Ong', NULL, NULL, 'Metro Manila', 'Quezon City', '1103', '025141288', '+639178357585', NULL, '2018-11-30 06:58:20', '2018-11-30 06:58:20', '95 unit 2', NULL, NULL, '95 Mother ignacia ave.', 'South Triangle', NULL, NULL, 'dcong57@gmail.com', 'ncr'),
(53, NULL, 27, 'Dorothy Ong', NULL, NULL, 'Metro Manila', 'QUEZON city', '1103', '025141288', '+639178357585', NULL, '2018-11-30 06:58:20', '2018-11-30 06:58:20', 'Unit 2.', NULL, NULL, '95 Mother ignacia ave.', 'South Triangle', NULL, NULL, 'dcong57@gmail.com', 'ncr'),
(54, 104, 27, 'macy vida', NULL, NULL, 'metro manila', 'quezon city', '1110', NULL, '09178100570', NULL, '2018-11-30 08:25:00', '2018-11-30 08:25:00', 'none', NULL, 'none', '34 odysseus st acropolis greens', 'bagumbayan', NULL, NULL, 'macyvidamd@gmail.com', 'ncr'),
(55, NULL, 27, 'Charlene Harn', NULL, NULL, 'Metro Manila', 'Mandaluyong', '1550', NULL, '09178177181', NULL, '2018-11-30 10:22:25', '2018-11-30 10:22:25', '7F', NULL, 'Columbia Tower', 'Ortigas Avenue', 'Brgy. Wack-Wack', NULL, NULL, 'charlene_harn@yahoo.com', 'ncr'),
(56, 33, 27, 'Tessa Gaerlan c/o Maria Nydia Danas', NULL, NULL, 'Malate', 'Metro Manila', '1004', '639237712363', '61430131354', NULL, '2018-11-30 13:53:06', '2018-11-30 13:53:06', '1920', NULL, 'EGI Taft Tower', 'Taft Avenue', '708', NULL, NULL, 'tf_gaerlan@hotmail.com', 'ncr'),
(57, 33, 27, 'Tessa Gaerlan c/o Maria Nydia Danas', NULL, NULL, 'Malate', 'Metro Manila', '1004', '639237712363', '61430131354', NULL, '2018-11-30 13:53:06', '2018-11-30 13:53:06', '1920', NULL, 'EGI Taft Tower', 'Taft Avenue', '708', NULL, NULL, 'tf_gaerlan@hotmail.com', 'ncr'),
(58, NULL, 27, 'Jhezarie Javier', NULL, NULL, 'Cebu', 'Cebu', '6000', NULL, '639162783888', NULL, '2018-11-30 14:57:28', '2018-11-30 14:57:28', '68 2nd street west', NULL, NULL, 'Beverly Hills', 'Lahug', NULL, NULL, 'Akim789789@yahoo.com.ph', 'visayas'),
(59, 113, 27, NULL, NULL, NULL, 'Rizal', 'Antipolo', '1870', NULL, NULL, NULL, '2018-11-30 18:08:30', '2018-11-30 18:08:30', '8', NULL, NULL, 'Nevada St Golden Meadows Subd.', 'Mayamot', NULL, NULL, NULL, 'ncr'),
(60, 113, 27, NULL, NULL, NULL, 'Rizal', 'Antipolo', '1870', NULL, NULL, NULL, '2018-11-30 18:08:30', '2018-11-30 18:08:30', '8', NULL, NULL, 'Nevada St. Golden Meadows Subd.', 'Mayamot', NULL, NULL, NULL, 'ncr'),
(61, NULL, 27, 'Maria Rica Aplasca', NULL, NULL, 'Metro Manila', 'San Juan', '1500', NULL, '09178851987', NULL, '2018-11-30 18:16:59', '2018-11-30 18:16:59', 'Unit A', NULL, 'Brownstone Homes', '307 M.A. Reyes', 'Little Baguio', NULL, NULL, 'cookieaplasca@gmail.com', 'ncr'),
(62, NULL, 27, 'Maria Rica Aplasca', NULL, NULL, 'Metro Manila', 'Marikina City', '1801', NULL, '09178851987', NULL, '2018-11-30 18:16:59', '2018-11-30 18:16:59', '34', NULL, NULL, 'Tanguile Street Monte Vista Subdivision', 'Industrial Valley', NULL, NULL, 'cookieaplasca@gmail.com', 'ncr'),
(63, 116, 27, NULL, NULL, NULL, 'Manila', 'Quezon City', '1106', NULL, NULL, NULL, '2018-12-01 03:45:00', '2018-12-01 03:51:59', '32', NULL, 'A', 'Aberdeen street', 'Bahay Toro', 'Project 8', NULL, NULL, 'ncr'),
(64, 116, 27, NULL, NULL, NULL, 'Manila', 'Quezon', '1106', NULL, NULL, NULL, '2018-12-01 03:45:00', '2018-12-01 03:45:00', '32', NULL, 'A', 'Aberdeen', 'Bahay Toro', 'Project 8', NULL, NULL, 'ncr'),
(65, 117, 27, NULL, NULL, NULL, 'NCR', 'Quezon city', '1102', NULL, NULL, NULL, '2018-12-01 05:28:48', '2018-12-01 05:28:48', '71', NULL, NULL, 'Chico Street', 'Barangay Quirino 2A', 'Project 2', NULL, NULL, 'ncr'),
(66, 117, 27, NULL, NULL, NULL, 'NCR', 'Quezon City', '1102', NULL, NULL, NULL, '2018-12-01 05:28:48', '2018-12-01 05:28:48', '71', NULL, NULL, 'Chico Street', 'Barangay Quirino 2A', 'Project 2', NULL, NULL, 'ncr');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `type`, `name`) VALUES
(1, 'dropdown', 'Color');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_attribute_set`
--

CREATE TABLE `attribute_attribute_set` (
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `attribute_set_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `attribute_attribute_set`
--

INSERT INTO `attribute_attribute_set` (`attribute_id`, `attribute_set_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_product_value`
--

CREATE TABLE `attribute_product_value` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `attribute_product_value`
--

INSERT INTO `attribute_product_value` (`product_id`, `attribute_id`, `value`) VALUES
(1, 1, 'Black'),
(2, 1, 'Black'),
(3, 1, 'Black'),
(4, 1, 'Black'),
(10, 1, 'Black'),
(12, 1, 'Black'),
(11, 1, 'Black'),
(9, 1, 'Black'),
(22, 1, 'Black'),
(13, 1, 'Black'),
(14, 1, 'Black'),
(15, 1, 'Black'),
(16, 1, 'Black'),
(17, 1, 'Black'),
(18, 1, 'Black'),
(5, 1, 'Black'),
(6, 1, 'Black'),
(7, 1, 'Black'),
(8, 1, 'Black'),
(29, 1, 'Black'),
(28, 1, 'Black'),
(30, 1, 'Black'),
(31, 1, 'Black'),
(32, 1, 'Black'),
(19, 1, 'Black'),
(20, 1, 'Black'),
(21, 1, 'Black'),
(23, 1, 'Black'),
(24, 1, 'Black'),
(25, 1, 'Black'),
(26, 1, 'Black'),
(27, 1, 'Black');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_sets`
--

CREATE TABLE `attribute_sets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `attribute_sets`
--

INSERT INTO `attribute_sets` (`id`, `name`) VALUES
(1, 'Color');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_values`
--

CREATE TABLE `attribute_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `value` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `attribute_values`
--

INSERT INTO `attribute_values` (`id`, `attribute_id`, `value`) VALUES
(1, 1, 'Black'),
(2, 1, 'White'),
(3, 1, 'Blue'),
(4, 1, 'Red');

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `carriers`
--

CREATE TABLE `carriers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(13,2) NOT NULL,
  `delivery_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `carriers`
--

INSERT INTO `carriers` (`id`, `name`, `price`, `delivery_text`, `logo`) VALUES
(1, 'LBC', '150.00', 'LBC Express, Inc. is a courier company based in the Philippines.', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart_rules`
--

CREATE TABLE `cart_rules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `priority` tinyint(4) NOT NULL,
  `start_date` datetime NOT NULL,
  `expiration_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `highlight` tinyint(1) NOT NULL DEFAULT '0',
  `minimum_amount` int(11) DEFAULT '0',
  `free_delivery` tinyint(1) NOT NULL DEFAULT '0',
  `total_available` int(11) DEFAULT NULL,
  `total_available_each_user` int(11) DEFAULT NULL,
  `promo_label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `promo_text` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `multiply_gift` int(11) DEFAULT '1',
  `min_nr_products` int(11) DEFAULT '0',
  `discount_type` enum('Percent - order','Percent - selected products','Amount - selected products','Amount - order') COLLATE utf8_unicode_ci NOT NULL,
  `reduction_amount` decimal(13,2) DEFAULT '0.00',
  `reduction_currency_id` int(10) UNSIGNED DEFAULT NULL,
  `minimum_amount_currency_id` int(10) UNSIGNED DEFAULT NULL,
  `gift_product_id` int(10) UNSIGNED DEFAULT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cart_rules`
--

INSERT INTO `cart_rules` (`id`, `name`, `code`, `priority`, `start_date`, `expiration_date`, `status`, `highlight`, `minimum_amount`, `free_delivery`, `total_available`, `total_available_each_user`, `promo_label`, `promo_text`, `multiply_gift`, `min_nr_products`, `discount_type`, `reduction_amount`, `reduction_currency_id`, `minimum_amount_currency_id`, `gift_product_id`, `customer_id`, `created_at`, `updated_at`) VALUES
(1, 'TLM10', 'TLM10', 1, '2018-11-28 18:00:21', '2018-12-08 19:16:21', 1, 0, 2500, 0, NULL, NULL, NULL, 'This promo code TLM10 has been applied to your shopping bag.', 0, NULL, 'Percent - order', '10.00', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'ABT10', 'ABT10', 1, '2018-11-28 18:00:38', '2018-12-08 23:55:00', 1, 0, 2500, 0, NULL, NULL, NULL, 'This promo code ABT10 has been applied to your shopping bag.', 0, NULL, 'Percent - order', '10.00', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart_rules_categories`
--

CREATE TABLE `cart_rules_categories` (
  `cart_rule_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart_rules_combinations`
--

CREATE TABLE `cart_rules_combinations` (
  `cart_rule_id_1` int(10) UNSIGNED NOT NULL,
  `cart_rule_id_2` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart_rules_customers`
--

CREATE TABLE `cart_rules_customers` (
  `cart_rule_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart_rules_products`
--

CREATE TABLE `cart_rules_products` (
  `cart_rule_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart_rules_product_groups`
--

CREATE TABLE `cart_rules_product_groups` (
  `cart_rule_id` int(10) UNSIGNED NOT NULL,
  `product_group_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `depth` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `slug`, `lft`, `rgt`, `depth`) VALUES
(1, NULL, 'Categories', 'categories', 2, 29, 1),
(2, NULL, 'Groups', 'groups', 30, 43, 1),
(3, 1, 'Eyes', 'eyes', 3, 12, 2),
(6, 2, 'Artist\'s Pick', 'artist-s-pick', 31, 32, 2),
(7, 2, 'Best Featured', 'best-featured', 33, 34, 2),
(8, 2, 'Best Sellers', 'best-sellers', 35, 36, 2),
(9, 2, 'Featured', 'featured', 37, 38, 2),
(10, 2, 'New', 'new', 39, 40, 2),
(11, 2, 'Collections', 'collections', 41, 42, 2),
(12, 1, 'Accessories', 'accessories', 21, 28, 2),
(13, 15, 'Eyebrow Tint', 'eyebrow-tint', 16, 17, 3),
(14, 15, 'Eyebrow Gel', 'eyebrow-gel', 18, 19, 3),
(15, 1, 'Eyebrows', 'eyebrows', 13, 20, 2),
(16, 3, 'Pen Eyeliner', 'pen-eyeliner', 6, 7, 3),
(17, 3, 'Mascara', 'mascara', 10, 11, 3),
(18, 15, 'Eyebrow Duo', 'eyebrow-duo', 14, 15, 3),
(19, 12, 'Upper Lashes', 'upper-lashes', 22, 23, 3),
(20, 12, 'Lower Lashes', 'lower-lashes', 24, 25, 3),
(21, 12, 'Tools', 'tools', 26, 27, 3),
(22, 3, 'Eye Shadow Palette', 'eye-shadow-palette', 4, 5, 3),
(23, 3, 'Glitter Liner', 'glitter-liner', 8, 9, 3);

-- --------------------------------------------------------

--
-- Table structure for table `category_product`
--

CREATE TABLE `category_product` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category_product`
--

INSERT INTO `category_product` (`category_id`, `product_id`) VALUES
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(9, 5),
(3, 6),
(9, 6),
(3, 7),
(9, 7),
(3, 8),
(9, 8),
(3, 9),
(9, 9),
(3, 10),
(9, 10),
(3, 11),
(9, 11),
(3, 12),
(9, 12),
(3, 13),
(3, 14),
(3, 15),
(3, 16),
(3, 17),
(3, 18),
(3, 19),
(3, 20),
(3, 21),
(3, 22),
(9, 24),
(9, 26),
(9, 27),
(9, 28),
(9, 30),
(9, 31),
(6, 22),
(6, 19),
(6, 20),
(6, 21),
(6, 16),
(6, 17),
(6, 18),
(6, 1),
(6, 2),
(6, 3),
(6, 4),
(6, 23),
(6, 29),
(9, 3),
(9, 2),
(12, 23),
(12, 24),
(12, 25),
(12, 26),
(12, 27),
(12, 28),
(12, 29),
(12, 30),
(12, 31),
(14, 5),
(14, 6),
(14, 7),
(14, 8),
(13, 16),
(13, 17),
(12, 32),
(22, 1),
(22, 2),
(22, 3),
(22, 4),
(23, 10),
(23, 12),
(23, 11),
(23, 9),
(17, 22),
(18, 13),
(18, 14),
(18, 15),
(15, 13),
(15, 14),
(15, 15),
(15, 16),
(15, 17),
(13, 18),
(15, 18),
(15, 5),
(15, 6),
(15, 7),
(15, 8),
(20, 29),
(20, 28),
(21, 30),
(21, 31),
(21, 32),
(16, 19),
(16, 20),
(16, 21),
(19, 23),
(19, 24),
(19, 25),
(19, 26),
(19, 27);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `county` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tin` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Tax Identification Number',
  `trn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Trade Registry Number'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`, `currency_id`) VALUES
(1, 'US', 'United States', 2),
(2, 'AD', 'Andorra', 3),
(3, 'AT', 'Austria', 3),
(4, 'BE', 'Belgium', 3),
(5, 'CY', 'Cyprus', 3),
(6, 'EE', 'Estonia', 3),
(7, 'FI', 'Finland', 3),
(8, 'FR', 'France', 3),
(9, 'GF', 'French Guiana', 3),
(10, 'DE', 'Germany', 3),
(11, 'GR', 'Greece', 3),
(12, 'GP', 'Guadeloupe', 3),
(13, 'ID', 'Indonesia', 4),
(14, 'IE', 'Ireland', 3),
(15, 'IT', 'Italy', 3),
(16, 'XK', 'Kosovo', 3),
(17, 'LV', 'Latvia', 3),
(18, 'LT', 'Lithuania', 3),
(19, 'LU', 'Luxembourg', 3),
(20, 'MT', 'Malta', 3),
(21, 'MQ', 'Martinique', 3),
(22, 'TY', 'Mayotte', 3),
(23, 'MC', 'Monaco', 3),
(24, 'ME', 'Montenegro', 3),
(25, 'NL', 'Netherlands', 3),
(26, 'PT', 'Portugal', 3),
(27, 'PH', 'Philippines', 1),
(28, 'RE', 'Reunion', 3),
(29, 'BL', 'Saint Barthélemy', 3),
(30, 'SM', 'San Marino', 3),
(31, 'SK', 'Slovakia', 3),
(32, 'SI', 'Slovenia', 3),
(33, 'ES', 'Spain', 3),
(34, 'VA', 'Vatican City State', 3),
(35, 'ZW', 'Zimbabwe', 3);

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `symbol` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `default` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `code`, `symbol`, `default`) VALUES
(1, 'Philippine Peso', 'PHP', '&#8369;', 0),
(2, 'United States Dollar', 'USD', '&#36;', 0),
(3, 'Euro', 'EUR', '&#8364;', 0),
(4, 'Indonesian rupiah', 'IDR', '&#82;&#112;', 0);

-- --------------------------------------------------------

--
-- Table structure for table `inquiries`
--

CREATE TABLE `inquiries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `contact_number` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('product inquiry','returns','order status','my account') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inquiries`
--

INSERT INTO `inquiries` (`id`, `name`, `email`, `contact_number`, `message`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Francesca Melon', 'chesamelon@gmail.com', '09175150915', 'Hi \r\nHow to distinguish the shades for the eyeliners? Would like to order\r\nSame with the rest of the other products with different shades\r\nLooking forward to your reply', 'product inquiry', '2018-11-28 22:18:18', '2018-11-28 22:18:18'),
(2, 'Janice chua', 'janice_dianne@yahoo.com', '09178396196', 'Hi! Which eye shadow palette you suggest for asian chinese, for everyday use?\r\n Also which is the simplest, shortest lashes for everyday look? \r\nLastly, whats the diff of the 2 eyebrow strokes pen?\r\n\r\nThanks!!', 'product inquiry', '2018-11-28 22:22:13', '2018-11-28 22:22:13'),
(3, 'Michiko Godiaco', 'michiko_kengi@yahoo.com', '09178900838', 'Hi, would like to ask if it is still possible to get the 10% introductory discount with promo code ABT10, i only saw the post of Mr Albert K.  a few minutes before I purchased the items. Is there any way we can refund so I can repurchase with the promo code?\r\n\r\nHoping for your kind consideration. Thank you.\r\n\r\nMichiko Godiaco', 'product inquiry', '2018-11-28 15:40:13', '2018-11-28 15:40:13'),
(4, 'Tessa Gaerlan', 'tf_gaerlan@hotmail.com', '61430131354', 'Do you ship to Australia? I clicked on the shipping and returns section but it kept taking me to the home page. Thanks', 'product inquiry', '2018-11-28 19:11:31', '2018-11-28 19:11:31'),
(5, 'Keren Culalic', 'keren_culalic@yahoo.com', '09169061346', 'Hi there I was trying to make a payment and the discount ABT10 was applied. When entering the credit card the amount shows the original amount and not the discounted amount. Please help, Here\'s the calculation. \r\nSUBTOTAL\r\n₱ 2,770.00\r\n SHIPPING\r\n₱ 0.00\r\n DISCOUNT\r\n₱ 277.00\r\n ESTIMATED TOTAL\r\n₱ 2,770.00\r\n\r\nOrdering from Australia but delivery in Manila. \r\n\r\nThanks,\r\nKeren', 'product inquiry', '2018-11-28 21:15:52', '2018-11-28 21:15:52'),
(6, 'Anna Lim', 'angi_lim@yahoo.com.ph', '09173040164', 'Hello, for the products that say available in three shades, they don’t  come with a description of what shade. For example, She & Her. How do I know what shades are they? Brown or black...', 'product inquiry', '2018-11-28 22:28:21', '2018-11-28 22:28:21'),
(7, 'Tessa Gaerlan', 'tf_gaerlan@hotmail.com', '61430131354', 'Hi, the eyebrow tints and liquid liners doesn’t say what the shades are. Where can I find these information, i am confident the products will be awesome but I want to know if the shades will complement my skin colour. Thank you.', 'product inquiry', '2018-11-28 22:51:12', '2018-11-28 22:51:12'),
(8, 'EJ', 'e_miera@hotmail.com', 'e_miera@hotmail.com', 'Hi there,\r\n\r\nJust wondering if Teviant\'s product can post to Australia? If yes, how much you Teviant charge the delivery?\r\n\r\n\r\nRegards,\r\nEJ', 'product inquiry', '2018-11-29 01:37:04', '2018-11-29 01:37:04'),
(9, 'Kristine Takemura', 'khimeys@gmail.com', '+818040624896', 'Hello,\r\n\r\nI have tried ordering online. Using my debit card from Japan.  My debit card was charged but I didn\'t get any order confirmation if my payment did go through your system.\r\n\r\nBest regards,\r\nKristine', 'order status', '2018-11-29 04:23:02', '2018-11-29 04:23:02'),
(10, 'Shey Lopez', 'shell_larosa@hotmail.com', '12043911828', 'Hi,\r\nI would like to know if you ship orders to Canada. I am interested to buy the Amore eyeshadow palette and Herself 3D eyebrow make-up. \r\n\r\nThanks.', 'product inquiry', '2018-11-29 04:52:37', '2018-11-29 04:52:37'),
(11, 'Cheyenne De Vroe-Bagayaua', 'chey_devroe@yahoo.com', '+32472213515', 'Hi, Albert and Heart! I\'m interested in pre-order ING the Amore palette.\r\nDo you ship to Belgium?\r\nHope to hear from you soonest!', 'product inquiry', '2018-11-29 05:51:38', '2018-11-29 05:51:38'),
(12, 'Jean Agoncillo Khaledi', 'jeanagoncillokhaledi@yahoo.com', '09174966373', 'How do you know what color is the eyeliner, brow gels? It says it has 3 different shades, or 4 different shades? How do we know which is which? Thank you so much!', 'product inquiry', '2018-11-29 06:27:45', '2018-11-29 06:27:45'),
(13, 'Mitch Cochanco', 'mitch_cochanco@yahoo.com', 'O9173282250', 'shades for the eyeliner, 3d eyebrow, mascara e brow gel are not indicated in the site so I can\'t choose what to order \r\nThanks \r\nMitch', 'product inquiry', '2018-11-29 07:59:27', '2018-11-29 07:59:27'),
(14, 'Mary Grace', 'gracebausa@gmail.com', '09175067999', 'Good pm. Can I make bank deposit? Just want to ask which false eyelashes are your saleable?\r\nI\'m interested to buy naomi and Linda', 'order status', '2018-11-29 13:13:24', '2018-11-29 13:13:24'),
(15, 'Rolls-Royce', 'bbbracer@gmail.com', '09178224004', 'If it ships december 15th, what approximate day would I receive it in Manila?\r\n\r\nIm so excited for the eye shadow palletes but im afraid I\'d be leaving on the 20th to LA, hoping this would be my go to kit there.\r\n\r\nCheers!', 'product inquiry', '2018-11-29 14:18:06', '2018-11-29 14:18:06'),
(16, 'Mary Grace Bausa', 'gracebausa@gmail.com', '09175067999', 'Good pm. Can I make bank deposit for my orders? I wouldn\'t like to ask What\'s your saleable false eyelash?', 'product inquiry', '2018-11-29 16:15:47', '2018-11-29 16:15:47'),
(17, 'Mary Grace Bausa', 'gracebausa@gmail.com', '09175067999', 'Good pm. Can I make bank deposit for my orders? I wouldn\'t like to ask What\'s your saleable false eyelash?', 'product inquiry', '2018-11-29 16:15:50', '2018-11-29 16:15:50'),
(18, 'Guila Roales', 'guilaroales@gmail.com', '012026793755', 'Hi! I live in the US (Maryland) and was wondering if you do international orders on Heart Evangelista\'s collection? I would love to pre-order some!\r\n\r\nThanks a lot!', 'product inquiry', '2018-11-29 22:16:44', '2018-11-29 22:16:44'),
(19, 'Michaela dela Peña', 'aleyadp@gmail.com', '09175780122', 'Hello there! I would just like to ask how long does the shipping take? Is there a minimum purchase? Thank you!', 'product inquiry', '2018-11-30 01:21:20', '2018-11-30 01:21:20'),
(20, 'Karen Pascual', 'karenmariep808@gmail.com', '808-343-6292', 'Aloha,\r\n\r\nI’ve been following Albert Kurniawan on YouTube. And I’m. so  happy for him.  He inspires me.  :)\r\n\r\nWould love try his new product line.  And wanted to know if you guys ship your products here in the United States, Honolulu, Hawaii?\r\n\r\nKaren', 'product inquiry', '2018-11-30 07:40:17', '2018-11-30 07:40:17'),
(21, 'Krista Nikki Amar', 'knpa101@gmail.com', '+971505778901', 'Hello\r\n\r\nI was just about to pay but I can see that you are only providing options to ship within the Philippines.\r\n\r\nI want to purchase now. But I am based in Dubai, UAE. \r\n\r\nIs it possible at all to get this shipped here??\r\n\r\n- Krista', 'product inquiry', '2018-11-30 08:59:14', '2018-11-30 08:59:14'),
(22, 'Tessa Gaerlan', 'tf_gaerlan@hotmail.com', '61430131354', 'Hi, \r\n\r\nI just placed an order. Order number 27181130215306, the address in the receipt i received is missing information.\r\n\r\nCan you make sure that the address on your system is correct: 1920 EGI Taft Tower Taft Ave, Malate Metro Manila, Brgy 708, postcode 1004 NCR. Thanks, Tessa', 'order status', '2018-11-30 14:01:59', '2018-11-30 14:01:59'),
(23, 'Vanessa', 'veesarah@gmail.com', '+65 81890597', 'Hello! Do you guys ship to Singapore? I really love this but I’m not  residing sa Pinas. Please say yes, pretty please.', 'product inquiry', '2018-12-01 02:42:28', '2018-12-01 02:42:28'),
(24, 'Grace', 'gracebausa@gmail.com', '09175067999', 'Is it possible Can I make bank deposit? And may I know what\'s your best seller for false eyelashes upper and lower', 'product inquiry', '2018-12-01 03:28:34', '2018-12-01 03:28:34');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2015_08_04_131626_create_tags_table', 1),
(2, '2016_07_24_060101_add_slug_to_tags_table', 1),
(3, '2017_07_03_000000_create_users_table', 1),
(4, '2017_07_03_000001_create_carriers_table', 1),
(5, '2017_07_03_000002_create_attribute_sets_table', 1),
(6, '2017_07_03_000003_create_password_resets_table', 1),
(7, '2017_07_03_000004_create_settings_table', 1),
(8, '2017_07_03_000006_create_taxes_table', 1),
(9, '2017_07_03_000007_create_notification_templates_table', 1),
(10, '2017_07_03_000008_create_currencies_table', 1),
(11, '2017_07_03_000009_create_order_statuses_table', 1),
(12, '2017_07_03_000010_create_product_groups_table', 1),
(13, '2017_07_03_000011_create_categories_table', 1),
(14, '2017_07_03_000012_create_attributes_table', 1),
(15, '2017_07_03_000013_create_countries_table', 1),
(16, '2017_07_03_000014_create_notifications_table', 1),
(17, '2017_07_03_000015_create_addresses_table', 1),
(18, '2017_07_03_000016_create_attribute_attribute_set_table', 1),
(19, '2017_07_03_000017_create_companies_table', 1),
(20, '2017_07_03_000018_create_products_table', 1),
(21, '2017_07_03_000019_create_attribute_values_table', 1),
(22, '2017_07_03_000020_create_orders_table', 1),
(23, '2017_07_03_000021_create_order_product_table', 1),
(24, '2017_07_03_000022_create_product_images_table', 1),
(25, '2017_07_03_000023_create_attribute_product_value_table', 1),
(26, '2017_07_03_000024_create_order_status_history_table', 1),
(27, '2017_07_03_000025_create_category_product_table', 1),
(28, '2017_07_03_000026_create_permission_tables', 1),
(29, '2017_08_07_091623_create_cart_rules_table', 1),
(30, '2017_08_07_102321_create_cart_rules_customers_table', 1),
(31, '2017_08_07_102633_create_cart_rules_categories_table', 1),
(32, '2017_08_07_103148_create_cart_rules_products_table', 1),
(33, '2017_08_07_111214_create_cart_rules_product_groups_table', 1),
(34, '2017_08_07_111321_create_cart_rules_combinations_table', 1),
(35, '2017_08_17_131600_create_specific_prices_table', 1),
(36, '2018_01_10_120321_delete_notifications_table', 1),
(37, '2018_01_23_153705_modify_orders_table_created_at_required', 1),
(38, '2018_02_07_212253_add_currency_id_to_countries', 1),
(39, '2018_02_27_121550_create_product_badge', 1),
(40, '2018_02_27_182300_create_product_measurement', 1),
(41, '2018_03_27_175840_add_created_by_to_order_status', 1),
(42, '2018_04_23_143850_add_specific_price_id_to_order_product', 1),
(43, '2018_04_29_200700_create_order_cart_rules', 1),
(44, '2018_05_25_155911_create_cache_table', 1),
(45, '2018_10_02_230218_modify_order_products', 1),
(46, '2018_10_08_213723_order_cart_rules_new_fields', 1),
(47, '2018_10_10_102517_add_email_verified_at_in_users', 1),
(48, '2018_11_13_112034_add_address_fields_in_addresses', 1),
(49, '2018_11_13_135806_add_type_in_addresses', 1),
(50, '2018_11_14_132512_create_shoppingcart_table', 1),
(51, '2018_11_14_165853_create_sessions_table', 1),
(52, '2018_11_20_093828_create_subscribers_table', 1),
(53, '2018_11_21_143349_add_default_address_to_user', 1),
(54, '2018_11_21_150146_add_magpie_charge_id_and_customer_id_in_orders', 1),
(55, '2018_11_21_205005_add_mobile_number_and_telephone_number_to_user', 1),
(56, '2018_11_21_225332_change_mobile_phone_nullable', 1),
(57, '2018_11_22_003659_add_email_to_address', 1),
(58, '2018_11_22_174116_create_inquiries_table', 1),
(59, '2018_11_22_220952_rename_price_with_tax_to_price_without_tax_from_order_products', 1),
(60, '2018_11_23_051119_add_region_in_addresses', 1),
(61, '2018_11_23_114918_add_vars_in_shoppingcart', 1),
(62, '2018_11_23_214219_drop_primary_in_shopping_cart', 1),
(63, '2018_11_25_164104_add_province_in_addresses', 1),
(64, '2018_11_25_175203_add_photo_path_in_users', 1),
(65, '2018_11_25_182605_change_photo_path_to_photo_in_users', 1),
(66, '2018_11_25_192156_create_wishes_table', 1),
(67, '2018_11_25_225846_remove_province_in_address', 1),
(68, '2018_11_28_193633_add_slug_in_products', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notification_templates`
--

CREATE TABLE `notification_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notification_templates`
--

INSERT INTO `notification_templates` (`id`, `name`, `slug`, `model`, `body`) VALUES
(1, 'Order Status Changed', 'order-status-changed', 'Order', '<p>Hello,&nbsp;&nbsp;{{ userName }},</p>\n                            <p>Your order status was changed to&nbsp;&nbsp;{{ status }}.</p>\n\n                            <p>Best,</p>\n                            <p>eStarter team.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `carrier_id` int(10) UNSIGNED NOT NULL,
  `shipping_address_id` int(10) UNSIGNED DEFAULT NULL,
  `billing_address_id` int(10) UNSIGNED DEFAULT NULL,
  `billing_company_id` int(10) UNSIGNED DEFAULT NULL,
  `currency_id` int(10) UNSIGNED NOT NULL,
  `comment` mediumtext COLLATE utf8_unicode_ci,
  `shipping_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_date` datetime DEFAULT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `total_discount` decimal(13,2) DEFAULT NULL,
  `total_discount_tax` decimal(13,2) DEFAULT NULL,
  `total_shipping` decimal(13,2) DEFAULT NULL,
  `total_shipping_tax` decimal(13,2) DEFAULT NULL,
  `total` decimal(13,2) DEFAULT NULL,
  `total_tax` decimal(13,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `magpie_charge_id` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `magpie_customer_id` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_cart_rules`
--

CREATE TABLE `order_cart_rules` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `cart_rule_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` datetime NOT NULL,
  `expiration_date` datetime NOT NULL,
  `free_delivery` tinyint(1) NOT NULL DEFAULT '0',
  `discount_type` enum('Percent - order','Percent - selected products','Amount - selected products','Amount - order') COLLATE utf8_unicode_ci NOT NULL,
  `reduction_amount` decimal(13,2) DEFAULT '0.00',
  `reduction_currency_id` int(10) UNSIGNED DEFAULT NULL,
  `total_discount` decimal(13,2) NOT NULL,
  `gift` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sku` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(13,2) DEFAULT NULL,
  `price_without_tax` decimal(13,2) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `specific_price_id` int(10) UNSIGNED DEFAULT NULL,
  `specific_price_reduction` decimal(13,2) DEFAULT '0.00',
  `specific_price_discount_type` enum('Amount','Percent') COLLATE utf8_unicode_ci DEFAULT NULL,
  `specific_price_start_date` datetime DEFAULT NULL,
  `specific_price_expiration_date` datetime DEFAULT NULL,
  `amount_per_item` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notification` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `name`, `notification`) VALUES
(1, 'Pending', 0),
(2, 'Paid', 1),
(3, 'Processing', 2),
(4, 'Delivered', 3),
(5, 'Done', 4),
(6, 'Cancelled', 5);

-- --------------------------------------------------------

--
-- Table structure for table `order_status_history`
--

CREATE TABLE `order_status_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('tjay.duldulao@gmail.com', '$2y$10$MddIF5iVH//nNOkq3j04/eUvpcJjEtiNPt5RrqVU8QGLpRlADUuia', '2018-11-28 16:49:42'),
('tj@boomtech.com', '$2y$10$up1iiCi2PQvu782OwFI5/O4lHWLm/H1GEeXex/gwAdgsJguLkMUTa', '2018-11-30 08:00:21');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'list_categories', NULL, NULL),
(2, 'create_category', NULL, NULL),
(3, 'update_category', NULL, NULL),
(4, 'delete_category', NULL, NULL),
(5, 'reorder_categories', NULL, NULL),
(6, 'list_products', NULL, NULL),
(7, 'create_product', NULL, NULL),
(8, 'update_product', NULL, NULL),
(9, 'clone_product', NULL, NULL),
(10, 'delete_product', NULL, NULL),
(11, 'list_attributes', NULL, NULL),
(12, 'create_attribute', NULL, NULL),
(13, 'update_attribute', NULL, NULL),
(14, 'delete_attribute', NULL, NULL),
(15, 'list_attribute_sets', NULL, NULL),
(16, 'create_attribute_set', NULL, NULL),
(17, 'update_attribute_set', NULL, NULL),
(18, 'delete_attribute_set', NULL, NULL),
(19, 'list_currencies', NULL, NULL),
(20, 'create_currency', NULL, NULL),
(21, 'update_currency', NULL, NULL),
(22, 'delete_currency', NULL, NULL),
(23, 'list_carriers', NULL, NULL),
(24, 'create_carrier', NULL, NULL),
(25, 'update_carrier', NULL, NULL),
(26, 'delete_carrier', NULL, NULL),
(27, 'list_taxes', NULL, NULL),
(28, 'create_tax', NULL, NULL),
(29, 'update_tax', NULL, NULL),
(30, 'delete_tax', NULL, NULL),
(31, 'list_order_statuses', NULL, NULL),
(32, 'create_order_status', NULL, NULL),
(33, 'update_order_status', NULL, NULL),
(34, 'delete_order_status', NULL, NULL),
(35, 'list_customers', NULL, NULL),
(36, 'create_customer', NULL, NULL),
(37, 'update_customer', NULL, NULL),
(38, 'delete_customer', NULL, NULL),
(39, 'list_cart_rules', NULL, NULL),
(40, 'create_cart_rule', NULL, NULL),
(41, 'update_cart_rule', NULL, NULL),
(42, 'delete_cart_rule', NULL, NULL),
(43, 'list_specific_prices', NULL, NULL),
(44, 'create_specific_price', NULL, NULL),
(45, 'update_specific_price', NULL, NULL),
(46, 'delete_specific_price', NULL, NULL),
(47, 'list_notification_templates', NULL, NULL),
(48, 'create_notification_template', NULL, NULL),
(49, 'update_notification_template', NULL, NULL),
(50, 'delete_notification_template', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_roles`
--

CREATE TABLE `permission_roles` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_roles`
--

INSERT INTO `permission_roles` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission_users`
--

CREATE TABLE `permission_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `attribute_set_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `tax_id` int(10) UNSIGNED NOT NULL,
  `price` decimal(13,2) DEFAULT NULL,
  `sku` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `stock` int(11) DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `group_id`, `attribute_set_id`, `name`, `slug`, `description`, `tax_id`, `price`, `sku`, `stock`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Amore', 'amore', '<p><big>Love Marie for Teviant&nbsp;Collection</big></p>\r\n\r\n<p><big>Here&rsquo;s to new beginnings. Embracing innocence and naivit&eacute;, Amore is a soft breath of fresh air. She has a fresh take on life and is ready to experience it.</big></p>\r\n\r\n<p><big>Soft and earth-toned shades for a soft, everyday look. With various shades of nude that&rsquo;s suitable for all skin tones.</big></p>\r\n\r\n<p><big>Use matte colors on the whole eyelid, followed by shimmery shades in the middle for definition. Concentrate dark tones on the outside, intensifying according to time of day.</big></p>\r\n\r\n<p><big>Shades included: Stellato, Rose, Grazie, Gelato, Sole, Vita, Bella, Toscana, Allora, Dolce, Cappucino, Milano</big></p>\r\n\r\n<p><strong><big>Ships out December 15, 2018</big></strong></p>', 1, '2450.00', '0710535605501', 2972, 1, NULL, '2018-11-30 18:16:59'),
(2, 2, 1, 'Mademoiselle', 'mademoiselle', '<p><big>Love Marie for Teviant Collection </big></p>\r\n\r\n<p><big>Fly free as a bird. A daydreamer and goal getter, Mademoiselle rides the waves yet loves a good experiment. She lives in the moment and is constantly on a mission to find herself. </big></p>\r\n\r\n<p><big>Jewel-toned pigments for rendezvous-ready eyes. A balanced mix of pigments and textures for that bold eye look in just one palette. A great transition palette from day to night. Use the more sober colors for day and build on it with jewel-toned hues for night.</big></p>\r\n\r\n<p><big>Shades included: La Mode, Ooh, Lala!, Monde, Cest La Vie, Provences, Lande, Bonjour, Eiffel, Merci, Caf&eacute;, Nuit, Rendevous</big></p>\r\n\r\n<p><big>&nbsp;<strong>Ships out December 15, 2018</strong></big></p>', 1, '2450.00', '0710535605518', 2998, 1, '2018-11-28 18:23:35', '2018-11-30 10:22:25'),
(3, 3, 1, 'Señorita', 'senorita', '<p><big>Love Marie for Teviant Collection </big></p>\r\n\r\n<p><big>Life is an endless party. Finding love and enjoying her youth, Se&ntilde;orita is the embodiment of rebellious passion. She is charming and mysterious&mdash;a soul that has been set on fire. </big></p>\r\n\r\n<p><big>Ladylike tones for a look that&rsquo;s forever young. A versatile palette that coasts from simple, to soft, to glamorous, to bold. Choose the pastel and neutral shades for a feminine look, while use the bold and dark colors for added edge. </big></p>\r\n\r\n<p><big>Shades included: Beso, Envy, Mialma, Destiny, Fantasia, Lust, Sangria, Carino, Vivir, Chica, Papi, Azar</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '2450.00', '0710535605525', 2989, 1, '2018-11-28 18:24:27', '2018-11-30 19:48:14'),
(4, 4, 1, 'Queen', 'queen', '<p><big>Love Marie for Teviant Collection</big></p>\r\n\r\n<p><big>She&rsquo;s out to conquer the world&mdash;with her crown in place. Like true royalty, Queen is aware of herself and exudes power, elegance, and regality. At the end of the day, she always knows that her best bet is herself. </big></p>\r\n\r\n<p><big>Candy pop bright colors for a boost of makeup-induced confidence. Richly pigmented, colors stand out in a swipe. Go ahead, be bold and don&rsquo;t be afraid to combine! </big></p>\r\n\r\n<p><big>Shades included: Neit, Nile, Palace, Aswan, Cleo, Luxor, Regal, Offering, Worship, Scarab, rania and Pharaoh </big></p>\r\n\r\n<p><strong><big>Ships out December 15, 2018</big></strong></p>', 1, '2450.00', '0710535605532', 2978, 1, '2018-11-28 18:25:21', '2018-11-30 18:16:59'),
(5, 5, 1, 'Madam', 'madam', '<p><big>This special no-clump formulation allow for coverage while maintaining a natural look. With enough product on the wand, brush up to shape and set eyebrows.</big></p>\r\n\r\n<p><big>Available in four shades.</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '995.00', '0710535605549', 2500, 1, '2018-11-28 18:27:04', '2018-11-28 19:50:33'),
(6, 6, 1, 'Lady', 'lady', '<p><big>This special no-clump formulation allow for coverage while maintaining a natural look. With enough product on the wand, brush up to shape and set eyebrows.</big></p>\r\n\r\n<p><big>Available in four shades.</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '995.00', '0710535605556', 5000, 1, '2018-11-28 18:28:09', '2018-11-28 19:50:50'),
(7, 7, 1, 'Dame', 'dame', '<p><big>This special no-clump formulation allow for coverage while maintaining a natural look. With enough product on the wand, brush up to shape and set eyebrows.</big></p>\r\n\r\n<p><big>Available in four shades.</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '995.00', '0710535605563', 4999, 1, '2018-11-28 18:28:50', '2018-11-29 10:24:08'),
(8, 8, 1, 'Miss', 'miss', '<p><big>This special no-clump formulation allow for coverage while maintaining a natural look. With enough product on the wand, brush up to shape and set eyebrows. </big></p>\r\n\r\n<p><big>Available in four shades. </big></p>\r\n\r\n<p><strong><big>Ships out December 15, 2018</big></strong></p>', 1, '995.00', '0710535605570', 2500, 1, '2018-11-28 18:29:52', '2018-11-28 19:57:04'),
(9, 9, 1, 'Pearl', 'pearl', '<p><big>Formulated in Italy, this long-wearing, quick drying eyeliner is composed of precise sizes of glitter particles and materials that keep eyes safe. With the thin applicator brush, build on multiple swipes or apply on top of liquid eyeliner for a fun look.</big></p>\r\n\r\n<p><big>Available in four shades.</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '995.00', '0710535605587', 1497, 1, '2018-11-28 18:30:43', '2018-11-30 18:16:59'),
(10, 10, 1, 'Amber', 'amber', '<p><big>Formulated in Italy, this long-wearing, quick drying eyeliner is composed of precise sizes of glitter particles and materials that keep eyes safe. With the thin applicator brush, build on multiple swipes or apply on top of liquid eyeliner for a fun look. </big></p>\r\n\r\n<p><big>Available in four shades. </big></p>\r\n\r\n<p><strong><big>Ships out December 15, 2018</big></strong></p>', 1, '995.00', '0710535605594', 1495, 1, '2018-11-28 18:31:42', '2018-11-28 20:46:00'),
(11, 11, 1, 'Oro', 'oro', '<p><big>Formulated in Italy, this long-wearing, quick drying eyeliner is composed of precise sizes of glitter particles and materials that keep eyes safe. With the thin applicator brush, build on multiple swipes or apply on top of liquid eyeliner for a fun look.</big></p>\r\n\r\n<p><big>Available in four shades. </big></p>\r\n\r\n<p><strong><big>Ships out December 15, 2018</big></strong></p>', 1, '995.00', '0710535605600', 1500, 1, '2018-11-28 18:32:35', '2018-11-28 19:51:52'),
(12, 12, 1, 'Argent', 'argent', '<p><big>Formulated in Italy, this long-wearing, quick drying eyeliner is composed of precise sizes of glitter particles and materials that keep eyes safe. With the thin applicator brush, build on multiple swipes or apply on top of liquid eyeliner for a fun look. </big></p>\r\n\r\n<p><big>Available in four shades. </big></p>\r\n\r\n<p><strong><big>Ships out December 15, 2018</big></strong></p>', 1, '995.00', '0710535605617', 1500, 1, '2018-11-28 18:34:11', '2018-11-28 19:52:01'),
(13, 13, 1, 'Empress', 'empress', '<p><big>Create the perfect eyebrow gradient with this highly pigmented eyebrow duo. Each product comes in a dark and light powder. Apply the darker shade from the outside to the arch point, and the lighter shade from the inner brow until it blends with the darker color. Blend further until natural-looking. </big></p>\r\n\r\n<p><big>Available in two shades. </big></p>\r\n\r\n<p><strong><big>Ship&nbsp;out December 15,2018</big></strong></p>', 1, '995.00', '0710535605624', 4998, 1, '2018-11-28 18:35:58', '2018-11-30 13:53:06'),
(14, 14, 1, 'Queen Duo', 'queen-duo', '<p><big>Create the perfect eyebrow gradient with this highly pigmented eyebrow duo. Each product comes in a dark and light powder. Apply the darker shade from the outside to the arch point, and the lighter shade from the inner brow until it blends with the darker color. Blend further until natural-looking. </big></p>\r\n\r\n<p><big>Available in two shades.</big></p>\r\n\r\n<p><strong><big>Ships out December 15, 2018</big></strong></p>', 1, '995.00', '0710535605631', 5000, 1, '2018-11-28 18:36:52', '2018-11-28 21:13:43'),
(15, 15, 1, 'Highness', 'highness', '<p><big>Create the perfect eyebrow gradient with this highly pigmented eyebrow duo. Each product comes in a dark and light powder. Apply the darker shade from the outside to the arch point, and the lighter shade from the inner brow until it blends with the darker color. Blend further until natural-looking. </big></p>\r\n\r\n<p><big>Available in two shades.</big></p>\r\n\r\n<p><strong><big>Ships out December 15, 2018</big></strong></p>', 1, '995.00', '0710535605648', 4999, 1, '2018-11-28 18:38:42', '2018-11-30 07:38:59'),
(16, 16, 1, 'She', 'she', '<p><big>Created with a special four-point brush on its tip, this unique product mimics hair strokes in the eyebrows to produce a 3-D microbladed look. Apply directly on fresh and dry brows to maintain the rich pigment. </big></p>\r\n\r\n<p><big>Available in three shades.&nbsp;</big></p>\r\n\r\n<p><strong><big>Ships out December 15, 2018</big></strong></p>', 1, '995.00', '0710535605655', 5599, 1, '2018-11-28 18:45:44', '2018-11-29 16:57:38'),
(17, 17, 1, 'Her', 'her', '<p><big>Created with a special four-point brush on its tip, this unique product mimics hair strokes in the eyebrows to produce a 3-D microbladed look. Apply directly on fresh and dry brows to maintain the rich pigment. </big></p>\r\n\r\n<p><big>Available in three shades.&nbsp;</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '995.00', '0710535605662', 5593, 1, '2018-11-28 18:48:26', '2018-11-30 14:57:28'),
(18, 18, 1, 'Herself', 'herself', '<p><big>Created with a special four-point brush on its tip, this unique product mimics hair strokes in the eyebrows to produce a 3-D microbladed look. Apply directly on fresh and dry brows to maintain the rich pigment.</big></p>\r\n\r\n<p><big>Available in three shades.&nbsp;</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '995.00', '0710535605679', 4998, 1, '2018-11-28 18:49:16', '2018-11-30 18:16:59'),
(19, 19, 1, 'Duenna', 'duenna', '<p><big>Defined eyes for up to 18 hours&mdash;Albert perfected this eyeliner formula until he made sure it could withstand long hours in tropical weather. Comes with a sharp point and hard bristle for easy application. Keep a steady hand while swiping outward, keeping as close to the lashline as possible.</big></p>\r\n\r\n<p><big>Available in three shades.</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '795.00', '0710535605686', 3500, 1, '2018-11-28 18:50:31', '2018-11-28 20:29:15'),
(20, 20, 1, 'Colleen', 'colleen', '<p><big>Defined eyes for up to 18 hours&mdash;Albert perfected this eyeliner formula until he made sure it could withstand long hours in tropical weather. Comes with a sharp point and hard bristle for easy application. Keep a steady hand while swiping outward, keeping as close to the lashline as possible. </big></p>\r\n\r\n<p><big>Available in three shades.</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '795.00', '0710535605693', 2993, 1, '2018-11-28 18:51:40', '2018-11-30 18:16:59'),
(21, 21, 1, 'Virago', 'virago', '<p><big>Defined eyes for up to 18 hours&mdash;Albert perfected this eyeliner formula until he made sure it could withstand long hours in tropical weather. Comes with a sharp point and hard bristle for easy application. Keep a steady hand while swiping outward, keeping as close to the lashline as possible. </big></p>\r\n\r\n<p><big>Available in three shades.</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '795.00', '0710535605709', 3500, 1, '2018-11-28 18:53:30', '2018-11-28 20:29:57'),
(22, 22, 1, 'Mon Cheri', 'mon-cheri', '<p><big>Not all mascaras are created equal, and this two-wand mascara is surely superior. Waterproof, smudge-proof and long wearing, the mascara makes lashes appear longer and fuller. Its double wands are composed of tiny fibers, with the large brush meant to be used for upper lashes and the small brush especially designed for lower lashes. Stroke outward from the base for a dramatic effect.</big></p>\r\n\r\n<p><big>Available in four shades.</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '795.00', '0710535605716', 11995, 1, '2018-11-28 18:59:35', '2018-11-30 14:57:28'),
(23, 23, 1, 'Love', 'love', '<p><big>Versatile lashes suited for everyday, suitable for almond to round-shaped lashes. Hand-made and designed to appear natural.</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '295.00', '0710535605723', 492, 1, '2018-11-28 19:00:38', '2018-11-30 19:48:14'),
(24, 24, 1, 'Naomi', 'naomi', '<p><big>Best for all eye shapes, and makes eyes look instantly awake. Hand-made and designed to appear natural.</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '295.00', '0710535605730', 498, 1, '2018-11-28 19:01:45', '2018-11-30 11:05:36'),
(25, 25, 1, 'Nelly', 'nelly', '<p><big>A daily lash option for a naturally bright eyes. Hand-made and designed to appear natural.</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '295.00', '0710535605747', 499, 1, '2018-11-28 19:02:48', '2018-11-30 08:56:47'),
(26, 26, 1, 'Tyra', 'tyra', '<p><big>Don&rsquo;t be intimidated by its spiky cut&mdash;these are perfect for a soft, feminine look that can go from daily to glam. Hand-made and designed to appear natural.</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '295.00', '0710535605754', 497, 1, '2018-11-28 19:03:44', '2018-11-30 11:06:11'),
(27, 27, 1, 'Taylor', 'taylor', '<p><big>A no-makeup look staple that makes eyes look expressive. Hand-made and designed to appear natural.</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '295.00', '0710535605761', 498, 1, '2018-11-28 19:08:15', '2018-11-30 11:06:32'),
(28, 28, 1, 'Grace', 'grace', '<p><big>A hard-to-find yet much needed makeup staple, these lower lashes are the perfect subtle finish to any makeup look&mdash;or even without makeup!</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '295.00', '0710535605778', 498, 1, '2018-11-28 19:09:04', '2018-11-30 10:15:56'),
(29, 29, 1, 'Linda', 'linda', '<p><big>A hard-to-find yet much needed makeup staple, these lower lashes are the perfect subtle finish to any makeup look&mdash;or even without makeup!</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '295.00', '0710535605785', 495, 1, '2018-11-28 19:09:47', '2018-11-30 08:53:51'),
(30, 30, 1, 'Eyelash Curler', 'eyelash-curler', '<p><big>Purposely designed for Asian eyes, this tool envelopes the lashes perfectly with its modified curve and length. </big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '995.00', '4897085970016', 2999, 1, '2018-11-28 19:11:06', '2018-11-30 04:55:19'),
(31, 31, 1, 'False Lashes Applicator', 'false-lashes-applicator', '<p><big>Made only with medical-grade material used in surgery tools, this lash applicator is rust-resistant and safe to use near the eyes.</big></p>\r\n\r\n<p><big><strong>Ships out December 15, 2018</strong></big></p>', 1, '1150.00', '4897085970139', 999, 1, '2018-11-28 19:12:11', '2018-11-30 04:52:01'),
(32, 32, 1, 'Tweezers', 'tweezers', '<p>&nbsp; &nbsp; &nbsp; &nbsp;</p>', 1, '1150.00', '4897085970122', 1500, 1, '2018-11-30 04:53:42', '2018-11-30 04:53:42');

-- --------------------------------------------------------

--
-- Table structure for table `product_badges`
--

CREATE TABLE `product_badges` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_badges`
--

INSERT INTO `product_badges` (`id`, `product_id`, `title`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL, 0, '2018-11-28 18:23:35', '2018-11-28 18:23:35'),
(2, 3, NULL, NULL, 0, '2018-11-28 18:24:27', '2018-11-28 18:24:27'),
(3, 4, NULL, NULL, 0, '2018-11-28 18:25:21', '2018-11-28 18:25:21'),
(4, 5, NULL, NULL, 0, '2018-11-28 18:27:04', '2018-11-28 18:27:04'),
(5, 6, NULL, NULL, 0, '2018-11-28 18:28:09', '2018-11-28 18:28:09'),
(6, 7, NULL, NULL, 0, '2018-11-28 18:28:50', '2018-11-28 18:28:50'),
(7, 8, NULL, NULL, 0, '2018-11-28 18:29:52', '2018-11-28 18:29:52'),
(8, 9, NULL, NULL, 0, '2018-11-28 18:30:43', '2018-11-28 18:30:43'),
(9, 10, NULL, NULL, 0, '2018-11-28 18:31:42', '2018-11-28 18:31:42'),
(10, 11, NULL, NULL, 0, '2018-11-28 18:32:35', '2018-11-28 18:32:35'),
(11, 12, NULL, NULL, 0, '2018-11-28 18:34:11', '2018-11-28 18:34:11'),
(12, 13, NULL, NULL, 0, '2018-11-28 18:35:58', '2018-11-28 18:35:58'),
(13, 14, NULL, NULL, 0, '2018-11-28 18:36:52', '2018-11-28 18:36:52'),
(14, 15, NULL, NULL, 0, '2018-11-28 18:38:43', '2018-11-28 18:38:43'),
(15, 16, NULL, NULL, 0, '2018-11-28 18:45:44', '2018-11-28 18:45:44'),
(16, 17, NULL, NULL, 0, '2018-11-28 18:48:26', '2018-11-28 18:48:26'),
(17, 18, NULL, NULL, 0, '2018-11-28 18:49:16', '2018-11-28 18:49:16'),
(18, 19, NULL, NULL, 0, '2018-11-28 18:50:31', '2018-11-28 18:50:31'),
(19, 20, NULL, NULL, 0, '2018-11-28 18:51:40', '2018-11-28 18:51:40'),
(20, 21, NULL, NULL, 0, '2018-11-28 18:53:30', '2018-11-28 18:53:30'),
(21, 22, NULL, NULL, 0, '2018-11-28 18:59:35', '2018-11-28 18:59:35'),
(22, 23, NULL, NULL, 0, '2018-11-28 19:00:38', '2018-11-28 19:00:38'),
(23, 24, NULL, NULL, 0, '2018-11-28 19:01:45', '2018-11-28 19:01:45'),
(24, 25, NULL, NULL, 0, '2018-11-28 19:02:48', '2018-11-28 19:02:48'),
(25, 26, NULL, NULL, 0, '2018-11-28 19:03:44', '2018-11-28 19:03:44'),
(26, 27, NULL, NULL, 0, '2018-11-28 19:08:15', '2018-11-28 19:08:15'),
(27, 28, NULL, NULL, 0, '2018-11-28 19:09:04', '2018-11-28 19:09:04'),
(28, 29, NULL, NULL, 0, '2018-11-28 19:09:48', '2018-11-28 19:09:48'),
(29, 30, NULL, NULL, 0, '2018-11-28 19:11:06', '2018-11-28 19:11:06'),
(30, 31, NULL, NULL, 0, '2018-11-28 19:12:11', '2018-11-28 19:12:11'),
(31, 32, NULL, NULL, 0, '2018-11-30 04:53:43', '2018-11-30 04:53:43');

-- --------------------------------------------------------

--
-- Table structure for table `product_groups`
--

CREATE TABLE `product_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_groups`
--

INSERT INTO `product_groups` (`id`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL),
(2, '2018-11-28 18:23:35', '2018-11-28 18:23:35'),
(3, '2018-11-28 18:24:27', '2018-11-28 18:24:27'),
(4, '2018-11-28 18:25:21', '2018-11-28 18:25:21'),
(5, '2018-11-28 18:27:04', '2018-11-28 18:27:04'),
(6, '2018-11-28 18:28:09', '2018-11-28 18:28:09'),
(7, '2018-11-28 18:28:50', '2018-11-28 18:28:50'),
(8, '2018-11-28 18:29:52', '2018-11-28 18:29:52'),
(9, '2018-11-28 18:30:43', '2018-11-28 18:30:43'),
(10, '2018-11-28 18:31:42', '2018-11-28 18:31:42'),
(11, '2018-11-28 18:32:35', '2018-11-28 18:32:35'),
(12, '2018-11-28 18:34:11', '2018-11-28 18:34:11'),
(13, '2018-11-28 18:35:58', '2018-11-28 18:35:58'),
(14, '2018-11-28 18:36:52', '2018-11-28 18:36:52'),
(15, '2018-11-28 18:38:42', '2018-11-28 18:38:42'),
(16, '2018-11-28 18:45:43', '2018-11-28 18:45:43'),
(17, '2018-11-28 18:48:26', '2018-11-28 18:48:26'),
(18, '2018-11-28 18:49:16', '2018-11-28 18:49:16'),
(19, '2018-11-28 18:50:31', '2018-11-28 18:50:31'),
(20, '2018-11-28 18:51:39', '2018-11-28 18:51:39'),
(21, '2018-11-28 18:53:30', '2018-11-28 18:53:30'),
(22, '2018-11-28 18:59:35', '2018-11-28 18:59:35'),
(23, '2018-11-28 19:00:38', '2018-11-28 19:00:38'),
(24, '2018-11-28 19:01:45', '2018-11-28 19:01:45'),
(25, '2018-11-28 19:02:48', '2018-11-28 19:02:48'),
(26, '2018-11-28 19:03:44', '2018-11-28 19:03:44'),
(27, '2018-11-28 19:08:15', '2018-11-28 19:08:15'),
(28, '2018-11-28 19:09:04', '2018-11-28 19:09:04'),
(29, '2018-11-28 19:09:47', '2018-11-28 19:09:47'),
(30, '2018-11-28 19:11:06', '2018-11-28 19:11:06'),
(31, '2018-11-28 19:12:11', '2018-11-28 19:12:11'),
(32, '2018-11-30 04:53:42', '2018-11-30 04:53:42');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `name`, `order`) VALUES
(2, 1, '307dd9f14ea6d1eeb2776faa47dfee4c.jpg', 0),
(3, 1, '9156e723832e5cdfc2fe0ef0604dc3eb.jpg', 2),
(5, 2, '97d46df51cb5be75c6b4e7db4d188704.jpg', 3),
(6, 2, 'f441cddfa4545a924ef7e4bec1d89afc.jpg', 0),
(7, 2, '7f1c2719161fd9ad03993b77bc63bbcd.jpg', 2),
(8, 3, 'c7b944fea7a384d5256aa4c68b33cf03.jpg', 0),
(9, 3, '19870ed9cbea767bb9958f0f650b5426.jpg', 2),
(11, 4, '0aee38c95479539f28efb9fb3d702185.jpg', 2),
(12, 4, '94838e2b78cd86a91fab81001e4418c3.jpg', 0),
(15, 5, 'b518931d372e266849b5c481cd610141.jpg', 1),
(16, 5, 'c54c83f476223c4fed3f8017c0299f08.jpg', 0),
(17, 6, 'f0788bd5275186b696589b2a83cf37d0.jpg', 1),
(18, 6, '2d828f70a8ac0965002e36085a16759e.jpg', 0),
(19, 7, '7fb4fdf1c7f69e4b5c31eb3b3532486c.jpg', 0),
(20, 7, 'ea28cae9551e4ff01d739c05e918f4c8.jpg', 1),
(21, 8, '95aaffa501c28d2881de7519b59c5466.jpg', 0),
(22, 8, 'c0096270b5eb5e08962f24e09306af7a.jpg', 1),
(23, 16, '144ed69e287ce95397ff7d3c1befb9dc.jpg', 0),
(24, 16, '4b3e75f5d88f9b3ec38c6a626bd765c4.jpg', 1),
(25, 17, 'd92eca39ca1cb236e8ec576913a2ee9c.jpg', 0),
(26, 17, '411f3975b3fe9ddd9268d4dfa66ffd0b.jpg', 1),
(27, 18, '899834e34315b13117014768fb378456.jpg', 0),
(28, 18, '32be5d40fef6b12863a5f56f45615049.jpg', 1),
(30, 19, '4e7c8e0952df560bce85a1698942df34.jpg', 0),
(31, 19, '266c3b6caee005797c608e6902745c91.jpg', 1),
(32, 20, '7025d1dffc0f2421371f63391dd26cdf.jpg', 0),
(33, 20, 'e1559d8654de79cad16147aa5c44a5d2.jpg', 1),
(34, 21, '2f5224267996e6df90ef427222dcca15.jpg', 0),
(35, 21, '576046037bd850a3688a609079ab9aa0.jpg', 1),
(36, 22, 'eab1125aa5cef03ccb78a509e4930841.jpg', 0),
(37, 22, '882bd5059d29b1bc7df94d77a5a2bad5.jpg', 1),
(40, 31, '1e1a16f422992957175318fa9c60ff5a.jpg', 2),
(41, 31, 'fb9eb4d8ebcd4e5cabe033f4695f8a3c.jpg', 3),
(42, 30, 'da0bfdda5ee732a8e354d5bd730033df.jpg', 0),
(43, 30, '47d80f8bc4fdef9c9fa8e792eb31e1d7.jpg', 1),
(44, 30, '5c62c22ced266dfa2f0dd26838a1de7b.jpg', 2),
(45, 30, '15ed2900431f06fdb443644fce5d2151.jpg', 3),
(46, 1, '0da08852d8954cfdc08c6fd817354c54.jpg', 3),
(47, 1, '80ea5d4fa00a1a9b30d2d4e393f3fcf1.jpg', 1),
(48, 2, 'ee5221ec69e5e3b174ebccc04ea32f37.jpg', 1),
(49, 3, 'efd1a5fab35140e876e1e0ad3576d897.jpg', 1),
(50, 3, '7142fa426b68c7fd7d0585cb3e3bf3bc.jpg', 3),
(51, 4, 'bc3f10aaf0c25ed84830b8e57e953838.jpg', 1),
(52, 4, 'eecc7ed6922c5897c13f7a9813367b92.jpg', 3),
(55, 10, '74b993b1ea08add4c79a24974b33799c.jpg', 0),
(57, 11, '8c428f97f466a4d6c2bfc02001df3af9.jpg', 1),
(58, 12, '658710184b17f051268e79b1997fa50e.jpg', 0),
(59, 9, 'aaba22c570f30194a9c6ce04552658cc.jpg', 0),
(61, 14, '42d1e9cc6955532f8e48112736ebe60d.jpg', 0),
(62, 14, 'f0fcc70558ea4714437e92f58dcc977f.jpg', 1),
(63, 13, '60c0384cadf4ba1a288cff8dadd1ef92.jpg', 0),
(64, 13, '5ef19bac492798835b761680890f8544.jpg', 1),
(65, 15, '0709c35134c929b6e95980845b2be7ba.jpg', 0),
(66, 15, '4b2c3448dca73e1d0d3ca5925a508324.jpg', 1),
(69, 28, 'f9f63f17f653ff75246a859732cb3bfe.jpg', 0),
(71, 24, 'ee968ef5ae507e85423df304d8aacf53.jpg', 0),
(73, 26, '17360dfa68b3d78882532b25527eaa6f.jpg', 0),
(74, 27, '95190ab92786ef29ad7dc0eab50063ed.jpg', 0),
(75, 32, '134b1097b44653cb6ca84d66fd9fd869.jpg', 0),
(76, 32, 'f93b567ddc3bf816851de741ca0c2664.jpg', 1),
(77, 29, 'e6be40f36de7bd51154e84a8f923e4e2.jpg', 0),
(78, 25, '749d768571b30abe9ef689c0ba697344.jpg', 0),
(79, 23, '3ee9ebf0a2d5966dfbca5d16d73356b4.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_measurements`
--

CREATE TABLE `product_measurements` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `weight` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `length` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `width` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `height` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_measurements`
--

INSERT INTO `product_measurements` (`id`, `product_id`, `weight`, `length`, `width`, `height`, `created_at`, `updated_at`) VALUES
(1, 1, '1', '1', '1', '1', NULL, NULL),
(2, 2, '1', '1', '1', '1', '2018-11-28 18:23:35', '2018-11-28 18:23:35'),
(3, 3, '1', '1', '1', '1', '2018-11-28 18:24:27', '2018-11-28 18:24:27'),
(4, 4, '1', '1', '1', '1', '2018-11-28 18:25:21', '2018-11-28 18:25:21'),
(5, 5, '1', '1', '1', '1', '2018-11-28 18:27:04', '2018-11-28 18:27:04'),
(6, 6, '1', '1', '1', '1', '2018-11-28 18:28:09', '2018-11-28 18:28:09'),
(7, 7, '1', '1', '1', '1', '2018-11-28 18:28:50', '2018-11-28 18:28:50'),
(8, 8, '1', '1', '1', '1', '2018-11-28 18:29:52', '2018-11-28 18:29:52'),
(9, 9, '1', '1', '1', '1', '2018-11-28 18:30:43', '2018-11-28 18:30:43'),
(10, 10, '1', '1', '1', '1', '2018-11-28 18:31:42', '2018-11-28 18:31:42'),
(11, 11, '1', '1', '1', '1', '2018-11-28 18:32:35', '2018-11-28 18:32:35'),
(12, 12, '1', '1', '1', '1', '2018-11-28 18:34:11', '2018-11-28 18:34:11'),
(13, 13, '1', '1', '1', '1', '2018-11-28 18:35:58', '2018-11-28 18:35:58'),
(14, 14, '1', '1', '1', '1', '2018-11-28 18:36:52', '2018-11-28 18:36:52'),
(15, 15, '1', '1', '1', '1', '2018-11-28 18:38:43', '2018-11-28 18:38:43'),
(16, 16, '1', '1', '1', '1', '2018-11-28 18:45:44', '2018-11-28 18:45:44'),
(17, 17, '1', '1', '1', '1', '2018-11-28 18:48:26', '2018-11-28 18:48:26'),
(18, 18, '1', '1', '1', '1', '2018-11-28 18:49:16', '2018-11-28 18:49:16'),
(19, 19, '1', '1', '1', '1', '2018-11-28 18:50:31', '2018-11-28 18:50:31'),
(20, 20, '1', '1', '1', '1', '2018-11-28 18:51:40', '2018-11-28 18:51:40'),
(21, 21, '1', '1', '1', '1', '2018-11-28 18:53:30', '2018-11-28 18:53:30'),
(22, 22, '1', '1', '1', '1', '2018-11-28 18:59:35', '2018-11-28 18:59:35'),
(23, 23, '1', '1', '1', '1', '2018-11-28 19:00:38', '2018-11-28 19:00:38'),
(24, 24, '1', '1', '1', '1', '2018-11-28 19:01:45', '2018-11-28 19:01:45'),
(25, 25, '1', '1', '1', '1', '2018-11-28 19:02:48', '2018-11-28 19:02:48'),
(26, 26, '1', '1', '1', '1', '2018-11-28 19:03:44', '2018-11-28 19:03:44'),
(27, 27, '1', '1', '1', '1', '2018-11-28 19:08:15', '2018-11-28 19:08:15'),
(28, 28, '1', '1', '1', '1', '2018-11-28 19:09:04', '2018-11-28 19:09:04'),
(29, 29, '1', '1', '1', '1', '2018-11-28 19:09:48', '2018-11-28 19:09:48'),
(30, 30, '1', '1', '1', '1', '2018-11-28 19:11:06', '2018-11-28 19:11:06'),
(31, 31, '1', '1', '1', '1', '2018-11-28 19:12:11', '2018-11-28 19:12:11'),
(32, 32, '1', '1', '1', '1', '2018-11-30 04:53:43', '2018-11-30 04:53:43');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', NULL, NULL),
(2, 'Customer', NULL, '2018-11-29 13:16:28');

-- --------------------------------------------------------

--
-- Table structure for table `role_users`
--

CREATE TABLE `role_users` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_users`
--

INSERT INTO `role_users` (`role_id`, `user_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('CGi3DUQ08L73Y5KVnnKXeGQpv6UKxRnqNbkYx52j', NULL, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.1 Safari/605.1.15', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiOXg3R21MM0dNTlc2WTd2dlUwRTVkcHdObXBvVkN3Z1dTdXo5c1FWVyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHA6Ly9sb2NhbGhvc3Q6ODAwMCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1543651737),
('IeZwSKYEujaTLRsCTBeWPReXMlgwVkY9unYYtzHD', NULL, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiRGhPQm1nWVJFa0RuQWpEbTB5YjNBdUZYbjQ2czkwZE5OV2VRaEo3dyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTM1OiJodHRwOi8vbG9jYWxob3N0OjgwMDAvY2hlY2tvdXQvcGF5bWVudD9fdG9rZW49RGhPQm1nWVJFa0RuQWpEbTB5YjNBdUZYbjQ2czkwZE5OV2VRaEo3dyZjYXJ0X3F1YW50aXR5JTVCMCU1RD0xJnByb2R1Y3RzJTVCMCU1RCU1QmlkJTVEPTEiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjQ6ImNhcnQiO2E6MTp7czo0OiJjYXJ0IjtPOjI5OiJJbGx1bWluYXRlXFN1cHBvcnRcQ29sbGVjdGlvbiI6MTp7czo4OiIAKgBpdGVtcyI7YToxOntzOjMyOiI1ZjNiMTRlMzRjODc0ZDgzNzc1NWE2NzdlMWUzOGU3MiI7TzozMjoiR2xvdWRlbWFuc1xTaG9wcGluZ2NhcnRcQ2FydEl0ZW0iOjg6e3M6NToicm93SWQiO3M6MzI6IjVmM2IxNGUzNGM4NzRkODM3NzU1YTY3N2UxZTM4ZTcyIjtzOjI6ImlkIjtpOjE7czozOiJxdHkiO3M6MToiMSI7czo0OiJuYW1lIjtzOjU6IkFtb3JlIjtzOjU6InByaWNlIjtkOjI0NTA7czo3OiJvcHRpb25zIjtPOjM5OiJHbG91ZGVtYW5zXFNob3BwaW5nY2FydFxDYXJ0SXRlbU9wdGlvbnMiOjE6e3M6ODoiACoAaXRlbXMiO2E6MTp7czoxMDoiaW1hZ2VfcGF0aCI7czo3OToiaHR0cDovLzE5Mi4xNjguNDMuNzY6ODAwMC91cGxvYWRzL3Byb2R1Y3RzLzMwN2RkOWYxNGVhNmQxZWViMjc3NmZhYTQ3ZGZlZTRjLmpwZyI7fX1zOjQ5OiIAR2xvdWRlbWFuc1xTaG9wcGluZ2NhcnRcQ2FydEl0ZW0AYXNzb2NpYXRlZE1vZGVsIjtzOjExOiJBcHBcUHJvZHVjdCI7czo0MToiAEdsb3VkZW1hbnNcU2hvcHBpbmdjYXJ0XENhcnRJdGVtAHRheFJhdGUiO2k6MDt9fX19fQ==', 1543651373),
('mF4fnV2cZRcRMjvY4nWLGPmBSO1l8rAP8tqFiQr6', NULL, '127.0.0.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiOGtKQjRGYklaSDVHbmlSZllHb3JlVmU0YjRDbW5FYk1iSFN4MklTcSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzA6Imh0dHA6Ly9sb2NhbGhvc3Q6ODAwMC9wcm9kdWN0cyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1543651737),
('oC4bLbbrjyTvj2VFefeKRGFEhBjHYeVnaKXKaM1m', NULL, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/605.1.15 (KHTML, like Gecko)', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiaEhGNUIySFI5cG1ZOUFYalc4M0dUdndWSFNrRU53MlB5cTgyOFVGWSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzA6Imh0dHA6Ly9sb2NhbGhvc3Q6ODAwMC9wcm9kdWN0cyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1543651736);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `identifier` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `instance` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `is_checkout` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vars` longtext COLLATE utf8_unicode_ci,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `specific_prices`
--

CREATE TABLE `specific_prices` (
  `id` int(10) UNSIGNED NOT NULL,
  `reduction` decimal(13,2) DEFAULT '0.00',
  `discount_type` enum('Amount','Percent') COLLATE utf8_unicode_ci NOT NULL,
  `start_date` datetime NOT NULL,
  `expiration_date` datetime NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'jheveran.bebe@yahoo.com', '2018-11-28 22:14:13', '2018-11-28 22:14:13'),
(2, 'quinchancent@yahoo.com', '2018-11-28 22:18:09', '2018-11-28 22:18:09'),
(3, 'cielocapalad@gmail.com', '2018-11-28 22:18:20', '2018-11-28 22:18:20'),
(4, 'michellemenes@gmail.com', '2018-11-28 22:22:59', '2018-11-28 22:22:59'),
(5, 'analynortanez@gmail.com', '2018-11-28 22:24:58', '2018-11-28 22:24:58'),
(6, 'rhytarlengco.17@gmail.com', '2018-11-28 22:25:22', '2018-11-28 22:25:22'),
(7, 'mvpsec@gmail.com', '2018-11-28 14:31:27', '2018-11-28 14:31:27'),
(8, 'nicolebutardo@gmail.com', '2018-11-28 14:32:05', '2018-11-28 14:32:05'),
(9, 'deniplin@gmail.com', '2018-11-28 14:48:39', '2018-11-28 14:48:39'),
(10, 'leah19val68@yahoo.com', '2018-11-28 14:54:20', '2018-11-28 14:54:20'),
(11, 'wirene888@yahoo.com', '2018-11-28 15:00:01', '2018-11-28 15:00:01'),
(12, 'lizaviola@yahoo.com', '2018-11-28 15:05:09', '2018-11-28 15:05:09'),
(13, 'jessica.mendoza0927@gmail.com', '2018-11-28 15:07:21', '2018-11-28 15:07:21'),
(14, 'lalaflores2003@yahoo.com', '2018-11-28 15:07:58', '2018-11-28 15:07:58'),
(15, 'miromodepon@gmail.com', '2018-11-28 15:13:39', '2018-11-28 15:13:39'),
(16, 'kim.gacita@gmail.com', '2018-11-28 15:36:38', '2018-11-28 15:36:38'),
(17, 'mallaribenson@yahoo.com', '2018-11-28 15:46:33', '2018-11-28 15:46:33'),
(18, 'queencleofy@yahoo.com', '2018-11-28 16:18:40', '2018-11-28 16:18:40'),
(19, 'teresitamerioles@gmail.com', '2018-11-28 16:20:37', '2018-11-28 16:20:37'),
(20, 'chandyn@yahoo.com', '2018-11-28 16:24:10', '2018-11-28 16:24:10'),
(21, 'ggtalaban@yahoo.com', '2018-11-28 16:25:24', '2018-11-28 16:25:24'),
(22, 'jennyvgarcia530@gmail.com', '2018-11-28 16:39:05', '2018-11-28 16:39:05'),
(23, 'ternfat@yahoo.com', '2018-11-28 16:40:54', '2018-11-28 16:40:54'),
(24, 'snippetsofkate@gmail.com', '2018-11-28 16:43:08', '2018-11-28 16:43:08'),
(25, 'tpadanza@gmail.com', '2018-11-28 17:08:26', '2018-11-28 17:08:26'),
(26, 'kimbernatia06@yahoo.com', '2018-11-28 17:16:08', '2018-11-28 17:16:08'),
(27, 'graceyap68@yahoo.com', '2018-11-28 17:17:44', '2018-11-28 17:17:44'),
(28, 'tf_gaerlan@hotmail.com', '2018-11-28 18:10:56', '2018-11-28 18:10:56'),
(29, 'charmed_honeyjeanne@yahoo.com', '2018-11-28 19:47:25', '2018-11-28 19:47:25'),
(30, 'burnbernabe1972@gmail.com', '2018-11-28 20:09:18', '2018-11-28 20:09:18'),
(31, 'hispoutingprincess@outlook.com', '2018-11-28 20:22:44', '2018-11-28 20:22:44'),
(32, 'ilovegwency@gmail.com', '2018-11-28 21:41:28', '2018-11-28 21:41:28'),
(33, 'm.estravo@gmail.com', '2018-11-28 21:42:17', '2018-11-28 21:42:17'),
(34, 'roschel.318aa@gmail.com', '2018-11-28 22:03:50', '2018-11-28 22:03:50'),
(35, 'gledoux12@gmail.com', '2018-11-28 22:07:51', '2018-11-28 22:07:51'),
(36, 'a0987326488@Gmail.com', '2018-11-28 22:30:25', '2018-11-28 22:30:25'),
(37, 'marieldgzmn@yahoo.com', '2018-11-28 22:34:02', '2018-11-28 22:34:02'),
(38, 'hamtigkaren@yahoo.com', '2018-11-28 22:41:02', '2018-11-28 22:41:02'),
(39, 'shebalbin@yahoo.com', '2018-11-28 22:42:21', '2018-11-28 22:42:21'),
(40, 'alessabaniel@yahoo.com', '2018-11-28 22:50:03', '2018-11-28 22:50:03'),
(41, 'erielbelle321@gmail.com', '2018-11-28 22:59:30', '2018-11-28 22:59:30'),
(42, 'suva.yayie13@yahoo.com', '2018-11-28 23:20:25', '2018-11-28 23:20:25'),
(43, 'chaypallera@yahoo.com', '2018-11-29 00:02:32', '2018-11-29 00:02:32'),
(44, 'andam.merygrace@gmail.com', '2018-11-29 00:09:04', '2018-11-29 00:09:04'),
(45, 'kasedegbega@gmail.com', '2018-11-29 00:42:57', '2018-11-29 00:42:57'),
(46, 'mariavictoria.galang@yahoo.com', '2018-11-29 01:05:51', '2018-11-29 01:05:51'),
(47, 'mylene.fresnido@yahoo.com', '2018-11-29 01:23:43', '2018-11-29 01:23:43'),
(48, 'apcreyes@yahoo.com', '2018-11-29 01:24:00', '2018-11-29 01:24:00'),
(49, 'Prl_jhoy@yahoo.ca', '2018-11-29 02:04:23', '2018-11-29 02:04:23'),
(50, 'aulairemarianne@yahoo.com', '2018-11-29 02:58:53', '2018-11-29 02:58:53'),
(51, 'redapple812@yahoo.com', '2018-11-29 03:28:51', '2018-11-29 03:28:51'),
(52, 'deasisdesiree@gmail.com', '2018-11-29 03:34:26', '2018-11-29 03:34:26'),
(53, 'gracebausa@gmail.com', '2018-11-29 04:11:34', '2018-11-29 04:11:34'),
(54, 'sexy-silk@live.com', '2018-11-29 04:40:09', '2018-11-29 04:40:09'),
(55, 'socialinbox@yahoo.com', '2018-11-29 04:46:56', '2018-11-29 04:46:56'),
(56, 'jayfailanga@gamail.com', '2018-11-29 05:06:02', '2018-11-29 05:06:02'),
(57, 'julieanne.punzalan@gmail.com', '2018-11-29 05:33:10', '2018-11-29 05:33:10'),
(58, 'agnes.bautista07@yahoo.com', '2018-11-29 06:05:44', '2018-11-29 06:05:44'),
(59, 'coleenquizon@yahoo.com', '2018-11-29 06:19:28', '2018-11-29 06:19:28'),
(60, 'bashasiayngco09@gmail.com', '2018-11-29 06:27:14', '2018-11-29 06:27:14'),
(61, 'jcdemesa0923@gmail.com', '2018-11-29 06:30:38', '2018-11-29 06:30:38'),
(62, 'ivymae.ambrose4@yahoo.com', '2018-11-29 06:32:15', '2018-11-29 06:32:15'),
(63, 'yasminmalabuyo@gmail.com', '2018-11-29 06:56:18', '2018-11-29 06:56:18'),
(64, 'mariemso.901@gmail.com', '2018-11-29 06:56:44', '2018-11-29 06:56:44'),
(65, 'yvetteavelino@gmail.com', '2018-11-29 07:52:01', '2018-11-29 07:52:01'),
(66, 'gascruzzz@gmail.com', '2018-11-29 08:27:07', '2018-11-29 08:27:07'),
(67, 'roniceanne@gmail.com', '2018-11-29 09:13:53', '2018-11-29 09:13:53'),
(68, 'ermiesomintac@gmail.com', '2018-11-29 09:42:27', '2018-11-29 09:42:27'),
(69, 'dr.gmlosito0822@gmail.com', '2018-11-29 10:07:04', '2018-11-29 10:07:04'),
(70, 'mylene_p_caparal@yahoo.com', '2018-11-29 10:22:06', '2018-11-29 10:22:06'),
(71, 'jaspaguia@gmail.com', '2018-11-29 11:00:46', '2018-11-29 11:00:46'),
(72, 'farhanah1925@gmail.com', '2018-11-29 11:32:13', '2018-11-29 11:32:13'),
(73, 'bianca_tamayo@hotmail.com', '2018-11-29 11:45:16', '2018-11-29 11:45:16'),
(74, 'lizaremulla@ymail.com', '2018-11-29 13:09:57', '2018-11-29 13:09:57'),
(75, 'amineentertainment@gmail.com', '2018-11-29 13:18:09', '2018-11-29 13:18:09'),
(76, 'ellendelacion_03@yahoo.com', '2018-11-29 13:20:23', '2018-11-29 13:20:23'),
(77, 'nto@joh.ph', '2018-11-29 13:34:01', '2018-11-29 13:34:01'),
(78, 'esr.rivera@gmail.com', '2018-11-29 13:35:22', '2018-11-29 13:35:22'),
(79, 'liza_sanchezangelo@yahoo.com', '2018-11-29 14:23:23', '2018-11-29 14:23:23'),
(80, 'hlpappas@hotmail.com', '2018-11-29 15:20:33', '2018-11-29 15:20:33'),
(81, 'talenepunso@gmail.com', '2018-11-29 15:29:05', '2018-11-29 15:29:05'),
(82, 'merce.paraso@yahoo.com', '2018-11-29 16:19:00', '2018-11-29 16:19:00'),
(83, 'bskowron26@gmail.com', '2018-11-29 16:46:25', '2018-11-29 16:46:25'),
(84, 'irisbona@gmail.com', '2018-11-29 18:32:06', '2018-11-29 18:32:06'),
(85, 'mfcdefensor@hot.ail.com', '2018-11-29 19:59:43', '2018-11-29 19:59:43'),
(86, 'mfcdefensor@hotmail.com', '2018-11-29 20:00:15', '2018-11-29 20:00:15'),
(87, 'glesan_agao@yahoo.com', '2018-11-29 20:13:07', '2018-11-29 20:13:07'),
(88, 'klaxamana16@gmail.com', '2018-11-29 20:27:00', '2018-11-29 20:27:00'),
(89, 'mjlonallab@gmail.com', '2018-11-29 22:53:22', '2018-11-29 22:53:22'),
(90, 'kyllie32@yahoo.com', '2018-11-30 01:33:12', '2018-11-30 01:33:12'),
(91, 'rccasurao@gmail.com', '2018-11-30 01:46:25', '2018-11-30 01:46:25'),
(92, 'shennah_1018@yahoo.com', '2018-11-30 05:58:38', '2018-11-30 05:58:38'),
(93, 'odet_pa@yahoo.com', '2018-11-30 08:05:20', '2018-11-30 08:05:20'),
(94, 'lengmedes@yahoo.com', '2018-11-30 10:32:59', '2018-11-30 10:32:59'),
(95, 'letty_tac@yahoo.com', '2018-11-30 12:14:31', '2018-11-30 12:14:31'),
(96, 'triciaischi@yahoo.com', '2018-11-30 12:31:41', '2018-11-30 12:31:41'),
(97, 'micahfienne13@gmail.com', '2018-11-30 13:45:57', '2018-11-30 13:45:57'),
(98, 'raemabel.magno@doleintl.com', '2018-11-30 14:12:47', '2018-11-30 14:12:47'),
(99, 'sheilamay.timoteo@gmail.com', '2018-11-30 22:34:10', '2018-11-30 22:34:10'),
(100, 'cherobelle@yahoo.com', '2018-12-01 00:55:34', '2018-12-01 00:55:34'),
(101, 'jenllorca@yahoo.com', '2018-12-01 03:42:29', '2018-12-01 03:42:29');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` decimal(13,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `taxes`
--

INSERT INTO `taxes` (`id`, `name`, `value`) VALUES
(1, 'Philippines Vat', '12.00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salutation` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `default_shipping_address_id` int(11) DEFAULT NULL,
  `default_billing_address_id` int(11) DEFAULT NULL,
  `mobile_phone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `email_verified_at`, `password`, `salutation`, `birthday`, `gender`, `active`, `remember_token`, `created_at`, `updated_at`, `default_shipping_address_id`, `default_billing_address_id`, `mobile_phone`, `phone`, `photo`) VALUES
(1, 'Thaniel Jay', NULL, 'Duldulao', 'tj@boomtech.com', NULL, '$2y$10$bmerVL72Ry.U9df2OG7bm.4P3NjEmDtKLwmGPrO9u3Gb35pcAkksW', 'Mr.', '2018-11-28', 1, 1, 'bm9M5cMIQOUFLQpYXL9vVZWfUhzUn6WYZqxgn84hrQEpqORsujPec9GpXjJi', '2018-11-28 18:16:23', '2018-11-28 17:45:04', 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wishes`
--

CREATE TABLE `wishes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `addresses_country_id_foreign` (`country_id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_attribute_set`
--
ALTER TABLE `attribute_attribute_set`
  ADD KEY `attribute_attribute_set_attribute_id_foreign` (`attribute_id`),
  ADD KEY `attribute_attribute_set_attribute_set_id_foreign` (`attribute_set_id`);

--
-- Indexes for table `attribute_product_value`
--
ALTER TABLE `attribute_product_value`
  ADD KEY `attribute_product_value_attribute_id_foreign` (`attribute_id`),
  ADD KEY `attribute_product_value_product_id_foreign` (`product_id`);

--
-- Indexes for table `attribute_sets`
--
ALTER TABLE `attribute_sets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attribute_values_attribute_id_foreign` (`attribute_id`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `carriers`
--
ALTER TABLE `carriers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_rules`
--
ALTER TABLE `cart_rules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_rules_customer_id_foreign` (`customer_id`),
  ADD KEY `cart_rules_gift_product_id_foreign` (`gift_product_id`),
  ADD KEY `cart_rules_reduction_currency_id_foreign` (`reduction_currency_id`),
  ADD KEY `cart_rules_minimum_amount_currency_id_foreign` (`minimum_amount_currency_id`);

--
-- Indexes for table `cart_rules_categories`
--
ALTER TABLE `cart_rules_categories`
  ADD KEY `cart_rules_categories_cart_rule_id_foreign` (`cart_rule_id`),
  ADD KEY `cart_rules_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `cart_rules_combinations`
--
ALTER TABLE `cart_rules_combinations`
  ADD KEY `cart_rules_combinations_cart_rule_id_1_foreign` (`cart_rule_id_1`),
  ADD KEY `cart_rules_combinations_cart_rule_id_2_foreign` (`cart_rule_id_2`);

--
-- Indexes for table `cart_rules_customers`
--
ALTER TABLE `cart_rules_customers`
  ADD KEY `cart_rules_customers_cart_rule_id_foreign` (`cart_rule_id`),
  ADD KEY `cart_rules_customers_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `cart_rules_products`
--
ALTER TABLE `cart_rules_products`
  ADD KEY `cart_rules_products_cart_rule_id_foreign` (`cart_rule_id`),
  ADD KEY `cart_rules_products_product_id_foreign` (`product_id`);

--
-- Indexes for table `cart_rules_product_groups`
--
ALTER TABLE `cart_rules_product_groups`
  ADD KEY `cart_rules_product_groups_cart_rule_id_foreign` (`cart_rule_id`),
  ADD KEY `cart_rules_product_groups_product_group_id_foreign` (`product_group_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_categories` (`slug`);

--
-- Indexes for table `category_product`
--
ALTER TABLE `category_product`
  ADD KEY `category_product_category_id_foreign` (`category_id`),
  ADD KEY `category_product_product_id_foreign` (`product_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `companies_user_id_foreign` (`user_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inquiries`
--
ALTER TABLE `inquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_templates`
--
ALTER TABLE `notification_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `notification_templates_slug_unique` (`slug`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_carrier_id_foreign` (`carrier_id`),
  ADD KEY `orders_currency_id_foreign` (`currency_id`),
  ADD KEY `orders_status_id_foreign` (`status_id`);

--
-- Indexes for table `order_cart_rules`
--
ALTER TABLE `order_cart_rules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD KEY `order_product_order_id_foreign` (`order_id`),
  ADD KEY `order_product_product_id_foreign` (`product_id`),
  ADD KEY `order_product_specific_price_id_foreign` (`specific_price_id`);

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_status_history`
--
ALTER TABLE `order_status_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_status_history_status_id_foreign` (`status_id`),
  ADD KEY `order_status_history_order_id_foreign` (`order_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_roles`
--
ALTER TABLE `permission_roles`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_roles_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_users`
--
ALTER TABLE `permission_users`
  ADD PRIMARY KEY (`user_id`,`permission_id`),
  ADD KEY `permission_users_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_skus` (`sku`),
  ADD KEY `products_attribute_set_id_foreign` (`attribute_set_id`),
  ADD KEY `products_tax_id_foreign` (`tax_id`),
  ADD KEY `products_group_id_foreign` (`group_id`);

--
-- Indexes for table `product_badges`
--
ALTER TABLE `product_badges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_groups`
--
ALTER TABLE `product_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`);

--
-- Indexes for table `product_measurements`
--
ALTER TABLE `product_measurements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `role_users_user_id_foreign` (`user_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specific_prices`
--
ALTER TABLE `specific_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `specific_prices_product_id_foreign` (`product_id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subscribers_email_unique` (`email`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_slug_unique` (`slug`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_users` (`email`);

--
-- Indexes for table `wishes`
--
ALTER TABLE `wishes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wishes_user_id_foreign` (`user_id`),
  ADD KEY `wishes_product_id_foreign` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `attribute_sets`
--
ALTER TABLE `attribute_sets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `attribute_values`
--
ALTER TABLE `attribute_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `carriers`
--
ALTER TABLE `carriers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cart_rules`
--
ALTER TABLE `cart_rules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `inquiries`
--
ALTER TABLE `inquiries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `notification_templates`
--
ALTER TABLE `notification_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_cart_rules`
--
ALTER TABLE `order_cart_rules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `order_status_history`
--
ALTER TABLE `order_status_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `product_badges`
--
ALTER TABLE `product_badges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `product_groups`
--
ALTER TABLE `product_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `product_measurements`
--
ALTER TABLE `product_measurements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `specific_prices`
--
ALTER TABLE `specific_prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `taxes`
--
ALTER TABLE `taxes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
--
-- AUTO_INCREMENT for table `wishes`
--
ALTER TABLE `wishes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `attribute_attribute_set`
--
ALTER TABLE `attribute_attribute_set`
  ADD CONSTRAINT `attribute_attribute_set_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `attribute_attribute_set_attribute_set_id_foreign` FOREIGN KEY (`attribute_set_id`) REFERENCES `attribute_sets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `attribute_product_value`
--
ALTER TABLE `attribute_product_value`
  ADD CONSTRAINT `attribute_product_value_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `attribute_product_value_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD CONSTRAINT `attribute_values_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cart_rules`
--
ALTER TABLE `cart_rules`
  ADD CONSTRAINT `cart_rules_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cart_rules_gift_product_id_foreign` FOREIGN KEY (`gift_product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cart_rules_minimum_amount_currency_id_foreign` FOREIGN KEY (`minimum_amount_currency_id`) REFERENCES `currencies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cart_rules_reduction_currency_id_foreign` FOREIGN KEY (`reduction_currency_id`) REFERENCES `currencies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cart_rules_categories`
--
ALTER TABLE `cart_rules_categories`
  ADD CONSTRAINT `cart_rules_categories_cart_rule_id_foreign` FOREIGN KEY (`cart_rule_id`) REFERENCES `cart_rules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cart_rules_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cart_rules_combinations`
--
ALTER TABLE `cart_rules_combinations`
  ADD CONSTRAINT `cart_rules_combinations_cart_rule_id_1_foreign` FOREIGN KEY (`cart_rule_id_1`) REFERENCES `cart_rules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cart_rules_combinations_cart_rule_id_2_foreign` FOREIGN KEY (`cart_rule_id_2`) REFERENCES `cart_rules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cart_rules_customers`
--
ALTER TABLE `cart_rules_customers`
  ADD CONSTRAINT `cart_rules_customers_cart_rule_id_foreign` FOREIGN KEY (`cart_rule_id`) REFERENCES `cart_rules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cart_rules_customers_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cart_rules_products`
--
ALTER TABLE `cart_rules_products`
  ADD CONSTRAINT `cart_rules_products_cart_rule_id_foreign` FOREIGN KEY (`cart_rule_id`) REFERENCES `cart_rules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cart_rules_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cart_rules_product_groups`
--
ALTER TABLE `cart_rules_product_groups`
  ADD CONSTRAINT `cart_rules_product_groups_cart_rule_id_foreign` FOREIGN KEY (`cart_rule_id`) REFERENCES `cart_rules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cart_rules_product_groups_product_group_id_foreign` FOREIGN KEY (`product_group_id`) REFERENCES `product_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `category_product`
--
ALTER TABLE `category_product`
  ADD CONSTRAINT `category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `companies_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_carrier_id_foreign` FOREIGN KEY (`carrier_id`) REFERENCES `carriers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `orders_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `orders_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `order_statuses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `order_product_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `order_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `order_product_specific_price_id_foreign` FOREIGN KEY (`specific_price_id`) REFERENCES `specific_prices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_status_history`
--
ALTER TABLE `order_status_history`
  ADD CONSTRAINT `order_status_history_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `order_status_history_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `order_statuses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `permission_roles`
--
ALTER TABLE `permission_roles`
  ADD CONSTRAINT `permission_roles_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_users`
--
ALTER TABLE `permission_users`
  ADD CONSTRAINT `permission_users_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_attribute_set_id_foreign` FOREIGN KEY (`attribute_set_id`) REFERENCES `attribute_sets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `products_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `product_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `products_tax_id_foreign` FOREIGN KEY (`tax_id`) REFERENCES `taxes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `role_users`
--
ALTER TABLE `role_users`
  ADD CONSTRAINT `role_users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `specific_prices`
--
ALTER TABLE `specific_prices`
  ADD CONSTRAINT `specific_prices_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `wishes`
--
ALTER TABLE `wishes`
  ADD CONSTRAINT `wishes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wishes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
